
RX = [0,0,0];

cmap = cool(61);

for sensor = 1:6

    figure;
    title(horzcat('effect of phi (sensor ',num2str(sensor),')'),'FontSize',12);
    xlabel('emitter number','FontSize',12);
    xlim([0.5, 6.5]);
    set(gca,'XTick',1:6);
    
    hold on;

    for phid = 0:1:60

        R = 15;
        thetad = 0;

        TX = [R*cos(thetad*pi/180), R*sin(thetad*pi/180), phid*pi/180];

        Lambda = BrightnessMatrix(RX, TX);
        
        plot(1:6,Lambda(sensor,:),'color',cmap(phid+1,:));
                
    end

    colormap(cmap);
    colorbar;
    
    hold off;
    
end