function R_max = RPrime(Lambda, thetaP, phiP)
%RPRIME Our best guess of R, given the brightness matrix, Lambda,
% and thetaP, phiP (our best guess of theta and phi).

SensorR = 1.98;

% verified that 100000 is close enough to infinity that this works,
% cant use Inf directly, as this does not specify correctly in which
% direction the robot actually is.

infinite_dist_ECM = ECM(100000, thetaP, phiP);

Lambda;

[biggest_lambda max_i] = max(max(Lambda'));
[biggest_lambda max_j] = max(max(Lambda));

denominator_MAT = infinite_dist_ECM;
denominator_MAT(denominator_MAT == 0) = Inf;
denominator_MAT = denominator_MAT.^-1;

%denominator_MAT

A = Lambda .* denominator_MAT; % elementwise multiplication

Ainv = InverseAmplitudeModel(A);

R_max = Ainv(max_i, max_j) + 2*SensorR; % this is an upper limit for R
% we think that the true R must be smaller than this

end

