function i = Rtoi(R)

% R cases 5 - 25, i = 1:11
% absolue minimum: 4.4 (inclusive, robots touching) 
% absolue maximum: 26 (exclusive)
% note: classes include thier lower boundary 
% and exclude their upper boundary
num_samples = length(R);

if(R(:,1) >= 26)
    error('ERROR: R out of interval [4.4, 26) -> found %f\n', max(R));
elseif(R(:,1) < 4.4)
    error('ERROR: R out of interval [4.4, 26) -> found %f\n', min(R));
end
    
i = ((R-5)/2)+1;

% handle continuous valued input by rounding to the nearest class
i = round(i);

% I DONT KNOW WHY THIS CODE DOESNT WORK
%{
if((i(:,1) < 1)||(i(:,1) > 11))
    % you should not ever see this error if the above code is correct
    error('ERROR: R class out of range [1, 11] BAD\n');
end
%}
if(i(:,1) < 1)
    % you should not ever see this error if the above code is correct
    error('ERROR: R class out of range [1, 11] BAD (i < 1)');
elseif(i(:,1) > 11)
    % you should not ever see this error if the above code is correct
    error('ERROR: R class out of range [1, 11] BAD (i > 11)');
end





end

