function [X, y, sample_configuration] = create_RNB_data_inf2(dim, num_samples)

%% figure out for which dimension we are labeling the cases
% expand the string dim to have 5 chars for string compare
while(length(dim) < 5)
    dim = horzcat(dim,' ');
end
if(dim == 'R    ')
    class_dim = 1;
elseif(dim == 'theta')
    class_dim = 2;
elseif(dim == 'phi  ')
    class_dim = 3;
else
    error('ERROR: unknown dim, dim = "%s"\n', dim); 
end

%% establish the region for which data will be created

absolute_min_angle = -2.5; % (inclusive)
absolute_max_angle = 57.5; % (exclusive)

% CAUTION, min_R is a special case caused by the robots touching
absolute_min_R = 4.4; % (inclusive)
absolute_max_R = 26; % (exclusive)

R_range = absolute_max_R - absolute_min_R;
angle_range = absolute_max_angle - absolute_min_angle;

R_list = randi((R_range*1000)-1,num_samples,1)/1000 + absolute_min_R;
thetad_list = randi((angle_range*1000)-1,num_samples,1)/1000 + absolute_min_angle;
phid_list = randi((angle_range*1000)-1,num_samples,1)/1000 + absolute_min_angle;

% verify our sets conform to the interval rules (important)
if((R_list(:,1) < absolute_min_R)+(R_list(:,1) >= absolute_max_R))
    error('ERROR: R_list invalid!');
elseif((thetad_list(:,1) < absolute_min_angle)+(thetad_list(:,1) >= absolute_max_angle))
    error('ERROR: thetad_list invalid!\n');
elseif((phid_list(:,1) < absolute_min_angle)+(phid_list(:,1) >= absolute_max_angle))
    error('ERROR: phid_list invalid!\n');
end


X = zeros(num_samples,36);
y = zeros(num_samples,1);
sample_configuration = [R_list thetad_list phid_list];

RX = [0,0,0];


for i = 1:num_samples
    
    R = R_list(i);
    theta = thetad_list(i)*pi/180.0;
    phi = phid_list(i)*pi/180.0;
    
     
    TX = [(R)*cos(theta), (R)*sin(theta), phi];
    Lambda = BrightnessMatrix(RX, TX);
    X(i,:) = reshape(Lambda,1,36);
                
end

if(class_dim == 1)
    y = Rtoi(sample_configuration(:,class_dim));
elseif(class_dim == 2)
    y = thetatoi(sample_configuration(:,class_dim),0);
elseif(class_dim == 3)
    y = phitoi(sample_configuration(:,class_dim),0);
end



end

