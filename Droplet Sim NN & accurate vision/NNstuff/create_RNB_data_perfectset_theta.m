function [X, y] = create_RNB_data_perfectset_theta(num_samples)

X = zeros(num_samples,36);
y = zeros(num_samples,1);

RX = [0,0,0];

for i = 1:num_samples
    %{
    theta = randi(360,1)*pi/180;
    phi = randi(360,1)*pi/180;
    %}
    theta = pi/2 + (i/(num_samples-1))*(pi/3);
    phi = 0;
    
    
    R = 10;
    y(i) = i;
    
    TX = [R*cos(theta), R*sin(theta), phi];
    
    Lambda = BrightnessMatrix(RX, TX);
    X(i,:) = reshape(Lambda,1,36);
end





end

