function [X, y] = create_RNB_data_inf();

min_angle = -3;
max_angle = 58;

%{
while(length(dim) < 5)
    dim = horzcat(dim,' ');
end
%}

%{
if(dim == 'R    ')
    num_samples = length(5:2:25)*length(min_angle:5:max_angle)*length(min_angle:5:max_angle);
else
    num_samples = length(5:1:25)*length(0:5:55)*length(min_angle:1:max_angle);
end
%}
num_samples = 20000;

X = zeros(num_samples,36);
y = zeros(num_samples,1);

RX = [0,0,0];

i = 1;

% dim cases need to be arranged smallest to biggest 'R', 'phi', 'theta'

for i = 1:num_samples
    
    R = randi(2000,1)/100 + 5;
    thetad = randi(600,1)/10 - 5;
    phid = randi(600,1)/10 - 5;
    
    fprintf('R(cm): %f, theta(deg): %f phi(deg): %f\n',R, thetad, phid);
    
    y(i) = floor((phid-1)*(12/60) + 0.5) + 1; % what is it??? 
    if(y(i) == 0)
        y(i) = y(i)+1;
    end
    
    fprintf('were calling it %i\n', y(i));
   
    theta = thetad*pi/180.0;
    phi = phid*pi/180.0;
    
     
    TX = [(R)*cos(theta), (R)*sin(theta), phi];
    Lambda = BrightnessMatrix(RX, TX);
    X(i,:) = reshape(Lambda,1,36);
                
end

%{
if(dim == 'R    ')
    for R = 5:2:25
        for thetad = min_angle:1:max_angle
            for phid = min_angle:1:max_angle
                theta = thetad*pi/180.0;
                phi = phid*pi/180.0;

                TX = [R*cos(theta), R*sin(theta), phi];

                Lambda = BrightnessMatrix(RX, TX);
                X(i,:) = reshape(Lambda,1,36);
                
                y(i) = (R-3)/2; % unique line
                
                i = i + 1;
            end
        end
    end
elseif(dim == 'phi  ')
    for R = 5:1:25
        for thetad = min_angle:1:max_angle
            for phid = 0:5:55
                theta = thetad*pi/180.0;
                phi = phid*pi/180.0;

                TX = [R*cos(theta), R*sin(theta), phi];

                Lambda = BrightnessMatrix(RX, TX);
                X(i,:) = reshape(Lambda,1,36);
                
                y(i) = (phid/5)+1; % unique line
                
                i = i + 1;
            end
        end    
    end
elseif(dim == 'theta')
    for R = 5:1:25
        for thetad = 0:5:55
            for phid = min_angle:1:max_angle
                theta = thetad*pi/180.0;
                phi = phid*pi/180.0;

                TX = [R*cos(theta), R*sin(theta), phi];

                Lambda = BrightnessMatrix(RX, TX);
                X(i,:) = reshape(Lambda,1,36);
                
                y(i) = (thetad/5)+1; % unique line
                
                i = i + 1;
            end
        end
    end        
else
    error('ERROR, unknown dimension passed to create_RNB_data_hard');
end

%}

end

