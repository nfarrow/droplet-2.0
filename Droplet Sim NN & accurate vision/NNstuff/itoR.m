function R = itoR(i)

% R cases 5:2:25, i = 1:11
if((max(i) > 11)||(min(i) < 1))
    error('i out of range, ERROR');
end
    
R = (i*2)+3;

end

