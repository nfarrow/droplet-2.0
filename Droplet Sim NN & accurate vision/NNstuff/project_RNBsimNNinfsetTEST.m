
addpath('C:\Users\Toshiba\Desktop\Droplet Sim')

tic();

%{
this_run_param = 'R';
this_run_file_prefix = 'RhardMED';
this_run_dim = 1; % 1 for R
%}

%{
this_run_param = 'theta';
this_run_file_prefix = 'thetahardM';
this_run_dim = 2; % 1 for theta
%}


this_run_param = 'phi';
this_run_file_prefix = 'phihard';
this_run_dim = 3; % 3 for phi



% PROGRAM OPTIONS:
show_accuracy_curve = 0;
new_test = 0;


%% load Neural Network structure

%input_layer_size  = 36;  % 6x6 Input Images of BM
input_layer_size  = 20;  % 4x5 Input Images of re-parameterized BM

hidden_layer_size = 10;
%hidden_layer_size = 2*hls;


Theta1 = load( horzcat(this_run_file_prefix,'Theta1.csv') );
Theta2 = load( horzcat(this_run_file_prefix,'Theta2.csv') );

fprintf('Loaded Thetas\n');



%% create data
if(new_test)

    fprintf('Creating test data set for "%s"\n', this_run_param);

    %[X_test, y_test] = create_RNB_data_hard(this_run_param);
    %[X_test, y_test] = create_RNB_data_inf(this_run_param); % were going with phi, parameter doesn't matter
    %[X_test, y_test, test_config] = create_RNB_data_hard2(this_run_param);
    %[X_test, y_test, test_config] = create_RNB_data_hard2_NOTRAIN(this_run_param);
    [X_test, y_test, test_config] = create_RNB_data_inf2(this_run_param, 100000);

    y_real = test_config(:,this_run_dim);

    X_test = Xpreprocess(X_test);
    fprintf('preprocess complete\n');
end

num_samples = length(y_test);
fprintf('Testing with %i examples\n', num_samples);

num_classes = length(unique(y_test));

%{
% draw a histogram of the latest data batch solution distribution
if(show_training_distribution)
    num_labels = length(unique(y_test))   
    figure;
    hist(y_test,num_labels);
    %unique(y)
end
%}
    
%% run the TEST

lambda = 1;
gamma = 0;

per_class_accuracy = zeros(num_classes,3);
per_class_reg_error = zeros(num_classes,3);
per_class_reg_MSE = zeros(num_classes,3);
per_class_J = zeros(num_classes,3);

[J, T1grad, T2grad] = NNCFsimpleClassification(Theta1, Theta2, ...
                                           input_layer_size, ...
                                           hidden_layer_size, ...
                                           num_classes, ...
                                           X_test, y_test, lambda, gamma);


pred = predictClassification(Theta1, Theta2, X_test);
fprintf('Finished Classification\n');
reg_pred = predictRegression(Theta1, Theta2, X_test, this_run_param);
fprintf('Finished Regression\n');



for test_class = 1:num_classes

    class_subset = (y_test == test_class);
    class_pred = pred(class_subset);
    class_reg_pred = reg_pred(class_subset);
    class_y_test = y_test(class_subset);
    class_y_real = y_real(class_subset);
    class_X_test = X_test(class_subset,:);
    
    [J, T1grad, T2grad] = NNCFsimpleClassification(Theta1, Theta2, ...
                                           input_layer_size, ...
                                           hidden_layer_size, ...
                                           num_classes, ...
                                           class_X_test, class_y_test, lambda, gamma);

    per_class_J(test_class,dimension) = J;
    per_class_accuracy(test_class,dimension) = mean(double(class_pred == class_y_test)) * 100;
    per_class_reg_error(test_class,dimension) = mean(class_reg_pred - class_y_real);
    per_class_reg_MSE(test_class,dimension) = mean((class_reg_pred - class_y_real) .* (class_reg_pred - class_y_real));

    fprintf('class %i\n',test_class);
    fprintf('cost: %f\n', J);
    fprintf('accuracy: %f\n', per_class_accuracy(test_class,dimension));
    fprintf('error: %f\n', per_class_reg_error(test_class,dimension));
    fprintf('MSE: %f\n', per_class_reg_MSE(test_class,dimension));

end

% plot some figures of accuracy versus correct answer, 
% to see where we miss the most

    
dimension = 1;

figure;
plot(1:num_classes, per_class_J(:,dimension),'g');
xlim([0,num_classes+1]);
title('Training Algorithm Cost Function','FontSize',12);
xlabel('class number','FontSize',12);
ylabel('cost','FontSize',12);

figure;
plot(1:num_classes, per_class_accuracy(:,dimension), 'b');
xlim([0,num_classes+1]);
title('Classification Accuracy','FontSize',12);
xlabel('class number','FontSize',12);
ylabel('accuracy (%)','FontSize',12);

figure;
plot(1:num_classes, per_class_reg_error(:,dimension), 'k');
xlim([0,num_classes+1]);
title('Regression Error','FontSize',12);
xlabel('class number','FontSize',12);
ylabel('error (degrees)','FontSize',12);

figure;
plot(1:num_classes, per_class_reg_MSE(:,dimension), 'r');
xlim([0,num_classes+1]);
title('Regression Mean Squared Error','FontSize',12);
xlabel('class number','FontSize',12);
ylabel('MSE (degrees^2)','FontSize',12);



toc()

fprintf('END PROGRAM, OK\n');


% hard test R accuracy = 85.4508 (classification), MSE = 31.0576
% hard test R (update) = 87.5248 (classification), MSE = 31.5397

% hard test phi accuracy = 61.3031 (classification), MSE = 247.639
% hard test phi (update) = 80.2227 (classification), MSE = 244.664

% hard test theta accuracy = 88.5241 (classification), MSE = 243.924
