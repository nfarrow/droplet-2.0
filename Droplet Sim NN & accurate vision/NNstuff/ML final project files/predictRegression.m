function p = predictRegression(Theta1, Theta2, X, dim)
%PREDICT Predict the label of an input given a trained neural network
%   p = PREDICT(Theta1, Theta2, X) outputs the predicted label of X given the
%   trained weights of a neural network (Theta1, Theta2)

% Useful values
m = size(X, 1);
num_labels = size(Theta2, 1);

% return variables 
p = zeros(size(X, 1), 1);

% algorithm

%X.Theta1 is 'input' of hidden layer, h1 is 'activation' of hidden layer
% and fed forward
h1 = sigmoid([ones(m, 1) X] * Theta1');
%h1.Theta2 is 'input' of output layer, h2 is 'activation' of output layer
h2 = sigmoid([ones(m, 1) h1] * Theta2');
% note that max and 'softmax' both will give the maximal entry, the only 
% difference is that 'softmax' outputs are guaranteed to sum to 1, and
% hence are interpretable as probabilities.

%szh2 = size(h2)

probability_list = softmax(h2);

while(length(dim) < 5)
    dim = horzcat(dim,' ');
end

if(dim == 'R    ')
    R_list = itoR(1:11);
    %szpl = size(probability_list)
    %szrl = size(R_list)
    
    %p = sum(probability_list * R_list');
    p = probability_list * R_list';
elseif(dim == 'phi  ')
    phi_list = itophi(1:12,0); % output in degrees
    %p = sum(probability_list .* phi_list);
    p = probability_list * phi_list';
elseif(dim == 'theta')
    theta_list = itotheta(1:12,0); % output in degrees
    %p = sum(probability_list .* theta_list);
    p = probability_list * theta_list';
else
    error('ERROR: unknown dimension inupt');
end

% while we are simply taking the maximum neuron of this final layer, it may
% be worthwhile to look at the other highest values to see what the other
% close 'correct' answers would be.  This information would provide insight
% into how our NN is making wrong predictions.

end
