function Theta = ExactTheta(RX, TX)

Theta = atan2(TX(2)-RX(2), TX(1)-RX(1)); % format is (delta-y, delta-x)
Theta = Theta - RX(3); % subtract off the receivers angle
Theta = PItoPIangle(Theta);

end

