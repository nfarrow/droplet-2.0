function VecMat = RXitoTXjVectorsCX(RX, TX)
%RXITOTXJVECTORSCX returns a [6 6] matrix of all the complex number vectors
% connecting every sensor to every emitter.  Note that by convention
% we have chosen the vector to point opposite to the path of the light
% formula = vector from RX center to TX center  
%           - vector from RX center to sensor
%           + vector from TX center to emitter
%
% the vectors for sensor 1 the emmiters 1-6 are given in the
% first 2 rows; sensor 2 in rows 3,4; etc. to row 12

SensorR = 1.98;

i2pi = i*2*pi;

% 6 complex valued numbers representing vectors to the sensors and emitters
% of an UNROTATED robot, rotations of the robot are applied independently
% later (below)
SandEvectorsCX = UnitNormalsCX(0)*SensorR;
    

VecMat = 0;
SensoriVecs = zeros(1,6);

for s = 1:6
    for e = 1:6     % here i is the complex valued imaginary unit sqrt(-1)
        SensoriVecs(e) = (TX(1) + i*TX(2)) - (RX(1) + i*RX(2))...
            - (exp(i*RX(3))*SandEvectorsCX(s))...
            + (exp(i*TX(3))*SandEvectorsCX(e));
    end
    
    if(s == 1)
        VecMat = SensoriVecs;
    else
        VecMat = [VecMat; SensoriVecs];
    end

end


end