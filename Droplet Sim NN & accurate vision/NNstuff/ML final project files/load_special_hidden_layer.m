function [initial_Theta1] = load_special_hidden_layer(initial_Theta1)

% this requires at least 13 hidden nodes in layer 1
initial_Theta1(:, 2:end) = 0;
initial_Theta1(2, 2:end) = 1;
initial_Theta1(2, [2:7]+(0*6)) = 1;
initial_Theta1(3, [2:7]+(1*6)) = 1;
initial_Theta1(4, [2:7]+(2*6)) = 1;
initial_Theta1(5, [2:7]+(3*6)) = 1;
initial_Theta1(6, [2:7]+(4*6)) = 1;
initial_Theta1(7, [2:7]+(5*6)) = 1;
initial_Theta1(8, [2 8 14 20 26 32]+0) = 1;
initial_Theta1(9, [2 8 14 20 26 32]+1) = 1;
initial_Theta1(10, [2 8 14 20 26 32]+2) = 1;
initial_Theta1(11, [2 8 14 20 26 32]+3) = 1;
initial_Theta1(12, [2 8 14 20 26 32]+4) = 1;
initial_Theta1(13, [2 8 14 20 26 32]+5) = 1;




end

