function likelyTX = BestDamnSolver(Lambda)

Tp = ThetaPrime(Lambda);
Pp = PhiPrime(Lambda);

options = optimset('Display','iter'); % there are other options, learn them

% start the search 10*sqrt(2) = 14.1 distance away, in the best theta and phi
% that we know about

search_start_guess = [10*cos(Tp),10*sin(Tp),Pp]; 

likelyTX = fminsearch(@(x) BDSolver_function(x,Lambda), search_start_guess, options);

end

