function i = thetatoi(theta,rad,cont)

if(rad) % accept input in radians
    theta = theta*180/pi;
end

% theta cases 0 - 55, i = 1:12
if((theta > 60)||(theta < -5))
    error('theta out of range (%f), ERROR', theta);
end
    
i = (theta/5.0)+1;

if(cont)
    i = round(i);
    if(i < 1)
        i = 1;
    elseif(i > 12)
        i = 12;
    end
end

end

