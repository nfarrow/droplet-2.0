function E = EmitterModel(angle)
%EMITTERMODEL


E = cos(angle);

% clip negative values to zero
E(find(E < 0)) = 0;


end

