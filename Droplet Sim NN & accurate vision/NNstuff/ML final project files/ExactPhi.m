function Phi = ExactPhi(RX, TX)

Phi = TX(3) - RX(3); % phi is the difference between the TX heading with the RX heading
Phi = PItoPIangle(Phi);

end

