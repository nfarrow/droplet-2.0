function rMat = rMatrix(RX, TX)
%RMATRIX gives the matrix of the magnitudes of all the 'little r's' 

rMat = abs(RXitoTXjVectorsCX(RX, TX));


end

