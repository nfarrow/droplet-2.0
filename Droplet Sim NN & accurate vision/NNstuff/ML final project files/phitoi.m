function i = phitoi(phi,rad,cont)

if(rad) % accept input in radians
    phi = phi*180/pi;
end

% phi cases 0 - 55, i = 1:12
if((phi > 60)||(phi < -5))
    error('phi out of range (%f), ERROR', phi);
end
    
i = (phi/5.0)+1;

if(cont)
    i = round(i);
    if(i < 1)
        i = 1;
    elseif(i > 12)
        i = 12;
    end
end

end

