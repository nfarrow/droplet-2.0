% changes from version 1:
% the learning rate has been increased

addpath('C:\Users\Toshiba\Desktop\Droplet Sim')

% This will be out typical starting arrangement:
%RX = [0,0,0];
%TX = [2,5,3];

num_pts = 2000;

num_runs = 1;

max_dist = 24;
min_dist = 4;
num_dists = max_dist-min_dist;

accuracy = zeros(1,num_runs);
mean_err = zeros(1,num_runs);
std_err = zeros(1,num_runs);
%hlsize = zeros(1,num_runs);


% PROGRAM OPTIONS:
special_init_layer_1 = 0;
plot_allruns_results = 0;
show_training_examples = 0;
show_training_distribution = 0;
show_hidden_layer_1 = 0;


for iter_num = 1:num_runs

    X = zeros(num_pts,36);
    y = zeros(num_pts,1);

    for i = 1:num_pts
        dist = 0;
        while((dist < min_dist)||(dist > max_dist))
            RX = [randn(1)*5, randn(1)*5, randn(1)*2*pi];
            TX = [randn(1)*5, randn(1)*5, randn(1)*2*pi];
            dist = sqrt((TX(1)-RX(1))^2 + (TX(2)-RX(2))^2);
        end
        Lambda = BrightnessMatrix(RX, TX);
        X(i,:) = reshape(Lambda,1,36);
        y(i) = floor(dist)-(min_dist-1); % now capped to 1 - num_dists
    end

    % draw a histogram of the latest data batch solution distribution
    if(show_training_distribution)
        figure;
        hist(y,20);
        %unique(y)
    end
    
    % draw lots of little brightness matricies
    if(show_training_examples)
        Display_N_brightness_matricies(X,100);
    end



    input_layer_size  = 36;  % 6x6 Input Images of BM

    %hidden_layer_size = 100;
    %hidden_layer_size = 5*iter_num;
    hidden_layer_size = 13;
    
    hlsize(iter_num) = hidden_layer_size; 

    num_labels = num_dists;   




    fprintf('\nInitializing Neural Network Parameters ...\n')

    initial_Theta1 = randInitializeWeights(input_layer_size, hidden_layer_size);
    initial_Theta2 = randInitializeWeights(hidden_layer_size, num_labels);

    if(special_init_layer_1) % this requires at least 13 hidden nodes in layer 1
        initial_Theta1(:, 2:end) = 0;
        initial_Theta1(2, 2:end) = 1;
        initial_Theta1(2, [2:7]+(0*6)) = 1;
        initial_Theta1(3, [2:7]+(1*6)) = 1;
        initial_Theta1(4, [2:7]+(2*6)) = 1;
        initial_Theta1(5, [2:7]+(3*6)) = 1;
        initial_Theta1(6, [2:7]+(4*6)) = 1;
        initial_Theta1(7, [2:7]+(5*6)) = 1;
        initial_Theta1(8, [2 8 14 20 26 32]+0) = 1;
        initial_Theta1(9, [2 8 14 20 26 32]+1) = 1;
        initial_Theta1(10, [2 8 14 20 26 32]+2) = 1;
        initial_Theta1(11, [2 8 14 20 26 32]+3) = 1;
        initial_Theta1(12, [2 8 14 20 26 32]+4) = 1;
        initial_Theta1(13, [2 8 14 20 26 32]+5) = 1;
    end
    
    
    %initial_Theta1 = AllTimeBest1;
    %initial_Theta2 = AllTimeBest2;

    
    % Unroll parameters
    initial_nn_params = [initial_Theta1(:) ; initial_Theta2(:)];

    fprintf('\nTraining Neural Network... \n')

    %  After you have completed the assignment, change the MaxIter to a larger
    %  value to see how more training helps.
    options = optimset('MaxIter', 50);

    %  You should also try different values of lambda
    lambda = 3;

    % Create "short hand" for the cost function to be minimized
    costFunction = @(p) nnCostFunctionClassification(p, ...
                                       input_layer_size, ...
                                       hidden_layer_size, ...
                                       num_labels, X, y, lambda);

    % Now, costFunction is a function that takes in only one argument (the
    % neural network parameters)
    [nn_params, cost] = fmincg(costFunction, initial_nn_params, options)

    % Obtain Theta1 and Theta2 back from nn_params
    Theta1 = reshape(nn_params(1:hidden_layer_size * (input_layer_size + 1)), ...
                     hidden_layer_size, (input_layer_size + 1));

    Theta2 = reshape(nn_params((1 + (hidden_layer_size * (input_layer_size + 1))):end), ...
                     num_labels, (hidden_layer_size + 1));

    if(show_hidden_layer_1)
        fprintf('\nVisualizing Neural Network... \n')
        displayData(Theta1(:, 2:end));
    end
    
    fprintf('END LEARNING PART\n');



    pred = predict(Theta1, Theta2, X);

    fprintf('\nTraining Set Accuracy: %f\n', mean(double(pred == y)) * 100);
    fprintf('\nTraining Set Mean Error: %f\n', mean(double(pred - y)));

    accuracy(1,iter_num) = mean(double(pred == y)) * 100;
    mean_err(1,iter_num) = mean(double(pred - y));
    std_err(1,iter_num) = std(double(pred - y));

    if(iter_num == 1)
        best_accuracy = accuracy(1,iter_num);
        bestTheta1 = Theta1;
        bestTheta2 = Theta2;
        bestInitTheta1 = initial_Theta1;
        bestInitTheta2 = initial_Theta2;
    elseif(accuracy(1,iter_num) > best_accuracy)
        best_accuracy = accuracy(1,iter_num);
        bestTheta1 = Theta1;
        bestTheta2 = Theta2;
        bestInitTheta1 = initial_Theta1;
        bestInitTheta2 = initial_Theta2;
    end

end

if(plot_allruns_results)
    figure;
    %plot(hlsize, accuracy);
    plot(1:num_runs, accuracy);
    xlabel('run number');
    ylabel('accuracy');

    figure;
    %plot(hlsize, mean_err);
    plot(1:num_runs, mean_err);
    xlabel('run number');
    ylabel('average error');

    figure;
    %plot(hlsize, std_err);
    plot(1:num_runs, std_err);
    xlabel('run number');
    ylabel('standard dev of error');
end



fprintf('END PROGRAM, wow WTF!\n');



