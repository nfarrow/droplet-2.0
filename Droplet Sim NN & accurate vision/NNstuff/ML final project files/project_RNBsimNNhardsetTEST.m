
addpath('C:\Users\Toshiba\Desktop\Droplet Sim')

tic();

this_run_param = 'theta';
this_run_file_prefix = 'thetahardM';


% PROGRAM OPTIONS:
show_accuracy_curve = 0;



%% load Neural Network structure

%input_layer_size  = 36;  % 6x6 Input Images of BM
input_layer_size  = 20;  % 4x5 Input Images of re-parameterized BM

hidden_layer_size = 10;
%hidden_layer_size = 2*hls;


Theta1 = load( horzcat(this_run_file_prefix,'Theta1.csv') );
Theta2 = load( horzcat(this_run_file_prefix,'Theta2.csv') );

fprintf('Loaded Thetas\n');


%% create data

fprintf('Creating test data set for "%s"\n', this_run_param);

[X_test, y_test] = create_RNB_data_hard(this_run_param);

num_samples = length(y_test);

%RETARDED!
TPN = this_run_param;
while(length(TPN) < 5)
    TPN = horzcat(TPN,' ');
end
if(TPN == 'R    ')
    real_y = itoR(y_test);
elseif(TPN == 'phi  ')
    real_y = itophi(y_test,0);
elseif(TPN == 'theta')
    real_y = itotheta(y_test,0);
else
    error('unknown this_run_param');
end
    
fprintf('Loaded %i test examples\n', num_samples);

X_test = Xpreprocess(X_test);

fprintf('preprocess complete\n');

% gather info about the test
num_labels = length(unique(y_test))   

%{
% draw a histogram of the latest data batch solution distribution
if(show_training_distribution)
    figure;
    hist(y_test,20);
    %unique(y)
end
%}
    
%% run the TEST

pred = predictClassification(Theta1, Theta2, X_test);
fprintf('Finished Classification\n');
reg_pred = predictRegression(Theta1, Theta2, X_test, this_run_param);
fprintf('Finished Regression\n');

accuracy = mean(double(pred == y_test)) * 100;
reg_MSE = mean((reg_pred - real_y) .* (reg_pred - real_y));

fprintf('hard test accuracy: %f\n', accuracy);
fprintf('MSE: %f\n', reg_MSE);


% plot some figures of accuracy versus correct answer, 
% to see where we miss the most
%{
figure;
plot(1:num_runs, Jlist(:,hls),'b');
figure;
plot(1:num_runs, accuracy(:,hls), 'b');
figure;
plot(1:num_runs, reg_MSE(:,hls), 'r');
%}


toc()

fprintf('END PROGRAM, OK\n');


% hard test R accuracy = 85.4508 (classification), MSE = 31.0576
% hard test R (update) = 87.5248 (classification), MSE = 31.5397

% hard test phi accuracy = 61.3031 (classification), MSE = 247.639
% hard test phi (update) = 80.2227 (classification), MSE = 244.664

% hard test theta accuracy = 88.5241 (classification), MSE = 243.924
