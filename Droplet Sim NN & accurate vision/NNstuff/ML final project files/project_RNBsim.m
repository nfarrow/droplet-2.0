%Range & Bearing sim!

% This will be out typical starting arrangement:
standardRX = [0,0,0];
standardTX = [2,5,3];

num_samples = 1000;

X = zeros(num_samples,36);  % to avoid 3D data structure, will store brightness
                            % matricies in rows of 36 values (values are
                            % stored columnwise from Lambda, use Pack_BM,
                            % and Unpack_BM to convert between these forms

y = zeros(num_samples,3);   % y will store exact (R, theta, phi)
y_guess = zeros(num_samples,3);   % y_guess will store the best guess of (R, theta, phi)

reset_sim = 1;
if(reset_sim)
    inputRX = zeros(num_samples,3); % store num_samples robot configurations 
    inputTX = zeros(num_samples,3);
end


for i = 1:num_samples
    
    if(reset_sim)
        dist = 0;
        while((dist < 5)||(dist > 24))
            %RX = [randn(1)*5, randn(1)*5, PItoPIangle(randn(1)*pi)];
            RX = [0,0,0];
            TX = [randn(1)*5, randn(1)*5, PItoPIangle(randn(1)*pi)];
            dist = ExactR(RX, TX);
        end
    else
        RX = inputRX(i,:);
        TX = inputTX(i,:);
        dist = ExactR(RX, TX);
    end
    
    %TX = standardTX;
    
    Lambda = BrightnessMatrix(RX, TX);
    X(i,:) = Pack_BM(Lambda);
    
    thetaP = ThetaPrime(Lambda);
    phiP = PhiPrime(Lambda);
    
    
    
    R_upper_limit = RPrime(Lambda, thetaP, phiP);
    
    little_r_matrix = rMatrixGuess(Lambda, R_upper_limit, thetaP, phiP);
    
    min_r = min(min(little_r_matrix));
    
    SensorR = 1.98; % note this is defined numerous places in this project so be careful
    R_guess = min_r + 2*SensorR; % note this is NOT the algorithm, this is a shortcut
    % that will should always be a small overestime, you need to 'point'
    % r(i,j) in the right direction, and so the SensorR vectors are tilted a little 
    
    y(i,1) = dist;
    y(i,2) = ExactTheta(RX, TX);
    y(i,3) = ExactPhi(RX, TX);
    
    y_guess(i,1) = R_guess;
    y_guess(i,2) = thetaP;
    y_guess(i,3) = phiP;
    
    % store inputs:
    inputRX(i,:) = RX;
    inputTX(i,:) = TX;
    
    % debug out:
    %RX
    %TX
    fprintf('\nround %i\n',i);
    
    fprintf('RX = [%f,%f,%f]\n', RX(1),RX(2),RX(3));
    fprintf('TX = [%f,%f,%f]\n', TX(1),TX(2),TX(3));
    
    fprintf('y_exact = (%f,%f,%f)\n', y(i,1),y(i,2),y(i,3));
    %y(i,:)
    %y_guess(i,:)
    fprintf('y_guess = (%f,%f,%f)\n', y_guess(i,1),y_guess(i,2),y_guess(i,3));
    
end

range_error = y_guess(:,1)-y(:,1);
max_Rerror = max(range_error)
min_Rerror = min(range_error)
mean_Rerror = mean(range_error)
stdev_Rerror = std(range_error)

theta_error = y_guess(:,2)-y(:,2);
theta_error(theta_error > pi) = theta_error(theta_error > pi) - 2*pi;
theta_error(theta_error < -pi) = theta_error(theta_error < -pi) + 2*pi;
max_thetaerror = max(theta_error)
min_thetaerror = min(theta_error)
mean_thetaerror = mean(theta_error)
stdev_thetaerror = std(theta_error)

phi_error = y_guess(:,3)-y(:,3);
phi_error(phi_error > pi) = phi_error(phi_error > pi) - 2*pi;
phi_error(phi_error < -pi) = phi_error(phi_error < -pi) + 2*pi;
max_phierror = max(phi_error)
min_phierror = min(phi_error)
mean_phierror = mean(phi_error)
stdev_phierror = std(phi_error)


%Display_N_brightness_matricies(X, 5);


