function [X, y] = create_RNB_data(dim);

num_samples = length(5:2:25)*length(0:5:55)*length(0:5:55);

X = zeros(num_samples,36);
y = zeros(num_samples,1);

RX = [0,0,0];

i = 1;

if(dim == 'R')
    for R = 5:2:25
        for thetad = 0:5:55
            for phid = 0:5:55
                theta = thetad*pi/180.0;
                phi = phid*pi/180.0;

                TX = [R*cos(theta), R*sin(theta), phi];

                Lambda = BrightnessMatrix(RX, TX);
                X(i,:) = reshape(Lambda,1,36);
                
                y(i) = (R-3)/2; % unique line
                
                i = i + 1;
            end
        end
    end
elseif(dim == 'theta')
    for R = 5:2:25
        for thetad = 0:5:55
            for phid = 0:5:55
                theta = thetad*pi/180.0;
                phi = phid*pi/180.0;

                TX = [R*cos(theta), R*sin(theta), phi];

                Lambda = BrightnessMatrix(RX, TX);
                X(i,:) = reshape(Lambda,1,36);
                
                y(i) = (thetad/5)+1; % unique line
                
                i = i + 1;
            end
        end
    end    
elseif(dim == 'phi')
    for R = 5:2:25
        for thetad = 0:5:55
            for phid = 0:5:55
                theta = thetad*pi/180.0;
                phi = phid*pi/180.0;

                TX = [R*cos(theta), R*sin(theta), phi];

                Lambda = BrightnessMatrix(RX, TX);
                X(i,:) = reshape(Lambda,1,36);
                
                y(i) = (phid/5)+1; % unique line
                
                i = i + 1;
            end
        end    
    end
else
    error('ERROR, unknown dimension passed to create_RNB_data');
end



end

