function R = ExactR(RX, TX)

R = sqrt((TX(1)-RX(1))^2 + (TX(2)-RX(2))^2);

end

