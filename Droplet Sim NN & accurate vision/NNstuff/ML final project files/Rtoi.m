function i = Rtoi(R,cont)

% R cases 5:2:25, i = 1:11
if((R > 26)||(R < 4))
    error('R out of range (%f), ERROR',R);
end
    
i = (R-3)/2;

if(cont) % accept continuous valued inputs
    i = round(i);
    if(i < 1)
        i = 1;
    elseif(i > 11)
        i = 11;
    end
end

end

