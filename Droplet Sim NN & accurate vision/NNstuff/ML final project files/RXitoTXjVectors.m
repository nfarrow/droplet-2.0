function VecMat = RXitoTXjVectors(RX, TX)
%RXITOTXJVECTORS returns a [12 6] matrix of all the vectors [2 1]
% connecting every sensor to every emitter.  Note that by convention
% we have chosen the vector to point opposite to the path of the light
% formula = vector from RX center to TX center  
%           - vector from RX center to sensor
%           + vector from TX center to emitter
%
% the vectors for sensor 1 the emmiters 1-6 are given in the
% first 2 rows; sensor 2 in rows 3,4; etc. to row 12


SensorR = 1.98;

UnitNormals = Rot2D(-pi/6)*[1,0]';
for i = 2:6
    UnitNormals = [UnitNormals, Rot2D(-pi/3)*UnitNormals(:,i-1)];
end

UnitNormals % monitor

VecMat = 0;
SensoriVecs = zeros(2,6);

for i = 1:6
    for j = 1:6
        SensoriVecs(:,j) =  [TX(1); TX(2)] - [RX(1); RX(2)]...
            - (SensorR * Rot2D(RX(3)*(pi/180))*UnitNormals(:,i))...
            + (SensorR * Rot2D(TX(3)*(pi/180))*UnitNormals(:,j));        
    end
    
    if(i == 1)
        VecMat = SensoriVecs;
    else
        VecMat = [VecMat; SensoriVecs];
    end

%VecMat
    
end

