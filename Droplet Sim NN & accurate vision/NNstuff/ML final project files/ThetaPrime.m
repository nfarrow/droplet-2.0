function theta_guess = ThetaPrime(Lambda)
%THETAPRIME Our best guess of theta, given the brightness matrix, Lambda.
%   Detailed explanation goes here

basisCX = UnitNormalsCX(0);

S = sum(Lambda');
%T = sum(Lambda)
%sum(S.*basis)


theta_guess = angle(sum(S.*basisCX));


% the correct answer from the inputs [0,0,0] [2,5,3] is 1.29282
% (this depends of how Lambda was created)

% thats weird, these are complex conjugates %%%
% S*basis'
% sum(S.*basis)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
end

