function phi = itophi(i,rad)

% phi cases 0 - 55, i = 1:12
if((max(i) > 12)||(min(i) < 1))
    error('i out of range [phi conv], ERROR');
end
    
phi = (i-1)*5;

if(rad == 1)
    phi = phi*pi/180.0;
end

end

