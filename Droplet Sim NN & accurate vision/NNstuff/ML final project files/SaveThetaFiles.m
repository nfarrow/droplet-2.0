function SaveThetaFiles(Theta1, Theta2, label)

% label should be something like 'phi12range' so that the
% file name becomes 'phi12rangeTheta1.csv'

file_base_name = 'Theta1.csv';
file_name = horzcat(label,file_base_name);
save(file_name,'Theta1','-ascii','-double');

file_base_name = 'Theta2.csv';
file_name = horzcat(label,file_base_name);
save(file_name,'Theta2','-ascii','-double');


end

