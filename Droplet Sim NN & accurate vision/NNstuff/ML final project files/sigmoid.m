function g = sigmoid(z)
%SIGMOID Compute sigmoid functoon
%   J = SIGMOID(z) computes the sigmoid of z.

%{
comments!
domain: all reals
range: (0,1)

inputs < 0 will evaluate to < 0.5
inputs > 0 will evaluate to > 0.5
g(0) = 0.5

%}

g = 1.0 ./ (1.0 + exp(-z));
end
