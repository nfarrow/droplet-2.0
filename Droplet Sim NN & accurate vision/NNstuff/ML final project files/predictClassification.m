function p = predictClassification(Theta1, Theta2, X)
%PREDICT Predict the label of an input given a trained neural network
%   p = PREDICT(Theta1, Theta2, X) outputs the predicted label of X given the
%   trained weights of a neural network (Theta1, Theta2)

% Useful values
m = size(X, 1);
num_labels = size(Theta2, 1);

% return variables 
p = zeros(size(X, 1), 1);

% algorithm

%X.Theta1 is 'input' of hidden layer, h1 is 'activation' of hidden layer
% and fed forward
h1 = sigmoid([ones(m, 1) X] * Theta1');
%h1.Theta2 is 'input' of output layer, h2 is 'activation' of output layer
h2 = sigmoid([ones(m, 1) h1] * Theta2');
% note that max and 'softmax' both will give the maximal entry, the only 
% difference is that 'softmax' outputs are guaranteed to sum to 1, and
% hence are interpretable as probabilities.
[dummy, p] = max(h2, [], 2); % returns the largest elements and thier indicies along the 2nd dimension of h2

% while we are simply taking the maximum neuron of this final layer, it may
% be worthwhile to look at the other highest values to see what the other
% close 'correct' answers would be.  This information would provide insight
% into how our NN is making wrong predictions.

end
