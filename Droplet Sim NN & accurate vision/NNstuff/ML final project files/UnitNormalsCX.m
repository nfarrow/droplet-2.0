function [NormalsCX] = UnitNormalsCX(angle)
%UNITNORMALSCX returns 6 complex numbers cooresponding to vectors
% to each of the 6 sensors or emitters.  These vectors are also first
% rotated by the input angle before being returned.   
% angle has units of radians
i2pi = i*2*pi;

NormalsCX = exp(i2pi*(-1/12));
for s = 2:6
    NormalsCX = [NormalsCX, exp(i2pi*(-1/6))*NormalsCX(s-1)];
end

NormalsCX = NormalsCX .* exp(i*angle);

end

