function [h, display_array] = displayFinalLayer(Theta)
%DISPLAYDATA Display 2D data in a nice grid
%   [h, display_array] = DISPLAYDATA(X, example_width) displays 2D data
%   stored in X in a nice grid. It returns the figure handle h and the 
%   displayed array if requested.

% Gray Image
colormap(gray);

% Compute rows, cols
[m n] = size(Theta);

% Compute number of items to display
display_rows = m;
display_cols = n;

% Between images padding
pad = 1;

% Setup blank display
display_array = - ones(pad + display_rows + pad, pad + display_cols + pad );

% Get the max value of all Theta
max_val = max(max(abs(Theta)));

% Copy Theta into a patch on the display array
for i = 1:display_rows
	for j = 1:display_cols
		
			 
	
		% Copy the patch
		% we color in the rows one at a time
		
		display_array(pad + i, pad + j) = Theta(i, j)/ max_val;
	end
	
end

% Display Image
h = imagesc(display_array, [-1 1]);

% Do not show axis
axis image off

drawnow;

end
