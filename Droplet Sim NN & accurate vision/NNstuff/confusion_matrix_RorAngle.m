function confusion_matrix_RorAngle(y_known, y_pred, dimension)

% dimension is a number:
% 1 = R
% 2 = theta
% 3 = phi

figure;
%title('Confusion Matrix','FontSize',12);


mat = confusionmat(y_known,y_pred);

[m n] = size(mat);

imagesc(mat);            %# Create a colored plot of the matrix values
colormap(flipud(gray));  %# Change the colormap to gray (so higher values are
                         %#   black and lower values are white)

%textStrings = num2str(mat(:),'%0.2f');  %# Create strings from the matrix values
textStrings = num2str(mat(:),'%i');  %# Create strings from the matrix values

textStrings = strtrim(cellstr(textStrings));  %# Remove any space padding
[x,y] = meshgrid(1:m);   %# Create x and y coordinates for the strings
hStrings = text(x(:),y(:),textStrings(:),...      %# Plot the strings
                'HorizontalAlignment','center');
midValue = mean(get(gca,'CLim'));  %# Get the middle value of the color range
textColors = repmat(mat(:) > midValue,1,3);  %# Choose white or black for the
                                             %#   text color of the strings so
                                             %#   they can be easily seen over
                                             %#   the background color
set(hStrings,{'Color'},num2cell(textColors,2));  %# Change the text colors

if(dimension == 1)
    label_nums = itoR(1:m);
    text(m/2-1,0,horzcat('Range Confusion Matrix'));
elseif(dimension == 2)
    label_nums = itotheta(1:m,0);
    text(m/2-1,0,horzcat('Theta Confusion Matrix'));
elseif(dimension == 3)
    label_nums = itophi(1:m,0);
    text(m/2-1,0,horzcat('Phi Confusion Matrix'));
end



set(gca,'XTick',1:m,...                         %# Change the axes tick marks
    'XTickLabel',label_nums,...  %#   and tick labels
    'YTick',1:m,...
    'YTickLabel',{label_nums},...  %#   and tick labels
    'TickLength',[0 0]);

    
end

