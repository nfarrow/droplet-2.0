function [J grad] = nnCostFunctionClassification(nn_params, ...
                                   input_layer_size, ...
                                   hidden_layer_size, ...
                                   num_labels, ...
                                   X, y, lambda)
%NNCOSTFUNCTION Implements the neural network cost function for a two layer
%neural network which performs classification
%   [J grad] = NNCOSTFUNCTON(nn_params, hidden_layer_size, num_labels, ...
%   X, y, lambda) computes the cost and gradient of the neural network. The
%   parameters for the neural network are "unrolled" into the vector
%   nn_params and need to be converted back into the weight matrices. 
% 
%   The returned parameter grad should be a "unrolled" vector of the
%   partial derivatives of the neural network.
%

gamma = 10; % learning rate test


% Reshape nn_params back into the parameters Theta1 and Theta2, the weight matrices
% for our 2 layer neural network
Theta1 = reshape(nn_params(1:hidden_layer_size * (input_layer_size + 1)), ...
                 hidden_layer_size, (input_layer_size + 1));

Theta2 = reshape(nn_params((1 + (hidden_layer_size * (input_layer_size + 1))):end), ...
                 num_labels, (hidden_layer_size + 1));

% Setup some useful variables
m = size(X, 1);
         
% You need to return the following variables correctly 
J = 0;
Theta1_grad = zeros(size(Theta1));
Theta2_grad = zeros(size(Theta2));

% ====================== YOUR CODE HERE ======================
% Instructions: You should complete the code by working through the
%               following parts.
%
% Part 1: Feedforward the neural network and return the cost in the
%         variable J. After implementing Part 1, you can verify that your
%         cost function computation is correct by verifying the cost
%         computed in ex4.m
%
% Part 2: Implement the backpropagation algorithm to compute the gradients
%         Theta1_grad and Theta2_grad. You should return the partial derivatives of
%         the cost function with respect to Theta1 and Theta2 in Theta1_grad and
%         Theta2_grad, respectively. After implementing Part 2, you can check
%         that your implementation is correct by running checkNNGradients
%
%         Note: The vector y passed into the function is a vector of labels
%               containing values from 1..K. You need to map this vector into a 
%               binary vector of 1's and 0's to be used with the neural network
%               cost function.
%
%         Hint: We recommend implementing backpropagation using a for-loop
%               over the training examples if you are implementing it for the 
%               first time.
%
% Part 3: Implement regularization with the cost function and gradients.
%
%         Hint: You can implement this around the code for
%               backpropagation. That is, you can compute the gradients for
%               the regularization separately and then add them to Theta1_grad
%               and Theta2_grad from Part 2.
%


%{
from figure 2 of handout:

htheta(x) = a3
a3 = g(z3)
z3 = theta2.a2
a2 = g(z2)
z2 = theta1.a1
a1 = x

X =         [5000 400] (measured)
x =         [400 1]
a1 =        [401 1]
theta1 =    [25 401]
z2 =        [25 1]
a2 =        [26 1]
theta2 =    [10 26]
z3 =        [10 1]
a3 =        [10 1] (dont add 1)
%}


% save off a set of normal unit vectors in # of dimensions equal to number
% of data classes (in this program thats 10)
num_classes = max(y);
yvecs = ind2vec(y'); % these are saved as 'sparse vectors' by default
%szyvecs = size(yvecs)

Jm = zeros(1,m);

DELTA2 = 0;
DELTA1 = 0;

for i = 1:m


    %szX = size(X)
    z2 = Theta1*[1 X(i,:)]';
    %szz2 = size(z2)
    a2 = [1; sigmoid(z2)];
    %sza2 = size(a2)
    z3 = Theta2*a2;
    %szz3 = size(z3)
    a3 = sigmoid(z3);
    %sza3 = size(a3)
    htheta = a3;
    
    Jm(i) = -yvecs(:,i)'*log(htheta) - (ones(num_classes,1) - yvecs(:,i))'*log(ones(num_classes,1) - htheta);

    % accumulate data for backpropagation
    delta3 = a3 - yvecs(:,i);
    temp = Theta2'*delta3;
    delta2 = temp(2:end) .* sigmoidGradient(z2);
   
    
    %DELTA2 = DELTA2 + delta3*a2';
    DELTA2 = DELTA2 + gamma*delta3*a2';
    %DELTA1 = DELTA1 + delta2*a1'; (??not needed, apparently this is only for
    % hidden layers) (what is going on in pg. 9, under item 3??, not clear 
    % why this function is given in general form, looks like it applies to
    % all layers l = {1,2,3}  )
    
    % strong assumption a1 = x
    %DELTA1 = DELTA1 + delta2*[1 X(i,:)]; 
    DELTA1 = DELTA1 + gamma*delta2*[1 X(i,:)]; 
    
    
end

% chop off the first row (makes this size = [10 25])
%DELTA2 = DELTA2(:,[2:end]);

%szD2 = size(DELTA2) % [10 25]
%szD1 = size(DELTA1) % [25 400]

J = sum(Jm)/m;

if(lambda ~= 0)
% regularize (IDK if this would waste more time to compute the
% regularization only to dump it with lambda = 0, or just check lambda
% beforehand and compute if necessary)

    J = J + (lambda/(2*m))*(sum(sum(Theta1(:,[2:end]).^2)) + sum(sum(Theta2(:,[2:end]).^2)));

end

% Theta1_grad should be [25 401]
% Theta2_grad should be [10 26]

Theta1_grad = DELTA1/m;
Theta2_grad = DELTA2/m;

% regularize the gradient

Theta1_grad(:,[2:end]) = Theta1_grad(:,[2:end]) + ((lambda/m)*Theta1(:,[2:end]));
Theta2_grad(:,[2:end]) = Theta2_grad(:,[2:end]) + ((lambda/m)*Theta2(:,[2:end]));



% -------------------------------------------------------------

% =========================================================================

% Unroll gradients
grad = [Theta1_grad(:) ; Theta2_grad(:)];


end
