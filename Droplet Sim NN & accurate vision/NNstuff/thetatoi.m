function i = thetatoi(theta,in_radians)

if(in_radians) % accept input in radians
    theta = theta*180/pi;
end

% theta cases 0 - 55, i = 1:12
% absolue minimum: -2.5 (inclusive)
% absolue maximum: 57.5 (exclusive)
% note: classes include thier lower boundary 
% and exclude their upper boundary

if(theta(:,1) >= 57.5)
    error('ERROR: theta out of interval [-2.5, 57.5) -> found %f\n', max(theta));
elseif(theta(:,1) < -2.5)
    error('ERROR: theta out of interval [-2.5, 57.5) -> found %f\n', min(theta));
end
    
i = (theta/5.0)+1;

% handle continuous valued input by rounding to the nearest class
i = round(i);

if(i(:,1) < 1)
    % you should not ever see this error if the above code is correct
    error('ERROR: theta class out of range [1, 12] BAD (i < 1)');
elseif(i(:,1) > 12)
    % you should not ever see this error if the above code is correct
    error('ERROR: theta class out of range [1, 12] BAD (i > 12)');
end


end

