function [J, Theta1_grad, Theta2_grad] = NNCFsimpleClassification(Theta1, Theta2, ...
                                   input_layer_size, ...
                                   hidden_layer_size, ...
                                   num_labels, ...
                                   X, y, lambda, gamma)
%NNCOSTFUNCTION Implements the neural network cost function for a two layer
%neural network which performs classification

% Setup some useful variables
m = size(X, 1);
         
% return variables 
J = 0;
Theta1_grad = zeros(size(Theta1));
Theta2_grad = zeros(size(Theta2));

% Part 1: Feedforward the neural network and return the cost in the
%         variable J. After implementing Part 1, you can verify that your
%         cost function computation is correct by verifying the cost
%         computed in ex4.m

% Part 2: Implement the backpropagation algorithm to compute the gradients
%         Theta1_grad and Theta2_grad. You should return the partial derivatives of
%         the cost function with respect to Theta1 and Theta2 in Theta1_grad and
%         Theta2_grad, respectively. After implementing Part 2, you can check
%         that your implementation is correct by running checkNNGradients
%
%         Note: The vector y passed into the function is a vector of labels
%               containing values from 1..K. You need to map this vector into a 
%               binary vector of 1's and 0's to be used with the neural network
%               cost function.
%
%         Hint: We recommend implementing backpropagation using a for-loop
%               over the training examples if you are implementing it for the 
%               first time.

% Part 3: Implement regularization with the cost function and gradients.
%
%         Hint: You can implement this around the code for
%               backpropagation. That is, you can compute the gradients for
%               the regularization separately and then add them to Theta1_grad
%               and Theta2_grad from Part 2.



%{
% this is passed as a parameter (num_labels)
num_classes = max(y);
num_classes2 = length(unique(y));
if(num_classes ~= num_classes2) fprintf('CLASS SIZE ERROR'); end
%}

% save off a set of normal unit vectors in # of dimensions equal to number
% of data classes

%yvecs = ind2vec(y'); % these are saved as 'sparse vectors' by default
yvecs = ind2vec(y',num_labels); % these are saved as 'sparse vectors' by default

%szyvecs = size(yvecs)

Jm = zeros(1,m); % save a J (cost) for each training example (m)

DELTA2 = 0;
DELTA1 = 0;

for i = 1:m

    %X.Theta1 is 'input' of hidden layer, a2 is 'activation' of hidden layer
    z2 = Theta1*[1 X(i,:)]';
    a2 = [1; sigmoid(z2)];
    % z3 is 'input' of final layer, a3 is 'activation' of final layer
    z3 = Theta2*a2;
    a3 = sigmoid(z3);
    %htheta = a3;
    
    %Jm(i) = -yvecs(:,i)'*log(htheta) - (ones(num_classes,1) - yvecs(:,i))'*log(ones(num_classes,1) - htheta);
    % SAME but without htheta (uses a3 explictly)
    
    %Jm(i) = -yvecs(:,i)'*log(a3) - (ones(num_classes,1) - yvecs(:,i))'*log(ones(num_classes,1) - a3);
    Jm(i) = -yvecs(:,i)'*log(a3) - (ones(num_labels,1) - yvecs(:,i))'*log(ones(num_labels,1) - a3);

    
    % cost (J) is the sum of -log(1-(activation of all wrong answers)) PLUS the -log(activation of the correct answer)
    % This results in there being a penalty associated with all answers
    % UNLESS the activation is perfect (e.g. [0 0 0 1 0 0 0 0])
    
    
    % accumulate data for backpropagation
    delta3 = a3 - yvecs(:,i); % 'delta3' is basically 'error in layer 3's output'
    temp = Theta2'*delta3;
    delta2 = temp(2:end) .* sigmoidGradient(z2);   
    
    %DELTA2 = DELTA2 + delta3*a2';
    DELTA2 = DELTA2 + gamma*delta3*a2';
    %DELTA1 = DELTA1 + delta2*a1'; (??not needed, apparently this is only for
    % hidden layers) (what is going on in pg. 9, under item 3??, not clear 
    % why this function is given in general form, looks like it applies to
    % all layers l = {1,2,3}  )
    
    % strong assumption a1 = x
    %DELTA1 = DELTA1 + delta2*[1 X(i,:)]; 
    DELTA1 = DELTA1 + gamma*delta2*[1 X(i,:)];     
end

J = sum(Jm)/m;

if(lambda ~= 0)
% regularize (IDK if this would waste more time to compute the
% regularization only to dump it with lambda = 0, or just check lambda
% beforehand and compute if necessary)

    J = J + (lambda/(2*m))*(sum(sum(Theta1(:,[2:end]).^2)) + sum(sum(Theta2(:,[2:end]).^2)));

end


Theta1_grad = DELTA1/m;
Theta2_grad = DELTA2/m;

% regularize the gradient

Theta1_grad(:,[2:end]) = Theta1_grad(:,[2:end]) + ((lambda/m)*Theta1(:,[2:end]));
Theta2_grad(:,[2:end]) = Theta2_grad(:,[2:end]) + ((lambda/m)*Theta2(:,[2:end]));


end
