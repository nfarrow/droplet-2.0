function Xnew = Xpreprocess(X)
% takes a 36 element X and returns a new X with constant valued features
% removed and linear combinations of features added (sums of rows and
% columns)

[m n] = size(X);

% these are the nonzero entries IF theta and phi are held between 0 - 55(60) degrees
Xnonzero = X(:,[7 11 12 13 17 18 19 23 24 25 29 30]); 

col1sum = X(:,7)+X(:,11)+X(:,12);
col2sum = X(:,13)+X(:,17)+X(:,18);
col3sum = X(:,19)+X(:,23)+X(:,24);
col4sum = X(:,25)+X(:,29)+X(:,30);
Xcol = [col1sum col2sum col3sum col4sum];

row1sum = X(:,7)+X(:,13)+X(:,19)+X(:,25);
row2sum = X(:,11)+X(:,17)+X(:,23)+X(:,29);
row3sum = X(:,12)+X(:,18)+X(:,24)+X(:,30);
Xrow = [row1sum row2sum row3sum];

Xnew = [Xnonzero Xcol Xrow zeros(m,1)];



end

