
addpath('C:\Users\Toshiba\Desktop\Droplet Sim')

tic();

this_run_param = 'R';

% PROGRAM OPTIONS:
plot_allruns_results = 0;
show_training_examples = 1;
show_training_distribution = 0;
show_hidden_layer_1 = 1;
show_hidden_layer_2 = 1;
show_cost_curve = 0;
show_accuracy_curve = 0;

%num_hls = 5;
num_runs = 10000;


%Jlist = zeros(num_runs,1);
Jlist = zeros(num_runs,num_hls);
Jchangelist = zeros(num_runs);

accuracy = zeros(num_runs,num_hls);
reg_MSE = zeros(num_runs,num_hls);


%{
accuracy = zeros(1,num_runs);
mean_err = zeros(1,num_runs);
std_err = zeros(1,num_runs);
%hlsize = zeros(1,num_runs);
%}

%% create data

%num_samples = 100;
%num_test_samples = 2000;

    
[X, y] = create_RNB_data(this_run_param);

num_samples = length(y);

if(this_run_param == 'R')
    real_y = itoR(y);
elseif(this_run_param == 'theta')
    real_y = itotheta(y,0);
elseif(this_run_param == 'phi')
    real_y = itophi(y,0);
else
    error('unknown this_run_param');
end
    
X = Xpreprocess(X);

%[X_test, y_test] = create_RNB_data_phi(num_test_samples);


% gather info about the test
num_labels = length(unique(y))   

% draw a histogram of the latest data batch solution distribution
if(show_training_distribution)
    figure;
    hist(y,20);
    %unique(y)
end

% draw lots of little brightness matricies
if(show_training_examples)
    num_to_show = min([num_samples 16]);
    Display_N_brightness_matricies(X,num_to_show); % figure included
    %Display_all_brightness_matricies(X); % only used for the complete set
    examples_fh = gcf;
end



%% create Neural Network structure

for hls = 1:1%num_hls

    %input_layer_size  = 36;  % 6x6 Input Images of BM
    input_layer_size  = 20;  % 4x5 Input Images of re-parameterized BM
    
    hidden_layer_size = 10;
    %hidden_layer_size = 2*hls;
    
    initial_Theta1 = randInitializeWeights(input_layer_size, hidden_layer_size);
    initial_Theta2 = randInitializeWeights(hidden_layer_size, num_labels);

    Theta1 = initial_Theta1;
    Theta2 = initial_Theta2;

    if(show_hidden_layer_1)
        figure;
        displayData(initial_Theta1(:, 2:end))
        pretrain_fh = gcf;
        fp = get(pretrain_fh, 'Position')
        set(pretrain_fh, 'Position', [50,400,fp(3),fp(4)]);
    end

    pred = predictClassification(Theta1, Theta2, X);

    fprintf('PRE TRAIN ACCURACY\n');
    fprintf('Training Set Accuracy: %f\n', mean(double(pred == y)) * 100);
    fprintf('Training Set Mean Error: %f\n', mean(double(pred - y)));

    
    
    %  You should also try different values of lambda
    lambda = 0; % this is for REGULARIZATION
    gamma = 1; % this is for LEARNING RATE

    prevJ = 0;

    for iter_num = 1:num_runs

        %[X, y] = create_RNB_data_phi(num_samples); % make new samples

            
        [J, T1grad, T2grad] = NNCFsimpleClassification(Theta1, Theta2, ...
                                           input_layer_size, ...
                                           hidden_layer_size, ...
                                           num_labels, X, y, lambda, gamma);


        Jlist(iter_num,hls) = J;
        Jchangelist(iter_num,hls) = prevJ-J;
        Theta1 = Theta1 - T1grad;
        Theta2 = Theta2 - T2grad;

        pred = predictClassification(Theta1, Theta2, X);
        reg_pred = predictRegression(Theta1, Theta2, X, this_run_param);

        accuracy(iter_num,hls) = mean(double(pred == y)) * 100;
        reg_MSE(iter_num,hls) = mean((reg_pred - real_y) .* (reg_pred - real_y));

        
        %L4std = std(Jchangelist(max(iter_num-3,1):iter_num,hls));
        %L4mean = mean(Jchangelist(max(iter_num-3,1):iter_num,hls));
        
        fprintf('run %i | accuracy: %1.2f | cost: %f (%f)\n', iter_num, accuracy(iter_num,hls)/100, J, prevJ-J);
        %fprintf('%f %f -- %f\n',L4std, L4mean, reg_error(iter_num,hls));
        fprintf('MSE: %f\n', reg_MSE(iter_num,hls));
        prevJ = J;
        %L4std = std(Jlist([(iter_num-5):iternum],hls);
        %L4mean = mean(Jlist([(iter_num-5):iternum],hls);
        
        if(accuracy(iter_num,hls) > 95)
            break;
        end
        
        %{
        fprintf('POST TRAIN ACCURACY %i\n', iter_num);
        fprintf('Training Set Accuracy: %f\n', mean(double(pred == y)) * 100);
        fprintf('Training Set Mean Error: %f\n', mean(double(pred - y)));
        %}
    end

    
    %{
    test_pred = predictClassification(Theta1, Theta2, X_test);
    fprintf('POST TRAIN TEST\n');
    fprintf('Test Set Accuracy: %f\n', mean(double(test_pred == y_test)) * 100);
    fprintf('Test Set Mean Error: %f\n', mean(double(test_pred - y_test)));
    %}
    
    figure;
    plot(1:num_runs, Jlist(:,hls),'b');
    figure;
    plot(1:num_runs, accuracy(:,hls), 'b');
    figure;
    plot(1:num_runs, reg_MSE(:,hls), 'r');

    
    if(show_hidden_layer_1)
        fprintf('\nVisualizing Neural Network... \n')
        figure;
        %title('test title 2');
        displayData(Theta1(:, 2:end));
        posttrain_fh = gcf;
        fp = get(posttrain_fh, 'Position');
        set(posttrain_fh, 'Position', [600,400,fp(3),fp(4)]);
    end

    if(show_hidden_layer_2)
        figure;
        %title('test title 2');
        displayFinalLayer(Theta2);
        HL2_fh = gcf;
        fp = get(HL2_fh, 'Position');
        set(HL2_fh, 'Position', [300,100,fp(3),fp(4)]);
    end

    %pause;
    
end

if(show_cost_curve)
    figure;
    hold on
    plot(1:num_runs, Jlist(:,1), 'b');
    plot(1:num_runs, Jlist(:,2), 'r');
    plot(1:num_runs, Jlist(:,3), 'y');
    plot(1:num_runs, Jlist(:,4), 'g');
    plot(1:num_runs, Jlist(:,5), 'c');
    hold off;
    
    cost_curve_fh = gcf;
end

if(show_accuracy_curve)
    figure;
    hold on
    plot(1:num_runs, accuracy(:,1), 'b');
    plot(1:num_runs, accuracy(:,2), 'r');
    plot(1:num_runs, accuracy(:,3), 'y');
    plot(1:num_runs, accuracy(:,4), 'g');
    plot(1:num_runs, accuracy(:,5), 'c');
    hold off;
    
    accuracy_curve_fh = gcf;
end


%{
if(show_hidden_layer_1)
    fprintf('\nVisualizing Neural Network... \n')
    figure;
    %title('test title 2');
    displayData(Theta1(:, 2:end));
    posttrain_fh = gcf;
    fp = get(posttrain_fh, 'Position');
    set(posttrain_fh, 'Position', [600,400,fp(3),fp(4)]);
end

if(show_hidden_layer_2)
    figure;
    %title('test title 2');
    displayFinalLayer(Theta2);
    HL2_fh = gcf;
    fp = get(HL2_fh, 'Position');
    set(HL2_fh, 'Position', [300,100,fp(3),fp(4)]);
end
%}

fprintf('END LEARNING PART\n');



fprintf('\nTraining Set Accuracy: %f\n', mean(double(pred == y)) * 100);
fprintf('\nTraining Set Mean Error: %f\n', mean(double(pred - y)));

%{
accuracy(1,iter_num) = mean(double(pred == y)) * 100;
mean_err(1,iter_num) = mean(double(pred - y));
std_err(1,iter_num) = std(double(pred - y));
%}


if(plot_allruns_results)
    figure;
    %plot(hlsize, accuracy);
    plot(1:num_runs, accuracy);
    xlabel('run number');
    ylabel('accuracy');

    figure;
    %plot(hlsize, mean_err);
    plot(1:num_runs, mean_err);
    xlabel('run number');
    ylabel('average error');

    figure;
    %plot(hlsize, std_err);
    plot(1:num_runs, std_err);
    xlabel('run number');
    ylabel('standard dev of error');
end


toc()

fprintf('END PROGRAM, wow WTF!\n');


%{
pause;

if(show_hidden_layer_1)
    close(posttrain_fh);    
    close(pretrain_fh);
end
if(show_hidden_layer_2)    
    close(HL2_fh);
end
if(show_training_examples)
    close(examples_fh);
end
if(show_cost_curve)
    close(cost_curve_fh);
end
%}