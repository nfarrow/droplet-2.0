
addpath('C:\Users\Toshiba\Desktop\Droplet Sim')

tic();

%this_run_param = 'R';
%this_run_file_prefix = 'RhardSHORT';
%this_run_param_long = 'R    ';

this_run_param = 'phi';
this_run_file_prefix = 'phihard';
this_run_param_long = 'phi  ';
this_run_dim = 3;

num_runs = 500; 
%  You should also try different values of lambda
lambda = 0; % this is for REGULARIZATION
gamma = 0.9; % this is for LEARNING RATE


fprintf('Attempt to improve Theta set for "%s"\n', this_run_param);

% PROGRAM OPTIONS:
reuse_previous_weights = 1;

plot_allruns_results = 0;
show_training_examples = 0;
show_training_distribution = 0;
show_hidden_layer_1 = 0;
show_hidden_layer_2 = 0;
show_cost_curve = 1;
show_accuracy_curve = 1;
resume_from_manual_stop = 1;

num_new_sets = 5;


%Jlist = zeros(num_runs,1);
Jlist = zeros(num_runs,num_new_sets);
Jchangelist = zeros(num_runs);

accuracy = zeros(num_runs,num_new_sets);
reg_MSE = zeros(num_runs,num_new_sets);


%{
accuracy = zeros(1,num_runs);
mean_err = zeros(1,num_runs);
std_err = zeros(1,num_runs);
%hlsize = zeros(1,num_runs);
%}

%% create data

%num_samples = 100;
%num_test_samples = 2000;

    
%[X, y] = create_RNB_data_hard(this_run_param);
%[X, y] = create_RNB_data_inf(); % classes are in phi
%X = Xpreprocess(X);

if(resume_from_manual_stop == 1)
    initial_Theta1 = Theta1;
    initial_Theta2 = Theta2;
else
    initial_Theta1 = load( horzcat(this_run_file_prefix,'Theta1.csv') );
    initial_Theta2 = load( horzcat(this_run_file_prefix,'Theta2.csv') );
end


[X, y, full_soln] = create_RNB_data_hard2(this_run_param); % classes are in phi
X = Xpreprocess(X);
y_real = full_soln(:,this_run_dim); 
num_samples = length(y);



%% load Neural Network structure

%input_layer_size  = 36;  % 6x6 Input Images of BM
input_layer_size  = 20;  % 4x5 Input Images of re-parameterized BM

hidden_layer_size = 10;
%hidden_layer_size = 2*hls;


for set_num = 1:num_new_sets%num_hls

    if(set_num > 1)
        % reset for another run
        [X, y, full_soln] = create_RNB_data_inf2(this_run_param,num_samples);
        y_real = full_soln(:,this_run_dim); 
        X = Xpreprocess(X);
    
        if(reuse_previous_weights)
            initial_Theta1 = Theta1;
            initial_Theta2 = Theta2;
        else
            initial_Theta1 = load( horzcat(this_run_file_prefix,'Theta1.csv') );
            initial_Theta2 = load( horzcat(this_run_file_prefix,'Theta2.csv') );
        end
    end
        
    Theta1 = initial_Theta1;
    Theta2 = initial_Theta2;

    if(show_hidden_layer_1)
        figure;
        displayData(initial_Theta1(:, 2:end))
        pretrain_fh = gcf;
        fp = get(pretrain_fh, 'Position')
        set(pretrain_fh, 'Position', [50,400,fp(3),fp(4)]);
    end

    pred = predictClassification(Theta1, Theta2, X);

    fprintf('PRE TRAIN ACCURACY\n');
    fprintf('Training Set Accuracy: %f\n', mean(double(pred == y)) * 100);
    fprintf('Training Set Mean Error: %f\n', mean(double(pred - y)));

    
    

    prevJ = 0;

    for iter_num = 1:num_runs

        
            
        [J, T1grad, T2grad] = NNCFsimpleClassification(Theta1, Theta2, ...
                                           input_layer_size, ...
                                           hidden_layer_size, ...
                                           num_labels, X, y, lambda, gamma);


        Jlist(iter_num,set_num) = J;
        Jchangelist(iter_num,set_num) = prevJ-J;
        Theta1 = Theta1 - T1grad;
        Theta2 = Theta2 - T2grad;

        pred = predictClassification(Theta1, Theta2, X);
        reg_pred = predictRegression(Theta1, Theta2, X, this_run_param_long);

        accuracy(iter_num,set_num) = mean(double(pred == y)) * 100;
        %PUT THIS BACK IN:
        reg_MSE(iter_num,set_num) = mean((reg_pred - y_real) .* (reg_pred - y_real));

        
        %L4std = std(Jchangelist(max(iter_num-3,1):iter_num,hls));
        %L4mean = mean(Jchangelist(max(iter_num-3,1):iter_num,hls));
        
        fprintf('run %i|%i | accuracy: %1.2f | cost: %f (%f)\n', set_num, iter_num, accuracy(iter_num,set_num)/100, J, prevJ-J);
        %fprintf('%f %f -- %f\n',L4std, L4mean, reg_error(iter_num,hls));
        %PUT THIS BACK IN:
        fprintf('MSE: %f,    gamma: %f\n', reg_MSE(iter_num,set_num), gamma);
        prevJ = J;
        %L4std = std(Jlist([(iter_num-5):iternum],hls);
        %L4mean = mean(Jlist([(iter_num-5):iternum],hls);
        
        if(accuracy(iter_num,set_num) > 95)
            break;
        end
        
        %{
        fprintf('POST TRAIN ACCURACY %i\n', iter_num);
        fprintf('Training Set Accuracy: %f\n', mean(double(pred == y)) * 100);
        fprintf('Training Set Mean Error: %f\n', mean(double(pred - y)));
        %}
    end

    
    %{
    test_pred = predictClassification(Theta1, Theta2, X_test);
    fprintf('POST TRAIN TEST\n');
    fprintf('Test Set Accuracy: %f\n', mean(double(test_pred == y_test)) * 100);
    fprintf('Test Set Mean Error: %f\n', mean(double(test_pred - y_test)));
    %}
    
    figure;
    plot(1:num_runs, Jlist(:,set_num),'b');
    figure;
    plot(1:num_runs, accuracy(:,set_num), 'b');
    figure;
    plot(1:num_runs, reg_MSE(:,set_num), 'r');

    
    if(show_hidden_layer_1)
        fprintf('\nVisualizing Neural Network... \n')
        figure;
        %title('test title 2');
        displayData(Theta1(:, 2:end));
        posttrain_fh = gcf;
        fp = get(posttrain_fh, 'Position');
        set(posttrain_fh, 'Position', [600,400,fp(3),fp(4)]);
    end

    if(show_hidden_layer_2)
        figure;
        %title('test title 2');
        displayFinalLayer(Theta2);
        HL2_fh = gcf;
        fp = get(HL2_fh, 'Position');
        set(HL2_fh, 'Position', [300,100,fp(3),fp(4)]);
    end

    %pause;
    
end

if(show_cost_curve)
    figure;
    hold on
    plot(1:num_runs, Jlist(:,1), 'b');
    plot(1:num_runs, Jlist(:,2), 'r');
    plot(1:num_runs, Jlist(:,3), 'y');
    plot(1:num_runs, Jlist(:,4), 'g');
    plot(1:num_runs, Jlist(:,5), 'c');
    hold off;
    
    cost_curve_fh = gcf;
end

if(show_accuracy_curve)
    figure;
    hold on
    plot(1:num_runs, accuracy(:,1), 'b');
    plot(1:num_runs, accuracy(:,2), 'r');
    plot(1:num_runs, accuracy(:,3), 'y');
    plot(1:num_runs, accuracy(:,4), 'g');
    plot(1:num_runs, accuracy(:,5), 'c');
    hold off;
    
    accuracy_curve_fh = gcf;
end


%{
if(show_hidden_layer_1)
    fprintf('\nVisualizing Neural Network... \n')
    figure;
    %title('test title 2');
    displayData(Theta1(:, 2:end));
    posttrain_fh = gcf;
    fp = get(posttrain_fh, 'Position');
    set(posttrain_fh, 'Position', [600,400,fp(3),fp(4)]);
end

if(show_hidden_layer_2)
    figure;
    %title('test title 2');
    displayFinalLayer(Theta2);
    HL2_fh = gcf;
    fp = get(HL2_fh, 'Position');
    set(HL2_fh, 'Position', [300,100,fp(3),fp(4)]);
end
%}

fprintf('END LEARNING PART\n');



fprintf('\nTraining Set Accuracy: %f\n', mean(double(pred == y)) * 100);
fprintf('\nTraining Set Mean Error: %f\n', mean(double(pred - y)));

%{
accuracy(1,iter_num) = mean(double(pred == y)) * 100;
mean_err(1,iter_num) = mean(double(pred - y));
std_err(1,iter_num) = std(double(pred - y));
%}


if(plot_allruns_results)
    figure;
    %plot(hlsize, accuracy);
    plot(1:num_runs, accuracy);
    xlabel('run number');
    ylabel('accuracy');

    figure;
    %plot(hlsize, mean_err);
    plot(1:num_runs, mean_err);
    xlabel('run number');
    ylabel('average error');

    figure;
    %plot(hlsize, std_err);
    plot(1:num_runs, std_err);
    xlabel('run number');
    ylabel('standard dev of error');
end


toc()

fprintf('END PROGRAM, wow WTF!\n');


%{
pause;

if(show_hidden_layer_1)
    close(posttrain_fh);    
    close(pretrain_fh);
end
if(show_hidden_layer_2)    
    close(HL2_fh);
end
if(show_training_examples)
    close(examples_fh);
end
if(show_cost_curve)
    close(cost_curve_fh);
end
%}