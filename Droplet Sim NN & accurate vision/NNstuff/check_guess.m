function [all_accuracy] = check_guess(y_guess, y_soln)

% TODO: line up commas in printout, currently for classes > 10000, or number
% of classes > 9, the alignment gets off

% TODO: save this file into a general ML toolbox folder and call from there

% IMPORTANT: y_vectors are assumed to be column-vectors

fprintf('Checking guess:\n\n');

[mg ng] = size(y_guess);
[ms ns] = size(y_soln);

if(ng == 1)
    y_guess = y_guess';
end

if(ns == 1)
    y_soln = y_soln';
end

num_classes = max(y_soln); % this assumes classes are numbered 1 - ?

num_guess_by_class = zeros(1,num_classes); % these are the 'true positives' + 'false positives'
num_soln_by_class = zeros(1,num_classes); % these are the 'true positives' + 'false_negatives'
num_correct_in_class = zeros(1,num_classes); % these are the 'true positives'

class_recall = zeros(1,num_classes);
class_precision = zeros(1,num_classes);

%% DEFINITIONS:
% precision (AKA positive predictive value) is the fraction of predictions
% that are correct or true-positive, 
% precision = "correct predictions"/"all predictions"
%           = "true-positives"/("true-positives" + "false_positives")
%
% recall (AKA sensitivity) is the fraction of predictions that are
% retrieved, this measures the miss-rate
% recall = "correct predictions"/"correct solutions"
%        = "true-positives"/("true-positives" + "false_negatives")

for i = 1:num_classes
    num_guess_by_class(i) = length(find(y_guess == i));
    class_correct_indicies = find(y_soln == i);
    num_soln_by_class(i) = length(class_correct_indicies);
    num_correct_in_class(i) = length(find(y_guess(1,class_correct_indicies) == y_soln(1,class_correct_indicies)));
    if(num_soln_by_class(1,i) == 0) % this catches a possible divide by 0
        if(num_guess_by_class(i) > 0)
            class_recall(i) = 0;
        else
            class_recall(i) = 1; % there were no examples in the test and this was correctly identified (no false negatives)
        end
    else
        class_recall(i) = num_correct_in_class(1,i)/num_soln_by_class(1,i);
    end
    if(num_guess_by_class(1,i) == 0) % this catches a possible divide by 0
        if(num_soln_by_class(1,i) > 0)
            class_precision(i) = 0;
        else
            class_precision(i) = 1;  % there were no examples in the test and this was correctly identified (no false positives)
        end
    else
        class_precision(i) = num_correct_in_class(1,i)/num_guess_by_class(1,i);
    end
    
    
    
    %mins(i,:) = min(X_data(data_of_interest,:));
    %maxs(i,:) = max(X_data(data_of_interest,:));
    %means(i,:) = mean(X_data(data_of_interest,:));
    %stdevs(i,:) = mean(X_data(data_of_interest,:));
end

all_n = length(y_guess);
all_correct = (y_guess == y_soln);
all_accuracy = sum(all_correct)/all_n;

%% PRINT A TABLE OF RESULTS
fprintf('CLASS:    ');
for i = 1:num_classes
    fprintf('     %i ',i);
    if(i == num_classes)
        fprintf('\n');
    end
end

fprintf('guesses:  ');
for i = 1:num_classes
    fprintf('  %4.0f,',num_guess_by_class(i));
    if(i == num_classes)
        fprintf('\b\n');
    end
end

fprintf('solutions:');
for i = 1:num_classes
    fprintf('  %4.0f,',num_soln_by_class(i));
    if(i == num_classes)
        fprintf('\b\n');
    end
end

fprintf('precision:');
for i = 1:num_classes
    fprintf(' %0.3f,',class_precision(i));
    if(i == num_classes)
        fprintf('\b (guesses that were correct)\n');
    end
end

fprintf('recall:   ');
for i = 1:num_classes
    fprintf(' %0.3f,',class_recall(i));
    if(i == num_classes)
        fprintf('\b (solutions correctly identified)\n');
    end
end

fprintf('miss rate:'); % this is (1 - recall rate)
for i = 1:num_classes
    fprintf(' %0.3f,',1-class_recall(i));
    if(i == num_classes)
        fprintf('\b\n');
    end
end


fprintf('\ntotal accuracy:   %0.4f\n', all_accuracy);

fprintf('\n');  % final buffer line

















end

