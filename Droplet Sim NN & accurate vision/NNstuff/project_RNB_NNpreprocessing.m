addpath('C:\Users\Toshiba\Desktop\Droplet Sim')

%min_dist = 4;
%max_dist = 39;

%num_dists = max_dist-min_dist;

%RX = [0,0,0];
%num_examples = length(min_dist:max_dist);

%X = zeros(num_examples,36);
%y = zeros(num_examples,1);

%Xshadow = zeros(num_examples,36);

[X, y] = create_RNB_data();
num_examples = length(y);

%Display_all_brightness_matricies(X);

stdX = std(X);

Xnonzero = X(:,[7 11 12 13 17 18 19 23 24 25 29 30]);
%size(Xnew);

col1sum = X(:,7)+X(:,11)+X(:,12);
col2sum = X(:,13)+X(:,17)+X(:,18);
col3sum = X(:,19)+X(:,23)+X(:,24);
col4sum = X(:,25)+X(:,29)+X(:,30);

Xcol = [col1sum col2sum col3sum col4sum];
size(Xcol)

row1sum = X(:,7)+X(:,13)+X(:,19)+X(:,25);
row2sum = X(:,11)+X(:,17)+X(:,23)+X(:,29);
row3sum = X(:,12)+X(:,18)+X(:,24)+X(:,30);
Xrow = [row1sum row2sum row3sum];
Xnew = [Xnonzero Xcol Xrow];
%size(Xnew)
max(Xnew)
min(Xnew)