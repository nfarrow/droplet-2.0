function [X, y] = create_RNB_data_phi(num_samples)

X = zeros(num_samples,36);
y = zeros(num_samples,1);

RX = [0,0,0];

for ii = 1:num_samples
    %{
    theta = randi(360,1)*pi/180;
    phi = randi(360,1)*pi/180;
    %}
    i = randi(20,1); + (randi(100,1)-50)/100; % last term is the noise term
    
    theta = pi/2;
    phi = (i/(num_samples-1))*(pi/3) + randn(1)/10;
    R = 10;
    
    y(ii) = i;
    
    TX = [R*cos(theta), R*sin(theta), phi];
    
    Lambda = BrightnessMatrix(RX, TX);
    X(ii,:) = reshape(Lambda,1,36);
end





end

