function [X, y, sample_configuration] = create_RNB_data_hard2_NOTRAIN(dim)
% X is the set of 6x6 brightness matricies (unrolled into 1x36 vectors)
% y is the set of correct classes (for this dimension specified by 'dim')
% sample_configuration is the set of exact values given as 
% [R (cm), theta (degrees), phi (degrees)] that regression would
% need to match in order to have 0 MSE.


%% figure out for which dimension we are labeling the cases
% expand the string dim to have 5 chars for string compare
while(length(dim) < 5)
    dim = horzcat(dim,' ');
end
if(dim == 'R    ')
    class_dim = 1;
elseif(dim == 'theta')
    class_dim = 2;
elseif(dim == 'phi  ')
    class_dim = 3;
else
    error('ERROR: unknown dim, dim = "%s"\n', dim); 
end


%% establish the region for which data will be created
angle_intervals = 1;
R_intervals = 2/5;

min_angle = 0 - 2*angle_intervals;
max_angle = 55 + 2*angle_intervals;

absolute_min_angle = -2.5; % (inclusive)
absolute_max_angle = 57.5; % (exclusive)

% CAUTION, min_R is a special case caused by the robots touching
% it does not exist on one of the sub-intervals
absolute_min_R = 4.4; % (inclusive)
absolute_max_R = 26; % (exclusive)

R_set = 5-(2*R_intervals):R_intervals:25+(2*R_intervals);
R_set(1) = 4.4; % handle the 1 weird case

angle_set = min_angle:angle_intervals:max_angle;

% verify our sets conform to the interval rules (important)
if((R_set(:,1) < absolute_min_R)||(R_set(:,1) >= absolute_max_R))
    error('ERROR: R_set invalid!\n');
elseif((angle_set(:,1) < absolute_min_angle)||(angle_set(:,1) >= absolute_max_angle))
    error('ERROR: angle_set invalid!\n');
end

% apply the NOTRAIN condition:
% remove the training examples (the class prototypes)
if(class_dim == 1)
    % remove the R's that are integer valued
    R_set = R_set(R_set ~= round(R_set));
end
if(class_dim == 2)
    % remove the angles that are multiples of 5
    theta_set = angle_set(angle_set/5 ~= round(angle_set/5));
else
    theta_set = angle_set;
end
if(class_dim == 3)
    % remove the angles that are multiples of 5
    phi_set = angle_set(angle_set/5 ~= round(angle_set/5));
else
    phi_set = angle_set;
end
    

%% create variables to store our generated data
num_samples = 12*12*11*(5*5*4); % (5^classes) examples from each class, minus the known training example
num_samples2 = length(R_set)*length(theta_set)*length(phi_set);
if(num_samples ~= num_samples2)
    error('Something went wrong in counting samples %i vs %i', num_samples, num_samples2);
end
X = zeros(num_samples,36);
y = zeros(num_samples,1);
sample_configuration = zeros(num_samples,3);

RX = [0,0,0];


i = 0;
for R = R_set
    for thetad = theta_set
        for phid = phi_set
            
            i = i + 1;
            sample_configuration(i,:) = [R, thetad, phid];
            
            % convert from degrees to radians 
            theta = thetad*pi/180.0;
            phi = phid*pi/180.0;

            TX = [R*cos(theta), R*sin(theta), phi];
            Lambda = BrightnessMatrix(RX, TX);
            X(i,:) = reshape(Lambda,1,36);
        end
    end
end

% verify that the correct number of samples was created
if(i ~= num_samples)
    error('ERROR: wrong number of samples were created, made: %i, need: %i\n', i, num_samples);
end

if(class_dim == 1)
    y = Rtoi(sample_configuration(:,class_dim));
elseif(class_dim == 2)
    y = thetatoi(sample_configuration(:,class_dim),0);
elseif(class_dim == 3)
    y = phitoi(sample_configuration(:,class_dim),0);
end


end

