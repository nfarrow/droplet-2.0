function Display_N_brightness_matricies(X, n)

% Randomly select n matricies to display
sel = randperm(size(X, 1));
sel = sel(1:n);
figure;
displayDataSquares(X(sel, :));

end

