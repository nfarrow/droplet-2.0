function theta = itotheta(i,rad)

% theta cases 0 - 55, i = 1:12
if((max(i) > 12)||(min(i) < 1))
    error('i out of range [theta conv], ERROR');
end
    
theta = (i-1)*5;

if(rad == 1)
    theta = theta*pi/180.0;
end

end

