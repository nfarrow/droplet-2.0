function probs = softmax(activations)

% softmax takes a vector of activations, and makes a vector of probabilities

[m n] = size(activations);

%denominator = ones(m,n);
probs = zeros(m,n);

denominator = repmat(sum(exp(activations'))',1,n);


%for i = 1:m
    
    probs = exp(activations) ./ denominator;
%end

end

