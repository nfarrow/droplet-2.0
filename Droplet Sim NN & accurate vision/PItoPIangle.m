function new_angle = PItoPIangle(angle)
% The input to this function could be a scalar, vector, or matrix

new_angle = mod(angle+pi,2*pi) - pi;

end

