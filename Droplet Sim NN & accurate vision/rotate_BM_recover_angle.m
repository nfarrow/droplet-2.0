function angle = rotate_BM_recover_angle(new_angle, delta_angle)
% this function is part of a triad of functions:
% 1) [newLambda, delta_theta, delta_rho] = rotate_BM(Lambda)
% 2) new_angle = rotate_BM_new_angle(angle, delta_angle)
% 3) angle = rotate_BM_recover_angle(new_angle, delta_angle)
%
% this enforces that recovered angle to be in the range -179 to 180,
% note: new_angle will be in domain -12 to 72

angle = mod(new_angle + delta_angle + 180, 360) - 180;
    
end

