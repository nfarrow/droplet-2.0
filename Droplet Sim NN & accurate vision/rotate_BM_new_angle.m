function new_angle = rotate_BM_new_angle(angle, delta_angle)
% this function is part of a triad of functions:
% 1) [newLambda, delta_theta, delta_rho] = rotate_BM(Lambda)
% 2) new_angle = rotate_BM_new_angle(angle, delta_angle)
% 3) angle = rotate_BM_recover_angle(new_angle, delta_angle)
%
% we assume angle to be in the domanin -179 to 180,
% this will still work without that assumption,
% however, that assumption will still be enforced on the recovered angle
% (this is in reference to rotate_BM_recover_angle() )

% note: new_angle will be in range -12 to 72
new_angle = mod(angle - delta_angle + 180, 360) - 180;
    
end

