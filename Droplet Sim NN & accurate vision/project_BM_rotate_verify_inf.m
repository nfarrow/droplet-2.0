% CAUTION, this function test code generates phis explictly, the new
% version (2) was the first version to switch to the more meaningful rhos

LambdaSum = zeros(6,6);

RX = [0,0,0];

RSum = 0;
phiSum = 0;
thetaSum = 0;

n = 1000;
for i = 1:n
    %phid = 45;
    %R = 15;
    %thetad = 90;

    phid = randi(80,1)-1; % asymmetrical on purpose 
    R = randi(20,1)+5;
    thetad = randi(360,1)-1;

    RSum = RSum + R;
    thetaSum = thetaSum + thetad;
    phiSum = phiSum + phid;
    
    TX = [R*cos(thetad*pi/180), R*sin(thetad*pi/180), phid*pi/180];
    Lambda = BrightnessMatrix(RX, TX);
    [Lambda, ~, ~] = rotate_BM(Lambda);
    
    LambdaSum = LambdaSum + Lambda;
end


figure;
cmap = jet(6);
title(horzcat('(n = ',num2str(n), ...
    ' means) R = ',num2str(RSum/n), ...
    ', theta = ',num2str(thetaSum/n), ...
    ', phi = ',num2str(phiSum/n)),'FontSize',12);
xlabel('emitter number','FontSize',12);
xlim([0.5, 6.5]);
set(gca,'XTick',1:6);
hold on;

for sensor = 1:6
   
    plot(1:6,LambdaSum(sensor,:),'color',cmap(sensor,:));
    
end

colormap(cmap);
hcb = colorbar;
% next two lines recenter the ticks onto the middle of the colors
set(hcb,'YTick',[1:6]+0.5); 
set(hcb,'YTickLabel',[1:6]);
set(get(hcb,'xlabel'),'String', 'sensor number','FontSize',12);

hold off;
