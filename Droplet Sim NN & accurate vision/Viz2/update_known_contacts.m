function bot_mem = update_known_contacts(bot_mem, new_ID);

% figure out if the new_ID is already in the list of known contacts

known_IDs = mem_access(bot_mem, 'known IDs');

if(length(find(known_IDs == new_ID)) == 0)
    % then we dont know about new_ID, save it if possible
    num_known = length(known_IDs);
    if(num_known < 5)
        bot_mem = mem_write(bot_mem, 'new ID', new_ID);
    else
        fprintf('this bot knows to much\n');
    end


end

