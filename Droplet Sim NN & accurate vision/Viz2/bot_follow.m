function [D1new S1new, D1plan] = bot_follow(D1, D2, S1, S2)
% here D1 is interacting with D2, only D1 updates its behavior (if any)
% from this interaction, this is returned as D1new & S1new.

plan_rotate_allowed = 1;

[r, theta] = bot_RNB_exact(D1, D2);




D1new = D1;

D1plan = [0 0];

if(plan_rotate_allowed)
    % this here is planning for rotating the robot
    %if(theta > 0)
    if(theta > 0.3)
        D1plan(1) = 1;
    elseif(theta < -0.3)
        D1plan(1) = -1;
    end
end

% currently not changing the subscription
S1new = S1;



end

