function MEM = mem_write(MEM, datatype, data)

reserved_mempos = 1:5;
known_ID_mempos = 6:10; % arbitrary for now, this should be standardized
known_IDs_know_me_mempos = 11:15; % booleans!
mem_size = 40;

if(strcmp(datatype, 'new ID'))
    num_known = length(mem_access(MEM, 'known IDs'));
    MEM(known_ID_mempos(1)+num_known) = data; % 6 = first memory pos for storing IDs
elseif(strcmp(datatype, 'they know me'))
    write_pos = find(MEM, data) + 5;   % known ID list is at most 5!
    MEM(write_pos) = 1;
end

end

