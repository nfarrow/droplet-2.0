function ans = mem_access(MEM, query)

reserved_mempos = 1:5;
known_ID_mempos = 6:10; % arbitrary for now, this should be standardized
known_IDs_know_me_mempos = 11:15; % booleans!
mem_size = 40;

if(strcmp(query,'known IDs'))
    % return nonzero IDs    
    known_IDs = MEM(known_ID_mempos);
    ans = known_IDs(known_IDs ~= 0);
    
elseif(strcmp(query,'who knows me'))
    indicies = find(MEM(known_IDs_know_me_pos) == 1);
    ans = MEM(known_ID_mempos(1)-1+indicies);
end

end

