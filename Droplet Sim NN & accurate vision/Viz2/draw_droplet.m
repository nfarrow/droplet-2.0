function draw_droplet(Droplet)

x = Droplet(1);
y = Droplet(2);
theta = Droplet(3);


r = 2; % this is the droplet radius

circle(x,y, r);
plot([x, x+r*cos(theta)], [y, y+r*sin(theta)], 'b-');


end

