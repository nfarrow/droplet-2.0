function RXbuff = add_to_buffer(RXbuff, RX_ind, message);

buffer_len = 10;
message_len = length(message);
RXbuff(RX_ind,1:message_len) = message;

end

