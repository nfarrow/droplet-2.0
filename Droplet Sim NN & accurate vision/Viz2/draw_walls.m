function draw_walls(walls)

% this block defined elsewhere
%{
num_walls = 4;
wall = zeros(num_walls,4); % square walls for now
wall(1,:) = [10, 10, 10, -10]; % its endpoints are x,y=10,10 and x,y=10,-10
wall(2,:) = [-10, 10, 10, 10];
wall(3,:) = [-10, -10, -10, 10];
wall(4,:) = [10, -10, -10, -10];
%}

[num_walls m] = size(walls);

for wi = 1:num_walls;
    % [x1 x2],[y1,y2]
    plot([walls(wi,1) walls(wi,3)],[walls(wi,2) walls(wi,4)], 'k');
end


end

