function [RXbuff, Memory] = read_message(RXbuff, Memory, di);

total_message_len = RXbuff(di,1);
TX_ID = RXbuff(di,2);
what_they_want = RXbuff(di,3);

% clear the message
RXbuff(di,1:total_message_len) = zeros(1,total_message_len);

if(what_they_want == 99) % ping
    Memory(di,:) = update_known_contacts(Memory(di,:), TX_ID);
end

end

