function RXbuff = InitRXbuffer(num_bots)

buffer_size = 10;

% -1 will represent an unfilled buffer position
RXbuff = zeros(num_bots, buffer_size);


end

