%vizzion

%addpath('C:\Users\Toshiba\Desktop\Droplet Sim')
addpath('C:\Users\Toshiba\Desktop\CURRENT RESEARCH\Droplet Sim NN & accurate vision')


%% initialize the robots

num_bots = 4;
num_active_bots = 4;
new_conditions = 1;

if(new_conditions)
    rand_list = randn(num_bots,1)+3;
end


[Droplets, Memory] = InitDroplets(num_bots);
RXbuff = InitRXbuffer(num_bots);

% plans are what the droplets want to do on the next time step
Plans = zeros(num_bots,2); % currently plans are (rotate direction, step forward boolean)

% Subscriber List, a subscriber interacts as the RX in a RX TX RNB interaction
max_subscriptions = 2;
Subscription_List = zeros(num_bots,max_subscriptions);
Subscription_List(1,:) = [2, 3];
Subscription_List(2,:) = [3, 2];
Subscription_List(3,:) = [4, 3];
Subscription_List(4,:) = [1, 2];
Subscription_List = randi(4,num_bots,2);


%comm_schedule = [0,0,1,0,0,2,0,0,3,0,0,4];
%comm_schedule = [1,3,2,4];
comm_schedule = 1:num_bots;
comm_period = length(comm_schedule);

num_walls = 5;
wall_list = zeros(num_walls,4); % square walls for now
% walls have to be defined CCW for collisions to work properly (for now)
wall_list(1,:) = [10, 10, 10, -10]*2; % its endpoints are x,y=10,10 and x,y=10,-10
wall_list(2,:) = [-10, 10, 10, 10]*2;
wall_list(3,:) = [-10, -10, -10, 10]*2;
wall_list(4,:) = [10, -10, -10, -10]*2;

draw_size = max(max(abs(wall_list)));




prev_step_Droplets = zeros(size(Droplets));

% obstruction matrix: 
% entry (i,j) == 1 if i & j cannot see each other 
% entry (i,j) == 0 if i & j can see each other
obstruction_mat = zeros(num_bots);

%% setup the drawing

hf = figure;
reset_fig(draw_size);

%delay_time = 0; % just do it
%delay_time = 0.01; % fast
delay_time = 0.05; % THIS IS STANDARD
%delay_time = 1.0; % slow

%% draw the initial configuration

for dTX = 1:num_bots
    draw_droplet(Droplets(dTX,:));
end
 
%% run the simulation
num_timesteps = 100;

for t = 1:num_timesteps

    prev_step_Droplets = Droplets;
    Interaction_List = [];
    
    %% check who gets control of the channel
    schedule_index = mod(t-1,comm_period) + 1;
    channel_allocation = comm_schedule(schedule_index);
    if(channel_allocation > 0)
        fprintf('timestep: %i (%i has the con)\n', t, channel_allocation);
    else
        fprintf('timestep: %i\n', t);
    end
    
    %% robots interact
    if(channel_allocation > 0)
        dTX = channel_allocation;
        
        % NEW: all robots in vicinity get message
        % find out who is in range
        % TODO: probabilistic message reception based on distance and LOS
        RXlist = find_robots_that_can_hear(Droplets, obstruction_mat, dTX);
        
        % figure out what the message the TX robot wants to send
        message = generate_message(dTX, Memory(dTX,:),'ping');
        %message = [3, dTX, 99]; % 99 is just a ping
       
        % place the message in all the RX robots msg buffer
        for dRX = RXlist
        
            if(obstruction_mat(dTX,dRX) == 0)
                RXbuff = add_to_buffer(RXbuff, dRX, message);
                %Plans(dTX,:) = bot_communicate(Droplets(dTX,:), Droplets(dRX,:), Plans(dTX,:));
            end
            
            Interaction_List = [Interaction_List; [dTX, dRX, obstruction_mat(dTX,dRX)]];
            
        end
        
    end

    % robots check their message buffers and update thier memory if applc.
    [RXbuff, Memory] = all_read_message(RXbuff, Memory);
    
    %{
    %% drive the robots
    for dTX = 1:num_active_bots
        [Droplets(dTX,:), Plans(dTX,:)] = bot_drive(Droplets(dTX,:), 2, Plans(dTX,:));
        Droplets = check_wall_collision(Droplets, wall_list);
        Droplets = check_droplet_collision(Droplets, prev_step_Droplets);

    end
    %}
    
    %% draw the new state
    reset_fig(draw_size);

    draw_walls(wall_list);

    for dTX = 1:num_bots
        draw_droplet(Droplets(dTX,:));
    end

    draw_interaction(Droplets, Interaction_List);
    
    obstruction_mat = check_LOS_collision_all(Droplets); % this draws too!

    
    pause(delay_time);
    
end