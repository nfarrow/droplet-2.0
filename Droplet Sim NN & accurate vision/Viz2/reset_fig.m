function reset_fig(draw_size)

clf;

plot_arena_size = draw_size*1.1;
%plot_arena_size = 15;

axmin = -plot_arena_size;
axmax = plot_arena_size;
aymin = -plot_arena_size;
aymax = plot_arena_size;
axis equal; % spacing the same in both dimensions
axis([axmin axmax aymin aymax]);
hold on;



end

