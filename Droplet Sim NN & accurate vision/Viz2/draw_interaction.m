%function draw_interaction(D1, D2, S1, S2, obstruction)
%function draw_interaction(Droplets, D1ind, D2ind, obstruction_mat)
function draw_interaction(Droplets, Interact_List)

[num_interactions, ~] = size(Interact_List);

% a green line is drawn in the figure to indicate an sucessful interaction.
% a red line is drawn in the figure to indicate an failed interaction.

for i = 1:num_interactions
    D1x = Droplets(Interact_List(i,1),1);
    D1y = Droplets(Interact_List(i,1),2);

    D2x = Droplets(Interact_List(i,2),1);
    D2y = Droplets(Interact_List(i,2),2);

    if(Interact_List(i,3) == 0)
        plot([D1x D2x],[D1y D2y], 'g');
    else
        plot([D1x D2x],[D1y D2y], 'r');
    end
end

end

