function message = generate_message(TX_ID, MEM, msg_type)

buffer_size = 10; % also defined in InitRXbuffer()

message = [buffer_size, TX_ID]; % placeholder length

% 99
if(strcmp(msg_type, 'ping'))
    message = [message, 99];
    message = [message, mem_access(MEM, 'known IDs')];
end

message(1) = length(message);

end

