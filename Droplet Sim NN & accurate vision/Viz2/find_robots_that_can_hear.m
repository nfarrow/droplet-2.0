function RXlist = find_robots_that_can_hear(Dlist, obstruction_mat, TXind);

msg_dist = 120;

[num_bots, ~] = size(Dlist);

possibleRXlist = 1:num_bots;
% take out the TX droplet
possibleRXlist = possibleRXlist(possibleRXlist ~= TXind);

distances = pdist2(Dlist(TXind,1:2),Dlist(possibleRXlist,1:2),'euclidean');
    
% cut down the list based on distance
possibleRXlist = possibleRXlist(distances < msg_dist);

RXlist = possibleRXlist;

end

