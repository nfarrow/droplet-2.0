%project_test_intersect



addpath('C:\Users\Toshiba\Desktop\Droplet Sim');


reset_fig();

num_lines = 2;
line = zeros(num_lines,4);


line(1,:) = [5, 5, 5, -5]; 
line(2,:) = [0,0, 6, 0];

% false negative:
%{
line(1,:) = [-10, -10, 10, 10]; 
line(2,:) = [10, 1, 1, 10];
%}

for wi = 1:num_lines;
    % [x1 x2],[y1,y2]
    plot([line(wi,1) line(wi,3)],[line(wi,2) line(wi,4)], 'b');
end

[intersect, intersect_x, intersect_y, t] = check_intersect(line(1,:), line(2,:));

if(intersect)
    fprintf('intersect at x=%f, y=%f, t=%f\n', intersect_x, intersect_y, t);
else
    fprintf('no intersection\n');
end












