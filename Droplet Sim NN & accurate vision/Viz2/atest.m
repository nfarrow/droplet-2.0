a=7
f=10
T=1/f;
v=40
wl=v/f;
x1=1;
x2=30
step=0.001

t=x1:step:x2;
x=x1:step:x2;
y=a*sind(2*pi*f*(t+(x*T)/wl)); 
h=plot3(x,y,t);
set(h,'EraseMode','xor','MarkerSize',20)
xlabel('displacement(m)');
ylabel('amplitude(m)');
title('Wave Animation');
for t=x1:step:x2
drawnow
y=a*sind(2*pi*f*(t+(x*T)/wl)); 
set(h,'YData',y)
pause(0.1)
end