function [RXbuff, Memory] = all_read_message(RXbuff, Memory)

[num_bots, buffer_len] = size(RXbuff); 

for RX_ID = 1:num_bots
    %RXbuff(RX_ID,:) % show the message?
    if(RXbuff(RX_ID,1) ~= 0) % if your message buffer is not empty
        total_message_len = RXbuff(RX_ID,1);
        TX_ID = RXbuff(RX_ID,2);
        what_they_want = RXbuff(RX_ID,3);


        if(what_they_want == 99) % ping
            Memory(RX_ID,:) = update_known_contacts(Memory(RX_ID,:), TX_ID);
            their_known_list = RXbuff(RX_ID,4:total_message_len);
            if(any(their_known_list == RX_ID))
                %fprintf('%i knows %i\n', TX_ID, RX_ID);
                Memory(RX_ID,:) = mem_write(Memory(RX_ID,:), 'they know me', TX_ID);
            end
        end

        % done processing, clear the message
        RXbuff(RX_ID,1:total_message_len) = zeros(1,total_message_len);
    end
end

end

