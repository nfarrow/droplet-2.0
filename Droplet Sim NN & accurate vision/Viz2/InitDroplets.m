function [Dlist, Mlist] = InitDroplets(num_robots)

max_bots = 4;
prototype_positisions = zeros(max_bots,3);
prototype_positions(1,:) = [5, 5, -pi];
prototype_positions(2,:) = [5, -7, -pi];
prototype_positions(3,:) = [0, 0, 0];
prototype_positions(4,:) = [-5, -5, 0];

if(num_robots > 4)
    error('max 4 robots are supported');
end

Dlist = prototype_positions(1,:);
for i = 2:min(num_robots, max_bots)
    next_bot = prototype_positions(i,:);
    Dlist = [Dlist; next_bot];
end

mem_size = 40;
Mlist = zeros(num_robots,mem_size);


end

