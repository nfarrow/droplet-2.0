function S = SensorModel(angle)
%SENSORMODEL


S = cos(angle);

% clip negative values to zero
S(find(S < 0)) = 0;


end

