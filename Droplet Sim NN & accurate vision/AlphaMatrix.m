function AMat = AlphaMatrix(RX, TX)
%ALPHAMATRIX returns a [6 6] matrix of all the alpha angles (in radians)
% rows coorespond to sensors
% columns coorespond to emitters
%

% alpha = angle between normal vector to sensor and sensor to emitter
% vector

Print_Alpha_Matrix = 0; % this AlphaMatrix function has been VERIFIED to work

iTOj = RXitoTXjVectorsCX(RX, TX);

RXUnitNormalsCX = UnitNormalsCX(RX(3));

AMat = zeros(6);

for s = 1:6
    for e = 1:6
        AMat(s,e) = angle(iTOj(s,e)) - angle(RXUnitNormalsCX(s));
    end
end

%AMat = mod(AMat+pi,2*pi) - pi;
AMat = PItoPIangle(AMat);

if(Print_Alpha_Matrix)
    fprintf('Alpha Matrix (in degrees):\n');
    fprintf('      e=1      e=2      e=3      e=4      e=5      e=6\n');
    for s = 1:6
        fprintf('s=%i',s);
        for e = 1:6
            val_to_print = AMat(s,e)*180/pi;
            if(val_to_print > 0)
                fprintf(' ');
            end
            if(abs(val_to_print) < 10)  
                fprintf(' ');
            end
            if(abs(val_to_print) < 100)  
                fprintf(' ');
            end
            
            fprintf('%3.3f ', val_to_print);
        end
        fprintf('\n');
    end
end






end

