function X_as_row = Pack_BM(Lambda)
% This function converts a 2D [6 6] matrirx to a single row [1 36]
% The first COLUMN of the original matrix occupies the first 6 elements of
% the stored row, etc.  This may be confusing, so use Unpack_BM to convert 
% back into the traditional form  

X_as_row = reshape(Lambda,1,36);


end

