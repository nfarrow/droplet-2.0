function A = InverseAmplitudeModel(r)

% this amplitude model (1/r) happens to be its own inverse

A = 1./r;

end

