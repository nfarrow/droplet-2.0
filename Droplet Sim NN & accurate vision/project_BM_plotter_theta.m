
RX = [0,0,0];

cmap = cool(61);

for sensor = 1:6

    figure;
    title(horzcat('effect of theta (sensor ',num2str(sensor),')'),'FontSize',12);
    xlabel('emitter number','FontSize',12);
    xlim([0.5, 6.5]);
    set(gca,'XTick',1:6);
    
    hold on;

    for thetad = 0:1:60

        R = 5;
        phid = 0;

        TX = [R*cos(thetad*pi/180), R*sin(thetad*pi/180), phid*pi/180];

        Lambda = BrightnessMatrix(RX, TX);
        
        plot(1:6,Lambda(sensor,:),'color',cmap(thetad+1,:));
                
    end

    colormap(cmap);
    colorbar;
    
    hold off;
    
end