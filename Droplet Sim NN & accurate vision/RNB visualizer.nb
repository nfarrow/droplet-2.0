(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 8.0' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       157,          7]
NotebookDataLength[     23198,        554]
NotebookOptionsPosition[     22448,        526]
NotebookOutlinePosition[     22871,        542]
CellTagsIndexPosition[     22828,        539]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{

Cell[CellGroupData[{
Cell[BoxData[
 RowBox[{"botR", "=", "2.2", " ", 
  RowBox[{"(*", " ", 
   RowBox[{
    RowBox[{
    "this", " ", "is", " ", "only", " ", "needed", " ", "for", " ", "drawing",
      " ", "the", " ", "robots"}], ",", " ", 
    RowBox[{
    "actual", " ", "algorithm", " ", "depends", " ", "on", " ", "sensorR"}]}],
    " ", "*)"}]}]], "Input",
 CellChangeTimes->{{3.571772263654125*^9, 3.57177226385725*^9}, {
  3.6073681497655177`*^9, 3.6073681860862083`*^9}}],

Cell[BoxData["2.2`"], "Output",
 CellChangeTimes->{3.571765830060375*^9, 3.57177226454475*^9, 
  3.571772346029125*^9, 3.57177906235725*^9, 3.571782495841625*^9, 
  3.5717859066034417`*^9, 3.571851332082*^9, 3.5718954072571154`*^9, 
  3.5719462423877487`*^9, 3.571947190851573*^9, 3.5719526960673532`*^9, 
  3.571960478255464*^9, 3.6048750598544044`*^9, 3.607367836199003*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"(*", 
   RowBox[{
    RowBox[{
    "This", " ", "is", " ", "just", " ", "a", " ", "Graphics", " ", "object", 
     " ", "depicting", " ", "a", " ", 
     RowBox[{"droplet", ":", " ", 
      RowBox[{"circular", " ", "outline"}]}]}], ",", " ", 
    RowBox[{"line", " ", 
     RowBox[{"pointing", " ", "'"}], 
     RowBox[{"forward", "'"}]}], ",", " ", 
    RowBox[{"and", " ", "the", " ", "IR", " ", 
     RowBox[{"sensors", "."}]}]}], "*)"}], "\[IndentingNewLine]", 
  RowBox[{
   RowBox[{
    RowBox[{
     RowBox[{"DropletGRAPHIC", "[", 
      RowBox[{"x_", ",", "y_", ",", "\[Alpha]_", ",", "color_"}], "]"}], ":=",
      "\[IndentingNewLine]", 
     RowBox[{"Graphics", "[", "\[IndentingNewLine]", 
      RowBox[{
       RowBox[{"{", "\[IndentingNewLine]", 
        RowBox[{"Black", ",", 
         RowBox[{"Circle", "[", 
          RowBox[{
           RowBox[{"{", 
            RowBox[{"x", ",", "y"}], "}"}], ",", "botR"}], "]"}], ",", 
         "\[IndentingNewLine]", "Thick", ",", "Black", ",", 
         RowBox[{"Line", "[", 
          RowBox[{"{", 
           RowBox[{
            RowBox[{"{", 
             RowBox[{"x", ",", "y"}], "}"}], ",", 
            RowBox[{"{", 
             RowBox[{
              RowBox[{"x", "+", 
               RowBox[{"botR", " ", 
                RowBox[{"Cos", "[", "\[Alpha]", "]"}]}]}], ",", 
              RowBox[{"y", "+", 
               RowBox[{"botR", " ", 
                RowBox[{"Sin", "[", "\[Alpha]", "]"}]}]}]}], "}"}]}], "}"}], 
          "]"}]}], "\[IndentingNewLine]", "}"}], "~", "Join", "~", 
       "\[IndentingNewLine]", 
       RowBox[{"Table", "[", 
        RowBox[{
         RowBox[{"{", "\[IndentingNewLine]", 
          RowBox[{"color", ",", " ", 
           RowBox[{"Polygon", "[", 
            RowBox[{"{", 
             RowBox[{
              RowBox[{"{", 
               RowBox[{
                RowBox[{"x", "+", 
                 RowBox[{
                  RowBox[{"(", 
                   RowBox[{"sensorR", "-", ".1"}], ")"}], 
                  RowBox[{"Cos", "[", 
                   RowBox[{"\[Alpha]", "-", 
                    RowBox[{"(", 
                    RowBox[{"i", "-", 
                    RowBox[{"\[Pi]", "/", "24"}]}], ")"}]}], "]"}]}]}], ",", 
                RowBox[{"y", "+", 
                 RowBox[{
                  RowBox[{"(", 
                   RowBox[{"sensorR", "-", ".1"}], ")"}], 
                  RowBox[{"Sin", "[", 
                   RowBox[{"\[Alpha]", "-", 
                    RowBox[{"(", 
                    RowBox[{"i", "-", 
                    RowBox[{"\[Pi]", "/", "24"}]}], ")"}]}], "]"}]}]}]}], 
               "}"}], ",", "\[IndentingNewLine]", "\t\t\t", 
              RowBox[{"{", 
               RowBox[{
                RowBox[{"x", "+", 
                 RowBox[{
                  RowBox[{"(", 
                   RowBox[{"sensorR", "-", ".1"}], ")"}], 
                  RowBox[{"Cos", "[", 
                   RowBox[{"\[Alpha]", "-", 
                    RowBox[{"(", 
                    RowBox[{"i", "+", 
                    RowBox[{"\[Pi]", "/", "24"}]}], ")"}]}], "]"}]}]}], ",", 
                RowBox[{"y", "+", 
                 RowBox[{
                  RowBox[{"(", 
                   RowBox[{"sensorR", "-", ".1"}], ")"}], " ", 
                  RowBox[{"Sin", "[", 
                   RowBox[{"\[Alpha]", "-", 
                    RowBox[{"(", 
                    RowBox[{"i", "+", 
                    RowBox[{"\[Pi]", "/", "24"}]}], ")"}]}], "]"}]}]}]}], 
               "}"}], ",", "\[IndentingNewLine]", "\t\t\t", 
              RowBox[{"{", 
               RowBox[{
                RowBox[{"x", "+", 
                 RowBox[{
                  RowBox[{"(", 
                   RowBox[{"sensorR", "+", ".1"}], ")"}], " ", 
                  RowBox[{"Cos", "[", 
                   RowBox[{"\[Alpha]", "-", 
                    RowBox[{"(", 
                    RowBox[{"i", "+", 
                    RowBox[{"\[Pi]", "/", "48"}]}], ")"}]}], "]"}]}]}], ",", 
                RowBox[{"y", "+", 
                 RowBox[{
                  RowBox[{"(", 
                   RowBox[{"sensorR", "+", ".1"}], ")"}], 
                  RowBox[{"Sin", "[", 
                   RowBox[{"\[Alpha]", "-", 
                    RowBox[{"(", 
                    RowBox[{"i", "+", 
                    RowBox[{"\[Pi]", "/", "48"}]}], ")"}]}], "]"}]}]}]}], 
               "}"}], ",", "\[IndentingNewLine]", "\t\t\t", 
              RowBox[{"{", 
               RowBox[{
                RowBox[{"x", "+", 
                 RowBox[{
                  RowBox[{"(", 
                   RowBox[{"sensorR", "+", ".1"}], ")"}], " ", 
                  RowBox[{"Cos", "[", 
                   RowBox[{"\[Alpha]", "-", 
                    RowBox[{"(", 
                    RowBox[{"i", "-", 
                    RowBox[{"\[Pi]", "/", "48"}]}], ")"}]}], "]"}]}]}], ",", 
                RowBox[{"y", "+", 
                 RowBox[{
                  RowBox[{"(", 
                   RowBox[{"sensorR", "+", ".1"}], ")"}], " ", 
                  RowBox[{"Sin", "[", 
                   RowBox[{"\[Alpha]", "-", 
                    RowBox[{"(", 
                    RowBox[{"i", "-", 
                    RowBox[{"\[Pi]", "/", "48"}]}], ")"}]}], "]"}]}]}]}], 
               "}"}]}], "\[IndentingNewLine]", "}"}], "]"}], ",", 
           "\[IndentingNewLine]", "Black", ",", 
           RowBox[{"Inset", "[", 
            RowBox[{
             RowBox[{
              RowBox[{"(", 
               RowBox[{"i", "/", 
                RowBox[{"(", 
                 RowBox[{"\[Pi]", "/", "3"}], ")"}]}], ")"}], "+", 
              RowBox[{"1", "/", "2"}]}], ",", 
             RowBox[{"{", 
              RowBox[{
               RowBox[{"x", "+", 
                RowBox[{
                 RowBox[{"(", 
                  RowBox[{"sensorR", "-", "0.2"}], ")"}], 
                 RowBox[{"Cos", "[", 
                  RowBox[{"\[Alpha]", "-", "i"}], "]"}]}]}], ",", 
               RowBox[{"y", "+", 
                RowBox[{
                 RowBox[{"(", 
                  RowBox[{"sensorR", "-", "0.2"}], ")"}], 
                 RowBox[{"Sin", "[", 
                  RowBox[{"\[Alpha]", "-", "i"}], "]"}]}]}]}], "}"}]}], 
            "]"}]}], "\[IndentingNewLine]", "}"}], ",", 
         RowBox[{"{", 
          RowBox[{"i", ",", 
           RowBox[{"\[Pi]", "/", "6"}], ",", 
           RowBox[{"11", 
            RowBox[{"\[Pi]", "/", "6"}]}], ",", 
           RowBox[{"\[Pi]", "/", "3"}]}], "}"}]}], "]"}]}], 
      "\[IndentingNewLine]", "]"}]}], ";"}], "\[IndentingNewLine]", 
   RowBox[{"DropletGRAPHIC", "[", 
    RowBox[{"0", ",", "0", ",", "0", ",", "Red"}], "]"}], 
   RowBox[{"(*", " ", 
    RowBox[{"basic", " ", "example"}], " ", "*)"}]}]}]], "Input",
 CellChangeTimes->{{3.571765792060375*^9, 3.571765792529125*^9}, 
   3.571772245185375*^9, {3.6073780603675737`*^9, 3.6073780935390873`*^9}, {
   3.6073781288460245`*^9, 3.607378157514639*^9}}],

Cell[BoxData[
 GraphicsBox[{
   {GrayLevel[0], CircleBox[{0, 0}, 2.2]}, 
   {GrayLevel[0], Thickness[Large], LineBox[{{0, 0}, {2.2, 0.}}], {
     {RGBColor[1, 0, 0], 
      PolygonBox[{{1.7368935211212189`, -0.7194448528463687}, {
       1.491504279747522, -1.1444714865363947`}, {
       1.7294567935892942`, -1.1555860846807726`}, {
       1.8654953023879917`, -0.9199604756555227}}]}, 
     {GrayLevel[0], InsetBox["1", {1.5415252187363007`, -0.89}]}}, {
     {RGBColor[1, 0, 0], 
      PolygonBox[{{
       0.24538924137369694`, -1.8639163393827634`}, {-0.24538924137369694`, \
-1.8639163393827634`}, {-0.13603850879869758`, -2.0755465603362953`}, {
       0.13603850879869758`, -2.0755465603362953`}}]}, 
     {GrayLevel[0], InsetBox["2", {0., -1.78}]}}, {
     {RGBColor[1, 0, 0], 
      PolygonBox[{{-1.491504279747522, -1.1444714865363947`}, \
{-1.7368935211212189`, -0.7194448528463687}, {-1.8654953023879917`, \
-0.9199604756555227}, {-1.7294567935892942`, -1.1555860846807726`}}]}, 
     {GrayLevel[0], InsetBox["3", {-1.5415252187363007`, -0.89}]}}, {
     {RGBColor[1, 0, 0], 
      PolygonBox[{{-1.7368935211212189`, 
       0.7194448528463687}, {-1.491504279747522, 
       1.1444714865363947`}, {-1.7294567935892942`, 
       1.1555860846807726`}, {-1.8654953023879917`, 0.9199604756555227}}]}, 
     {GrayLevel[0], InsetBox["4", {-1.5415252187363007`, 0.89}]}}, {
     {RGBColor[1, 0, 0], 
      PolygonBox[{{-0.24538924137369694`, 1.8639163393827634`}, {
       0.24538924137369694`, 1.8639163393827634`}, {0.13603850879869758`, 
       2.0755465603362953`}, {-0.13603850879869758`, 2.0755465603362953`}}]}, 
     {GrayLevel[0], InsetBox["5", {0., 1.78}]}}, {
     {RGBColor[1, 0, 0], 
      PolygonBox[{{1.491504279747522, 1.1444714865363947`}, {
       1.7368935211212189`, 0.7194448528463687}, {1.8654953023879917`, 
       0.9199604756555227}, {1.7294567935892942`, 1.1555860846807726`}}]}, 
     {GrayLevel[0], InsetBox["6", {1.5415252187363007`, 0.89}]}}}}]], "Output",
 CellChangeTimes->{
  3.607378060943901*^9, {3.6073781321414347`*^9, 3.6073781584360027`*^9}}]
}, Open  ]],

Cell[BoxData[
 RowBox[{
  RowBox[{"RxTxPairLineGRAPHIC", "[", 
   RowBox[{
    RowBox[{"{", 
     RowBox[{"RxDropletX_", ",", "RxDropletY_"}], "}"}], ",", 
    "RxDroplet\[Theta]_", ",", 
    RowBox[{"{", 
     RowBox[{"TxDropletX_", ",", "TxDropletY_"}], "}"}], ",", 
    "TxDroplet\[Theta]_", ",", "EmitterNum_", ",", "SensorNum_"}], "]"}], ":=", 
  RowBox[{"Graphics", "[", 
   RowBox[{"Line", "[", 
    RowBox[{"{", "\[IndentingNewLine]", 
     RowBox[{
      RowBox[{
       RowBox[{"{", 
        RowBox[{"RxDropletX", ",", "RxDropletY"}], "}"}], "+", 
       RowBox[{"sensorR", " ", 
        RowBox[{"RxNormalUnit", "[", 
         RowBox[{"RxDroplet\[Theta]", ",", "SensorNum"}], "]"}]}]}], ",", 
      "\[IndentingNewLine]", 
      RowBox[{
       RowBox[{"{", 
        RowBox[{"RxDropletX", ",", "RxDropletY"}], "}"}], "+", 
       RowBox[{"sensorR", " ", 
        RowBox[{"RxNormalUnit", "[", 
         RowBox[{"RxDroplet\[Theta]", ",", "SensorNum"}], "]"}]}], "+", 
       RowBox[{"RxTxPairVector", "[", 
        RowBox[{
         RowBox[{"{", 
          RowBox[{"RxDropletX", ",", "RxDropletY"}], "}"}], ",", 
         "RxDroplet\[Theta]", ",", 
         RowBox[{"{", 
          RowBox[{"TxDropletX", ",", "TxDropletY"}], "}"}], ",", 
         "TxDroplet\[Theta]", ",", "EmitterNum", ",", "SensorNum"}], 
        "]"}]}]}], "\[IndentingNewLine]", "}"}], "]"}], "]"}]}]], "Input",
 CellChangeTimes->{{3.571765947841625*^9, 3.571765977560375*^9}}],

Cell[BoxData[
 RowBox[{
  RowBox[{"DashedVectorsGRAPHIC", "[", 
   RowBox[{"\[Theta]_", ",", 
    RowBox[{"{", 
     RowBox[{"x_", ",", "y_"}], "}"}], ",", "color_"}], "]"}], ":=", 
  RowBox[{"Graphics", "[", 
   RowBox[{"{", 
    RowBox[{"color", ",", "Dashed", ",", 
     RowBox[{"Line", "[", 
      RowBox[{"Table", "[", "\[IndentingNewLine]", 
       RowBox[{
        RowBox[{"{", 
         RowBox[{
          RowBox[{"{", 
           RowBox[{"x", ",", "y"}], "}"}], ",", 
          RowBox[{
           RowBox[{"{", 
            RowBox[{"x", ",", "y"}], "}"}], "+", 
           RowBox[{"sensorR", " ", "2", " ", 
            RowBox[{"TxNormalUnit", "[", 
             RowBox[{"\[Theta]", ",", "EmitterNum"}], "]"}]}]}]}], "}"}], 
        "\[IndentingNewLine]", ",", 
        RowBox[{"{", 
         RowBox[{"EmitterNum", ",", "0", ",", "5"}], "}"}]}], "]"}], "]"}]}], 
    "}"}], "]"}]}]], "Input",
 CellChangeTimes->{{3.57176606385725*^9, 3.5717660741385*^9}, {
  3.571766145279125*^9, 3.571766147654125*^9}, {3.57176621941975*^9, 
  3.5717662213885*^9}, {3.571766264716625*^9, 3.571766264904125*^9}, {
  3.57194961680993*^9, 3.5719496299988947`*^9}}],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"With", "[", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{
     RowBox[{"RxDroplet\[Theta]", " ", "=", " ", "0"}], ",", 
     RowBox[{"RxDropletX", " ", "=", " ", "0"}], ",", 
     RowBox[{"RxDropletY", " ", "=", " ", "0"}]}], "}"}], ",", 
   "\[IndentingNewLine]", 
   RowBox[{"Manipulate", "[", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{
       RowBox[{"Show", "[", "\[IndentingNewLine]", 
        RowBox[{
         RowBox[{"DropletGRAPHIC", "[", 
          RowBox[{
          "TxDropletX", ",", "TxDropletY", ",", "TxDroplet\[Theta]", ",", 
           "Green"}], "]"}], ",", "\[IndentingNewLine]", 
         RowBox[{"DashedVectorsGRAPHIC", "[", 
          RowBox[{"TxDroplet\[Theta]", ",", 
           RowBox[{"{", 
            RowBox[{"TxDropletX", ",", "TxDropletY"}], "}"}], ",", "Green"}], 
          "]"}], ",", "\[IndentingNewLine]", 
         RowBox[{"DropletGRAPHIC", "[", 
          RowBox[{
          "RxDropletX", ",", "RxDropletY", ",", "RxDroplet\[Theta]", ",", 
           "Red"}], "]"}], ",", "\[IndentingNewLine]", 
         RowBox[{"DashedVectorsGRAPHIC", "[", 
          RowBox[{"RxDroplet\[Theta]", ",", 
           RowBox[{"{", 
            RowBox[{"RxDropletX", ",", "RxDropletY"}], "}"}], ",", "Red"}], 
          "]"}], ",", "\[IndentingNewLine]", 
         RowBox[{"RxTxPairLineGRAPHIC", "[", 
          RowBox[{
           RowBox[{"{", 
            RowBox[{"RxDropletX", ",", "RxDropletY"}], "}"}], ",", 
           "RxDroplet\[Theta]", ",", 
           RowBox[{"{", 
            RowBox[{"TxDropletX", ",", "TxDropletY"}], "}"}], ",", 
           "TxDroplet\[Theta]", ",", "EmitterNum", ",", "SensorNum"}], "]"}], 
         ",", "\[IndentingNewLine]", 
         RowBox[{"PlotRange", "\[Rule]", 
          RowBox[{"{", 
           RowBox[{
            RowBox[{"{", 
             RowBox[{
              RowBox[{"-", "5"}], ",", "25"}], "}"}], ",", 
            RowBox[{"{", 
             RowBox[{
              RowBox[{"-", "5"}], ",", "25"}], "}"}]}], "}"}]}], ",", 
         RowBox[{"ImageSize", "\[Rule]", "Medium"}], ",", 
         RowBox[{"Axes", "\[Rule]", "True"}]}], "\[IndentingNewLine]", "]"}], 
       ",", "\[IndentingNewLine]", 
       RowBox[{"{", 
        RowBox[{"\"\<Tx\[Beta]\>\"", ",", 
         RowBox[{
          RowBox[{"Tx\[Beta]", "[", 
           RowBox[{
            RowBox[{"{", 
             RowBox[{"RxDropletX", ",", "RxDropletY"}], "}"}], ",", 
            "RxDroplet\[Theta]", ",", 
            RowBox[{"{", 
             RowBox[{"TxDropletX", ",", "TxDropletY"}], "}"}], ",", 
            "TxDroplet\[Theta]", ",", "EmitterNum", ",", "SensorNum"}], "]"}],
           "/", "Degree"}]}], "}"}], ",", "\[IndentingNewLine]", 
       RowBox[{"{", 
        RowBox[{"\"\<Rx\[Alpha]\>\"", ",", 
         RowBox[{
          RowBox[{"Rx\[Alpha]", "[", 
           RowBox[{
            RowBox[{"{", 
             RowBox[{"RxDropletX", ",", "RxDropletY"}], "}"}], ",", 
            "RxDroplet\[Theta]", ",", 
            RowBox[{"{", 
             RowBox[{"TxDropletX", ",", "TxDropletY"}], "}"}], ",", 
            "TxDroplet\[Theta]", ",", "EmitterNum", ",", "SensorNum"}], "]"}],
           "/", "Degree"}]}], "}"}]}], "\[IndentingNewLine]", "}"}], ",", 
     "\[IndentingNewLine]", 
     RowBox[{"{", 
      RowBox[{
       RowBox[{"{", 
        RowBox[{"TxDroplet\[Theta]", ",", "Standard\[Theta]"}], "}"}], ",", 
       "0", ",", 
       RowBox[{"2", "\[Pi]"}]}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{
       RowBox[{"{", 
        RowBox[{"TxDropletX", ",", "StandardX"}], "}"}], ",", "0", ",", 
       "20"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{
       RowBox[{"{", 
        RowBox[{"TxDropletY", ",", "StandardY"}], "}"}], ",", "0", ",", 
       "20"}], "}"}], ",", "\[IndentingNewLine]", 
     RowBox[{"{", 
      RowBox[{"EmitterNum", ",", "1", ",", "6", ",", "1"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"SensorNum", ",", "1", ",", "6", ",", "1"}], "}"}]}], 
    "\[IndentingNewLine]", "]"}]}], "]"}]], "Input",
 CellChangeTimes->{
  3.571765997122875*^9, {3.5717661591385*^9, 3.571766193466625*^9}, {
   3.5717662363885*^9, 3.57176624279475*^9}, {3.57176682766975*^9, 
   3.571766868826*^9}, {3.5717722027635*^9, 3.5717722283885*^9}, {
   3.571772355154125*^9, 3.57177236904475*^9}, {3.57177242723225*^9, 
   3.571772430747875*^9}, {3.571772753029125*^9, 3.571772758779125*^9}, {
   3.571785917849698*^9, 3.571785934453545*^9}, {3.571949649186485*^9, 
   3.571949661333952*^9}, {3.6073783120850244`*^9, 3.6073783196770105`*^9}}],

Cell[BoxData[
 TagBox[
  StyleBox[
   DynamicModuleBox[{$CellContext`EmitterNum$$ = 6, $CellContext`SensorNum$$ =
     1, $CellContext`TxDropletX$$ = 2, $CellContext`TxDropletY$$ = 
    5, $CellContext`TxDroplet\[Theta]$$ = 3, Typeset`show$$ = True, 
    Typeset`bookmarkList$$ = {}, Typeset`bookmarkMode$$ = "Menu", 
    Typeset`animator$$, Typeset`animvar$$ = 1, Typeset`name$$ = 
    "\"untitled\"", Typeset`specs$$ = {{{
       Hold[$CellContext`TxDroplet\[Theta]$$], 3}, 0, 2 Pi}, {{
       Hold[$CellContext`TxDropletX$$], 2}, 0, 20}, {{
       Hold[$CellContext`TxDropletY$$], 5}, 0, 20}, {
      Hold[$CellContext`EmitterNum$$], 1, 6, 1}, {
      Hold[$CellContext`SensorNum$$], 1, 6, 1}}, Typeset`size$$ = {
    620., {178., 183.}}, Typeset`update$$ = 0, Typeset`initDone$$, 
    Typeset`skipInitDone$$ = True, $CellContext`TxDroplet\[Theta]$23467$$ = 
    0, $CellContext`TxDropletX$23468$$ = 0, $CellContext`TxDropletY$23469$$ = 
    0, $CellContext`EmitterNum$23470$$ = 0, $CellContext`SensorNum$23471$$ = 
    0}, 
    DynamicBox[Manipulate`ManipulateBoxes[
     1, StandardForm, 
      "Variables" :> {$CellContext`EmitterNum$$ = 1, $CellContext`SensorNum$$ = 
        1, $CellContext`TxDropletX$$ = 2, $CellContext`TxDropletY$$ = 
        5, $CellContext`TxDroplet\[Theta]$$ = 3}, "ControllerVariables" :> {
        Hold[$CellContext`TxDroplet\[Theta]$$, \
$CellContext`TxDroplet\[Theta]$23467$$, 0], 
        Hold[$CellContext`TxDropletX$$, $CellContext`TxDropletX$23468$$, 0], 
        Hold[$CellContext`TxDropletY$$, $CellContext`TxDropletY$23469$$, 0], 
        Hold[$CellContext`EmitterNum$$, $CellContext`EmitterNum$23470$$, 0], 
        Hold[$CellContext`SensorNum$$, $CellContext`SensorNum$23471$$, 0]}, 
      "OtherVariables" :> {
       Typeset`show$$, Typeset`bookmarkList$$, Typeset`bookmarkMode$$, 
        Typeset`animator$$, Typeset`animvar$$, Typeset`name$$, 
        Typeset`specs$$, Typeset`size$$, Typeset`update$$, Typeset`initDone$$,
         Typeset`skipInitDone$$}, "Body" :> {
        Show[
         $CellContext`DropletGRAPHIC[$CellContext`TxDropletX$$, \
$CellContext`TxDropletY$$, $CellContext`TxDroplet\[Theta]$$, Green], 
         $CellContext`DashedVectorsGRAPHIC[$CellContext`TxDroplet\[Theta]$$, \
{$CellContext`TxDropletX$$, $CellContext`TxDropletY$$}, Green], 
         $CellContext`DropletGRAPHIC[0, 0, 0, Red], 
         $CellContext`DashedVectorsGRAPHIC[0, {0, 0}, Red], 
         $CellContext`RxTxPairLineGRAPHIC[{0, 0}, 
          0, {$CellContext`TxDropletX$$, $CellContext`TxDropletY$$}, \
$CellContext`TxDroplet\[Theta]$$, $CellContext`EmitterNum$$, \
$CellContext`SensorNum$$], PlotRange -> {{-5, 25}, {-5, 25}}, ImageSize -> 
         Medium, Axes -> True], {
        "Tx\[Beta]", $CellContext`Tx\[Beta][{0, 0}, 
           0, {$CellContext`TxDropletX$$, $CellContext`TxDropletY$$}, \
$CellContext`TxDroplet\[Theta]$$, $CellContext`EmitterNum$$, \
$CellContext`SensorNum$$]/Degree}, {
        "Rx\[Alpha]", $CellContext`Rx\[Alpha][{0, 0}, 
           0, {$CellContext`TxDropletX$$, $CellContext`TxDropletY$$}, \
$CellContext`TxDroplet\[Theta]$$, $CellContext`EmitterNum$$, \
$CellContext`SensorNum$$]/Degree}}, 
      "Specifications" :> {{{$CellContext`TxDroplet\[Theta]$$, 3}, 0, 2 
         Pi}, {{$CellContext`TxDropletX$$, 2}, 0, 
         20}, {{$CellContext`TxDropletY$$, 5}, 0, 
         20}, {$CellContext`EmitterNum$$, 1, 6, 1}, {$CellContext`SensorNum$$,
          1, 6, 1}}, "Options" :> {}, "DefaultOptions" :> {}],
     ImageSizeCache->{671., {304., 309.}},
     SingleEvaluation->True],
    Deinitialization:>None,
    DynamicModuleValues:>{},
    SynchronousInitialization->True,
    UnsavedVariables:>{Typeset`initDone$$},
    UntrackedVariables:>{Typeset`size$$}], "Manipulate",
   Deployed->True,
   StripOnInput->False],
  Manipulate`InterpretManipulate[1]]], "Output",
 CellChangeTimes->{
  3.571766003091625*^9, 3.57176611573225*^9, 3.571766194247875*^9, 
   3.57176624348225*^9, {3.571766862997875*^9, 3.57176686991975*^9}, {
   3.57177220541975*^9, 3.571772229029125*^9}, 3.571772346216625*^9, {
   3.5717724193885*^9, 3.57177244291975*^9}, 3.571772759029125*^9, 
   3.571779062497875*^9, 3.57178249598225*^9, 3.5717844219570713`*^9, {
   3.5717859067596397`*^9, 3.571785934781561*^9}, 3.57185133230075*^9, 
   3.571895407847965*^9, 3.5719462428684397`*^9, 3.5719471912721777`*^9, 
   3.5719496626057806`*^9, 3.57195269696865*^9, 3.571960478826285*^9, 
   3.6048750600884056`*^9, 3.6073678363465233`*^9, 3.6073783217635207`*^9}]
}, Open  ]],

Cell["Abandoned Code:", "Section",
 CellChangeTimes->{{3.571854719660125*^9, 3.5718547221445*^9}, {
  3.571895456467877*^9, 3.571895468244811*^9}}]
},
WindowSize->{1056, 861},
WindowMargins->{{Automatic, 94}, {Automatic, 48}},
PrivateNotebookOptions->{"VersionedStylesheet"->{"Default.nb"[8.] -> False}},
FrontEndVersion->"9.0 for Microsoft Windows (64-bit) (January 25, 2013)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[CellGroupData[{
Cell[579, 22, 459, 11, 31, "Input"],
Cell[1041, 35, 376, 5, 31, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[1454, 45, 7029, 172, 392, "Input"],
Cell[8486, 219, 2088, 38, 374, "Output"]
}, Open  ]],
Cell[10589, 260, 1456, 36, 132, "Input"],
Cell[12048, 298, 1155, 29, 72, "Input"],
Cell[CellGroupData[{
Cell[13228, 331, 4539, 107, 372, "Input"],
Cell[17770, 440, 4512, 79, 662, "Output"]
}, Open  ]],
Cell[22297, 522, 147, 2, 86, "Section"]
}
]
*)

(* End of internal cache information *)
