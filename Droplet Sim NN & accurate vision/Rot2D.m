function mat = Rot2D(theta)
%ROT2D Produces a 2D rotation matrix (rotate X-Y plane about Z-axis)
%
% input: theta is in radians

mat = [cos(theta), -sin(theta); sin(theta), cos(theta)];


end

