function [newLambda, delta_theta, delta_rho] = rotate_BM(Lambda)
% this function is part of a triad of functions:
% 1) [newLambda, delta_theta, delta_rho] = rotate_BM(Lambda)
% 2) new_angle = rotate_BM_new_angle(angle, delta_angle)
% 3) angle = rotate_BM_recover_angle(new_angle, delta_angle)
%
% Lambda indicies: (sensor,emitter)

% max operates on the columns of a matrix 
[max_sensor, max_s_ind] = max(max(Lambda'));
[max_emitter, max_e_ind] = max(max(Lambda));

max_s_ind; % row with highest value
max_e_ind; % column with highest value

% note: put the row that you want to have the max directly after the minus
first_row = mod(max_s_ind - 3, 6)+1; % in this case, its 3
newLambda = [Lambda(first_row:6,:); Lambda(1:first_row-1,:)];

% note: put the column that you want to have the max directly after the minus
first_col = mod(max_e_ind - 3, 6)+1; % in this case, its 3
newLambda = [newLambda(:,first_col:6) newLambda(:,1:first_col-1)];

rot_row = mod(1-first_row,6);
rot_col = mod(1-first_col,6);

delta_theta = -(mod(-60*rot_row -2*60, 360) - 60);
delta_rho = -(mod(60*rot_col + 2*60, 360) - 60);


end

