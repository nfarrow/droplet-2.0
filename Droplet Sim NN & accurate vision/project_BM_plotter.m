
RX = [0,0,0];

for sensor = 1:6

    figure;
    title(horzcat('Brightness at sensor ',num2str(sensor)),'FontSize',12);
    xlabel('emitter','FontSize',12);
    xlim([0.5, 6.5]);
    set(gca,'XTick',1:6);
    hold on;

    for R = 5:1:25

        thetad = -90;
        phid = -90;

        TX = [R*cos(thetad*pi/180), R*sin(thetad*pi/180), phid*pi/180];

        Lambda = BrightnessMatrix(RX, TX);
        %X = Pack_BM(Lambda);

        plot(1:6,Lambda(sensor,:),'Color',[(25-R)/25,0,R/25]);
        
    end

    hold off;
    
end