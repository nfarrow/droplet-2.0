function ECMmat = ECM(range, bearing, heading)
%ECM expected contribution matrix given at the specified 
% range, bearing, and heading

tempRX = [0,0,0];
tempTX = [range*cos(bearing), range*sin(bearing), heading];


alpha = AlphaMatrix(tempRX,tempTX);
beta = BetaMatrix(tempRX,tempTX);


ECMmat = SensorModel(alpha).*EmitterModel(beta);

end

