
n = 10000;
RX = zeros(n,3);

R = randi(20,n,1)+5;
% with theta, we show that even when an angle is created in an unexpected
% range the pipeline can still handle it (and recovers the angle into
% the expected range)
thetad = randi(360,n,1)-1+180;
rhod = randi(360,n,1)-1-180;
phid = rhod + thetad;

RSum = sum(R);
thetaSum = sum(thetad);
phiSum = sum(phid);
TX = [R.*cos(thetad*pi/180), R.*sin(thetad*pi/180), phid*pi/180];

LambdaSum = zeros(6,6);
deltatheta = zeros(n,1);
deltarho = zeros(n,1);
for i = 1:n
    Lambda = BrightnessMatrix(RX(i,:), TX(i,:));
    
    [Lambda, deltatheta(i), deltarho(i)] = rotate_BM(Lambda);
    
    LambdaSum = LambdaSum + Lambda;
end

newtheta = rotate_BM_new_angle(thetad, deltatheta);
newrho = rotate_BM_new_angle(rhod, deltarho);
    
recovertheta = rotate_BM_recover_angle(newtheta, deltatheta);
recoverrho = rotate_BM_recover_angle(newrho, deltarho);

figure;
hist(thetad, max(thetad)-min(thetad)+1);
title('theta pre rotate','FontSize',12);

figure;
hist(newtheta, max(newtheta)-min(newtheta)+1);
title('theta post rotate','FontSize',12);

figure;
hist(recovertheta, max(recovertheta)-min(recovertheta)+1);
title('recover theta','FontSize',12);


figure;
hist(rhod, max(rhod)-min(rhod)+1);
title('rho pre rotate','FontSize',12);

figure;
hist(newrho, max(newrho)-min(newrho)+1);
title('rho post rotate','FontSize',12);

figure;
hist(recoverrho, max(recoverrho)-min(recoverrho)+1);
title('recover rho','FontSize',12);


figure;
cmap = jet(6);
title(horzcat('(n = ',num2str(n), ...
    ' means) R = ',num2str(RSum/n), ...
    ', theta = ',num2str(thetaSum/n), ...
    ', phi = ',num2str(phiSum/n)),'FontSize',12);
xlabel('emitter number','FontSize',12);
xlim([0.5, 6.5]);
set(gca,'XTick',1:6);
hold on;

for sensor = 1:6
   
    plot(1:6,LambdaSum(sensor,:),'color',cmap(sensor,:));
    
end

colormap(cmap);
hcb = colorbar;
% next two lines recenter the ticks onto the middle of the colors
set(hcb,'YTick',[1:6]+0.5); 
set(hcb,'YTickLabel',[1:6]);
set(get(hcb,'xlabel'),'String', 'sensor number','FontSize',12);

hold off;
