
RX = [0,0,0];

phid = 45;
R = 15;
thetad = 90;

TX = [R*cos(thetad*pi/180), R*sin(thetad*pi/180), phid*pi/180];
Lambda = BrightnessMatrix(RX, TX);
rotate_BM(Lambda);

cmap = jet(6);

figure;
title(horzcat('R = ',num2str(R),', theta = ',num2str(thetad), ...
    ', phi = ',num2str(phid)),'FontSize',12);
xlabel('emitter number','FontSize',12);
xlim([0.5, 6.5]);
set(gca,'XTick',1:6);

hold on;


for sensor = 1:6
   
    plot(1:6,Lambda(sensor,:),'color',cmap(sensor,:));
    
end

colormap(cmap);
hcb = colorbar;
% next two lines recenter the ticks onto the middle of the colors
set(hcb,'YTick',[1:6]+0.5); 
set(hcb,'YTickLabel',[1:6]);
set(get(hcb,'xlabel'),'String', 'sensor number','FontSize',12);

hold off;
