%vizzion

addpath('C:\Users\Toshiba\Desktop\Droplet Sim')

%% setup the drawing

hf = figure;
reset_fig;
delay_time = 0.05;

%% initialize the robots

num_bots = 4;
num_active_bots = 2;
new_conditions = 1;
%rand_list = randn(num_bots,1);

if(new_conditions)
    rand_list = randn(num_bots,1)+3;
end

% Start the droplets in a square facing random directions
Droplets = zeros(num_bots,3);

%{
Droplets(1,:) = [5, 5, rand_list(1,1)];
Droplets(2,:) = [-5, 5, rand_list(2,1)];
Droplets(3,:) = [-5, -5, rand_list(3,1)];
Droplets(4,:) = [5, -5, rand_list(4,1)];
Droplets(5,:) = [2, 2, rand_list(5,1)];
Droplets(6,:) = [-2, 0, rand_list(6,1)];
%}

Droplets(1,:) = [5, 5, -pi];
Droplets(2,:) = [5, -7, -pi];
Droplets(3,:) = [0, 0, rand_list(3,1)];
Droplets(4,:) = [-5, -5, rand_list(4,1)];




% plans are what the droplets want to do on the next time step
Plans = zeros(num_bots,2); % currently plans are (rotate direction, step forward boolean)



% Subscriber List, a subscriber interacts as the RX in a RX TX RNB interaction
max_subscriptions = 1;
Subscription_List = zeros(num_bots,max_subscriptions);
Subscription_List(1,:) = 2;
Subscription_List(2,:) = 3;
Subscription_List(3,:) = 4;
Subscription_List(4,:) = 1;
%Subscription_List(5,:) = 1;
%Subscription_List(6,:) = 6;

%{
num_walls = 4;
wall_list = zeros(num_walls,4); % square walls for now
wall_list(1,:) = [10, 10, 10, -10]; % its endpoints are x,y=10,10 and x,y=10,-10
wall_list(2,:) = [-10, 10, 10, 10];
wall_list(3,:) = [-10, -10, -10, 10];
wall_list(4,:) = [10, -10, -10, -10];
%}

num_walls = 5;
wall_list = zeros(num_walls,4); % square walls for now
% walls have to be defined CCW for collisions to work properly (for now)
wall_list(1,:) = [10, 10, 10, -10]; % its endpoints are x,y=10,10 and x,y=10,-10
wall_list(2,:) = [-10, 10, 10, 10];
wall_list(3,:) = [-10, -6, -10, 10];
wall_list(4,:) = [10, -10, -6, -10];
wall_list(5,:) = [-6, -10, -10, -6];

prev_step_Droplets = zeros(size(Droplets));

obstruction_mat = zeros(num_bots);

%% draw the initial configuration

for d = 1:num_bots
    draw_droplet(Droplets(d,:));
end
 
%% run the simulation

drive_speed = 1; % this is arbitrary for now, assume 1 is full speed

num_timesteps = 1000;

for t = 1:num_timesteps

    %pause(0.01); 

    if(mod(t,2) == 1)
        % update the postitions
        %for d = 1:num_bots
        if(Droplets == prev_step_Droplets)
            break
        end
        
        prev_step_Droplets = Droplets;
        
        for d = 1:num_active_bots
            Droplets(d,:) = bot_drive(Droplets(d,:), 2, drive_speed, Plans(d,:));
            Droplets = check_wall_collision(Droplets, wall_list);
            Droplets = check_droplet_collision(Droplets, prev_step_Droplets);

        end
        
        pause(delay_time);
        % draw the new state
        reset_fig;

        draw_walls(wall_list);
        
        for d = 1:num_bots
            draw_droplet(Droplets(d,:));
        end

    else
        % robots interact
        for d = 1:num_active_bots % iterate through the droplets
            % fd is 'following droplet', the robot that is (being followed)
            % being subscribed to, by d
            fd = Subscription_List(d,1); % bot d is 'following' bot fd
            %obstruction = check_LOS_collision(Droplets, [Droplets(d,1), Droplets(d,2), Droplets(fd,1), Droplets(fd,2)]);
            obstruction_mat = check_LOS_collision_all(Droplets);

            [Droplets(d,:) Subscription_List(d,:) Plans(d,:)] = bot_follow(Droplets(d,:), Droplets(fd,:), Subscription_List(d,:), Subscription_List(fd,:));
            
            draw_interaction(Droplets, d, fd, obstruction_mat);
            %draw_interaction(Droplets(d,:), Droplets(fd,:), Subscription_List(d,:), Subscription_List(fd,:), obstruction_mat);
        end
        
    end
    
    
end