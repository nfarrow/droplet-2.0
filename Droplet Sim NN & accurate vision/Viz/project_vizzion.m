%vizzion

addpath('C:\Users\Toshiba\Desktop\Droplet Sim')

%% setup the drawing

hf = figure;

reset_fig;
%% initialize the robots

num_bots = 4;

D = zeros(num_bots,3);
D(1,:) = [5, 5, randn(1)];
D(2,:) = [-5, 5, randn(1)];
D(3,:) = [-5, -5, randn(1)];
D(4,:) = [5, -5, randn(1)];

% subscriber value
S = zeros(num_bots,1);
S(1,:) = 2;
S(2,:) = 3;
S(3,:) = 4;
S(4,:) = 1;

%% draw the initial configuration

for d = 1:num_bots
    draw_droplet(D(d,:));
end
 
%% run the simulation

num_timesteps = 3000;

for t = 1:num_timesteps

    pause(0.01); 

    if(mod(t,2) == 1)
        % update the postitions
        for d = 1:4
            D(d,:) = drive(D(d,:), 1, 1);
        end
    
        % draw the new state
        reset_fig;

        for d = 1:num_bots
            draw_droplet(D(d,:));
        end

    else
        % robots interact
        for d = 1:4
            fd = S(d,1); % bot d is 'following' bot fd
            [D(d,:) S(d,:)] = bot_follow(D(d,:), D(fd,:), S(d,:), S(fd,:));
            
        end
    end
    
    
    
end