function [obstruction_mat] = check_LOS_collision_all(Droplets)

r = 2; % this is the droplet radius (TODO move all defs of this to one place)

[n ~] = size(Droplets);

obstruction_mat = zeros(n);

%ti = 1; % transmission index (only 1 for now)

for d1 = 1:n
    for d2 = (d1+1):n
        obst_list = 1:n;
        obst_list = obst_list(obst_list ~= d1);
        obst_list = obst_list(obst_list ~= d2);
        
        tx_line = [Droplets(d1,1), Droplets(d1,2), Droplets(d2,1), Droplets(d2,2)];
        %tx_norm_angle = atan2(tx_line(3)-tx_line(1),tx_line(4)-tx_line(2)) + pi/2; % atan2(y,x)
        tx_norm_angle = atan2(tx_line(3)-tx_line(1),tx_line(4)-tx_line(2)); % atan2(y,x)
        
        
        for do = obst_list
            
            droplet_side_points = [Droplets(do,1), Droplets(do,2), Droplets(do,1), Droplets(do,2)] ...
                + [-r*cos(tx_norm_angle), r*sin(tx_norm_angle), r*cos(tx_norm_angle), -r*sin(tx_norm_angle)];
                % NOTE!: this is different than the wall collision checking!(TODO, why?)
            
            [intersect, i_x, i_y, t] = check_intersect(droplet_side_points, tx_line);

           
            
            if(intersect)
                %fprintf('LOS: %i to %i with %i blocking\n',d1, d2, do);
                obstruction_mat(d1,d2) = 1;
                obstruction_mat(d2,d1) = 1;
                draw_arb_line(droplet_side_points, 'r');
            else
                draw_arb_line(droplet_side_points, 'k');
            end
            
        end
    end
end

end