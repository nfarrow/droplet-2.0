function [Droplets] = check_wall_collision(Droplets, walls)

r = 2; % this is the droplet radius (TODO move all defs of this to one place)

[num_walls ~] = size(walls);

[n ~] = size(Droplets);

for wi = 1:num_walls
    wall_dx = walls(wi,1) - walls(wi,3);
    wall_dy = walls(wi,2) - walls(wi,4);
    
    wall_norm_angle = atan2(wall_dy,wall_dx) + pi/2;
    
    
%{    
    wall_normal_x = -(walls(wi,2)-walls(wi,4));
    wall_normal_y = -(walls(wi,1)-walls(wi,3));
    %fprintf('wall normal: %f,%f\n', wall_normal_x, wall_normal_y);
%}
    
    for di = 1:n   % di = 'droplet index'
        droplet_side_points = [Droplets(di,1), Droplets(di,2), Droplets(di,1), Droplets(di,2)] ...
            + [r*cos(wall_norm_angle), r*sin(wall_norm_angle), -r*cos(wall_norm_angle), -r*sin(wall_norm_angle)];
            % NOTE!: this is different than the LOS collision checking!(TODO, why?)
            
        [intersect, i_x, i_y, t] = check_intersect(droplet_side_points, walls(wi,:));
        
        if(intersect)
            if(t < 0.5)
                Droplets(di,:) = Droplets(di,:) + [2*r*t*cos(wall_norm_angle), 2*r*t*sin(wall_norm_angle), 0];
            else
                Droplets(di,:) = Droplets(di,:) + [2*r*(1-t)*cos(wall_norm_angle), 2*r*(1-t)*sin(wall_norm_angle), 0];
            end
                
        end
        
    end    
end

end

