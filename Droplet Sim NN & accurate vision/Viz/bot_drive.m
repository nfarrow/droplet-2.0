function [Dnew plannew] = bot_drive(Droplet, dir, speed, plan)

x = Droplet(1);
y = Droplet(2);
theta = Droplet(3);

step_sz = 0.2*speed;


%dir %monitor

rotate_idea = plan(1);
%rotate_idea = 0;

walk_idea = plan(2);

%rotate_idea %monitor

% NOTE the ratio of drive speed and maximum rotation speed is important
% for proper dynamics of motion 

%% rotate if you are going to rotate
% here, 0.1 is the maximum rotation speed
theta = theta + 0.1*rotate_idea;

%% walk if you are going to walk
if(walk_idea == 0)
    Dnew = [x, y, theta];
elseif(walk_idea == -1)
    Dnew = [x - step_sz*cos(theta), y - step_sz*sin(theta), theta];
elseif(walk_idea == 1)
    Dnew = [x + step_sz*cos(theta), y + step_sz*sin(theta), theta];
else
    error('INVALID walk_idea: %i', walk_idea);
end
    
plannew = [0, 0];

end

