function [Droplets] = check_droplet_collision(Droplets, prev_Droplets)

r = 2; % this is the droplet radius (TODO move all defs of this to one place)
push_rate = 0.05; % number between 1 and 0,
    % 1 means driving droplet is 100% effectient pusher
    % 0 means objects cannot be pushed by the droplet

[n m] = size(Droplets);



for di1 = 1:n   % di = 'droplet index'
    for di2 = di1+1:n 
        D1 = Droplets(di1,:);
        D2 = Droplets(di2,:);
        current_dist = sqrt((D1(1)-D2(1))^2 + (D1(2)-D2(2))^2);
        if(current_dist  < 2*r)
            overlap = 2*r - current_dist;
            degree_of_overlap = overlap/(2*r);
            dx = D1(1)-D2(1);
            dy = D1(2)-D2(2);
            %fprintf('collision %i %i, %f\n', di1, di2, degree_of_overlap);

            % need to figure out who the driver is, we want it so that the
            % droplet or object being pushed resists the push
            d1_diff = sqrt((prev_Droplets(di1,1)-Droplets(di1,1))^2 + (prev_Droplets(di1,2)-Droplets(di1,2))^2);
            d2_diff = sqrt((prev_Droplets(di2,1)-Droplets(di2,1))^2 + (prev_Droplets(di2,2)-Droplets(di2,2))^2);
                
            %fprintf('collision %i %i, %f, %f, %f\n', di1, di2, degree_of_overlap, d1_diff, d2_diff);
            if(d1_diff + d2_diff ~= 0)

                d1_push_rate = 2*(((1-push_rate)*d1_diff+push_rate*d2_diff)/(d1_diff+d2_diff));
                d2_push_rate = 2*((push_rate*d1_diff+(1-push_rate)*d2_diff)/(d1_diff+d2_diff));

                d1_pos_change = d1_push_rate*degree_of_overlap*[dx/2, dy/2, 0];
                d2_pos_change = -d2_push_rate*degree_of_overlap*[dx/2, dy/2, 0];

                Droplets(di1,:) = Droplets(di1,:) + d1_pos_change;
                Droplets(di2,:) = Droplets(di2,:) + d2_pos_change;
            end
        end
    end

end

