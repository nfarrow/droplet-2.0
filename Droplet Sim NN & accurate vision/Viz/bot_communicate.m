function [D1plan] = bot_follow(D1, D2, D1plan)
% here D1 is interacting with D2, only D1 updates its behavior (if any)
% from this interaction, this is returned as D1new & S1new.

plan_rotate_allowed = 1;

[r, theta] = bot_RNB_exact(D1, D2);




D1new = D1;


if(plan_rotate_allowed)
    % this here is planning for rotating the robot
    %if(theta > 0)
    if(theta > 0.1)
        D1plan(1) = 1; % turn CCW
    elseif(theta < -0.1)
        D1plan(1) = -1; % turn CW
    end
end

if(r > 10)
    D1plan(2) = 1;  % walk forward
elseif(r < 9)
    D1plan(2) = -1; % walk backward
end
    
    
end

