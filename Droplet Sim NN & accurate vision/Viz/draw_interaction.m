%function draw_interaction(D1, D2, S1, S2, obstruction)
function draw_interaction(Droplets, D1ind, D2ind, obstruction_mat)

% Also, a green line is drawn in the figure to indicate an interaction.

D1x = Droplets(D1ind,1);
D2x = Droplets(D2ind,1);
D1y = Droplets(D1ind,2);
D2y = Droplets(D2ind,2);

if(obstruction_mat(D1ind, D2ind) == 0)
    plot([D1x D2x],[D1y D2y], 'g');
else
    plot([D1x D2x],[D1y D2y], 'r');
end

end

