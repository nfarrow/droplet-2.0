function [intersect, intersect_x, intersect_y, t] = check_intersect(line1, line2)

dx1 = line1(3) - line1(1);
dy1 = line1(4) - line1(2);
dx2 = line2(3) - line2(1);
dy2 = line2(4) - line2(2);

%{
fprintf('dx1: %f, dy1: %f\n', dx1, dy1);
fprintf('dx2: %f, dy2: %f\n', dx2, dy2);
plot([line1(1) line1(3)],[line1(2) line1(4)], 'r');
plot([line2(1) line2(3)],[line2(2) line2(4)], 'r');
%}

s = (-dy1*(line1(1)-line2(1)) + dx1*(line1(2)-line2(2)))/(-dx2*dy1 + dx1*dy2);
t = ( dx2*(line1(2)-line2(2)) - dy2*(line1(1)-line2(1)))/(-dx2*dy1 + dx1*dy2);
% s is a 'percentage' along the length of line 1 where the intersection
% occurs, beginning with the s=0 being the first point in the pair for line1
% t is the cooresponding variable for line 2

%fprintf('s: %f, t: %f\n', s, t);

if(s>=0 && s<=1 && t>=0 && t<=1)
    intersect = 1;
    intersect_x = line1(1) + t*dx1;
    intersect_y = line1(2) + t*dy1;
else
    intersect = 0;
    intersect_x = 0;
    intersect_y = 0;
end


% ref: http://stackoverflow.com/questions/563198/how-do-you-detect-where-two-line-segments-intersect
%{
char get_line_intersection(float p0_x, float p0_y, float p1_x, float p1_y, 
    float p2_x, float p2_y, float p3_x, float p3_y, float *i_x, float *i_y)
{
    float s1_x, s1_y, s2_x, s2_y;
    s1_x = p1_x - p0_x;     s1_y = p1_y - p0_y;
    s2_x = p3_x - p2_x;     s2_y = p3_y - p2_y;

    float s, t;
    s = (-s1_y * (p0_x - p2_x) + s1_x * (p0_y - p2_y)) / (-s2_x * s1_y + s1_x * s2_y);
    t = ( s2_x * (p0_y - p2_y) - s2_y * (p0_x - p2_x)) / (-s2_x * s1_y + s1_x * s2_y);

    if (s >= 0 && s <= 1 && t >= 0 && t <= 1)
    {
        // Collision detected
        if (i_x != NULL)
            *i_x = p0_x + (t * s1_x);
        if (i_y != NULL)
            *i_y = p0_y + (t * s1_y);
        return 1;
    }

    return 0; // No collision
}
%}

end

