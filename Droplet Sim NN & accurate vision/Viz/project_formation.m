%vizzion

addpath('C:\Users\Toshiba\Desktop\Droplet Sim')

%% setup the drawing

hf = figure;
reset_fig;
delay_time = 0.05; % THIS IS STANDARD
%delay_time = 0.01; % fast

%% initialize the robots

num_bots = 4;
num_active_bots = 4;
new_conditions = 0;

if(new_conditions)
    rand_list = randn(num_bots,1)+3;
end

Droplets = zeros(num_bots,3);
Droplets(1,:) = [5, 5, -pi];
Droplets(2,:) = [5, -7, -pi];
Droplets(3,:) = [0, 0, rand_list(3,1)];
Droplets(4,:) = [-5, -5, rand_list(4,1)];

% plans are what the droplets want to do on the next time step
Plans = zeros(num_bots,2); % currently plans are (rotate direction, step forward boolean)

% Subscriber List, a subscriber interacts as the RX in a RX TX RNB interaction
max_subscriptions = 2;
Subscription_List = zeros(num_bots,max_subscriptions);
Subscription_List(1,:) = [2, 3];
Subscription_List(2,:) = [3, 2];
Subscription_List(3,:) = [4, 3];
Subscription_List(4,:) = [1, 2];
Subscription_List = randi(4,num_bots,2);
%comm_schedule = [0,0,1,0,0,2,0,0,3,0,0,4];
comm_schedule = [1,3,2,4];
comm_period = length(comm_schedule);

num_walls = 5;
wall_list = zeros(num_walls,4); % square walls for now
% walls have to be defined CCW for collisions to work properly (for now)
wall_list(1,:) = [10, 10, 10, -10]; % its endpoints are x,y=10,10 and x,y=10,-10
wall_list(2,:) = [-10, 10, 10, 10];
wall_list(3,:) = [-10, -6, -10, 10];
wall_list(4,:) = [10, -10, -6, -10];
wall_list(5,:) = [-6, -10, -10, -6];






prev_step_Droplets = zeros(size(Droplets));

% obstruction matrix: 
% entry (i,j) == 1 if i & j cannot see each other 
% entry (i,j) == 0 if i & j can see each other
obstruction_mat = zeros(num_bots);

%% draw the initial configuration

for d = 1:num_bots
    draw_droplet(Droplets(d,:));
end
 
%% run the simulation

drive_speed = 1; % this is arbitrary for now, assume 1 is full speed

num_timesteps = 1000;

for t = 1:num_timesteps

    
    % check who gets control of the channel
    schedule_index = mod(t,comm_period) + 1;
    channel_allocation = comm_schedule(schedule_index);
    if(channel_allocation > 0)
        fprintf('timestep: %i (%i has the con)\n', t, channel_allocation);
    else
        fprintf('timestep: %i\n', t);
    end
    
    % update the postitions
    %{
    if(Droplets == prev_step_Droplets)
        error('NOBODY moved'); % not really an error
        break;
    end
    %}
    prev_step_Droplets = Droplets;

    for d = 1:num_active_bots
        [Droplets(d,:), Plans(d,:)] = bot_drive(Droplets(d,:), 2, drive_speed, Plans(d,:));
        Droplets = check_wall_collision(Droplets, wall_list);
        Droplets = check_droplet_collision(Droplets, prev_step_Droplets);

    end

    % draw the new state
    reset_fig;

    draw_walls(wall_list);

    for d = 1:num_bots
        draw_droplet(Droplets(d,:));
    end

    % robots interact
    %for d = 1:num_active_bots % iterate through the droplets
    if(channel_allocation > 0)
        d = channel_allocation;
        
        d1 = Subscription_List(d,1);
        d2 = Subscription_List(d,2);
        
        obstruction_mat = check_LOS_collision_all(Droplets);

        if(obstruction_mat(d,d1) == 0)
            Plans(d,:) = bot_communicate(Droplets(d,:), Droplets(d1,:), Plans(d,:));
        else
            % make something up to find your partner
            Plans(d,1) = randi(3,1)-2;  % rotate ? -1,0,1
            Plans(d,2) = randi(3,1)-2;  % walk ? -1,0,1
        end
        
        if(obstruction_mat(d,d2) == 0)
            Plans(d,:) = bot_communicate(Droplets(d,:), Droplets(d2,:), Plans(d,:));
        end
        
        draw_interaction(Droplets, d, d1, obstruction_mat);
        draw_interaction(Droplets, d, d2, obstruction_mat);
    end

    
    
    pause(delay_time);
    
end