function [obstruction] = check_LOS_collision(Droplets, transmisttion)

r = 2; % this is the droplet radius (TODO move all defs of this to one place)

[n ~] = size(Droplets);

obstruction = 0;

ti = 1; % transmission index (only 1 for now)

transmission_dx = transmisttion(ti,1) - transmisttion(ti,3);
transmission_dy = transmisttion(ti,2) - transmisttion(ti,4);

tx_norm_angle = atan2(transmission_dy,transmission_dx) + pi/2;


for di = 1:n   % di = 'droplet index'
    droplet_side_points = [Droplets(di,1), Droplets(di,2), Droplets(di,1), Droplets(di,2)] ...
        + [r*cos(tx_norm_angle), r*sin(tx_norm_angle), -r*cos(tx_norm_angle), -r*sin(tx_norm_angle)];

    [intersect, i_x, i_y, t] = check_intersect(droplet_side_points, transmisttion(ti,:));

    if(intersect)
        obstruction = 1;
        %fprintf('collision: d%i\n',di);
    else
        obstruction = 0;
    end

end    

end

