function [R, theta] = bot_RNB_exact(RX, TX)

theta = atan2(TX(2)-RX(2), TX(1)-RX(1)); % format is (delta-y, delta-x)
theta = theta - RX(3); % subtract off the receivers angle
theta = PItoPIangle(theta);

R = sqrt((TX(1)-RX(1))^2 + (TX(2)-RX(2))^2);

end

