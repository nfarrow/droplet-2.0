
RX = [0,0,0];

cmap = cool(21);

for sensor = 1:6

    figure;
    title(horzcat('effect of R (sensor ',num2str(sensor),')'),'FontSize',12);
    xlabel('emitter number','FontSize',12);
    xlim([0.5, 6.5]);
    set(gca,'XTick',1:6);
    
    hold on;

    for R = 5:1:25

        thetad = -90;
        phid = -90;

        TX = [R*cos(thetad*pi/180), R*sin(thetad*pi/180), phid*pi/180];

        Lambda = BrightnessMatrix(RX, TX);

        plot(1:6,Lambda(sensor,:),'Color',cmap(R-4,:));
        
    end

    colormap(cmap);
    handle_cb = colorbar;
    set(handle_cb,'YTick',1:21,'YTickLabel',5:25)

    
    hold off;
    
end