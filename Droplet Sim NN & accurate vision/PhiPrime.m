function phi_guess = PhiPrime(Lambda)
%PHIPRIME Our best guess of phi, given the brightness matrix, Lambda.
%
%   taking the sum over the bj vectors weighted by the Tj
%   sensor readings, we obtain a vector Phi, pointing in the
%   approximate direction of RX from the coordinate frame
%   of TX. By using a coordinate transform back to the
%   coordinate frame of RX, we obtain . Using  as an
%   estimate of  yields an estimate  of the relative angle
%   of RX and TX, using only information locally available to RX
%   (through its sensors). Mathematically, see algorithm in code.

% the correct answer from the inputs [0,0,0] [2,5,3] is 2.98264

basisCX = UnitNormalsCX(0);

S = sum(Lambda');
T = sum(Lambda);
%sum(S.*basis)

theta_guess = angle(sum(S.*basisCX));
phi_guess = PItoPIangle(theta_guess + pi + angle(sum(T.*fliplr(basisCX))));





end

