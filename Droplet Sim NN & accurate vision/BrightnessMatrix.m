function BM = BrightnessMatrix(RX, TX)
% RX/TX each a 3 feature point [x y theta]
% this solves the general case of arbitrary positions, and also the special
% case of (RXx, RXy, RXtheta) = (0,0,0), for which we find 
% the R&B solution: (R, theta, phi) = (Range, Bearing, and Heading)

% OUTPUT:
% sensors are enumerated by the rows
% emitters are enumerated by the columns
% (sensors, emitters)

% conventions used throughout (harware):
% when droplets were designed, the convention for the sensor numberings 
% was "like a clock face", sensors are numbered 1 - 6
% "Straight ahaead" is 0-degrees, aligned with x-axis
% sensor & emiter 1 exist at -30 degrees, every sensor thereafter is -60
% degrees beyond that (sensor 2 is at -90 degrees)

r = rMatrix(RX,TX);
alpha = AlphaMatrix(RX,TX);
beta = BetaMatrix(RX,TX);

%SM = SensorModel(alpha)
%EM = EmitterModel(beta)
%AM = AmplitudeModel(r)

BM = AmplitudeModel(r).*SensorModel(alpha).*EmitterModel(beta);



end

