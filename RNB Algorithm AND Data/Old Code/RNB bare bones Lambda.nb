(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 8.0' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       157,          7]
NotebookDataLength[     30699,        826]
NotebookOptionsPosition[     29341,        776]
NotebookOutlinePosition[     29685,        791]
CellTagsIndexPosition[     29642,        788]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[BoxData[{
 RowBox[{
  RowBox[{"StandardX", "=", "14"}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"StandardY", "=", "14"}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"Standard\[Theta]", "=", 
   FractionBox[
    RowBox[{"5", " ", "\[Pi]"}], "4"]}], ";"}]}], "Input",
 CellChangeTimes->{{3.5717723810135*^9, 3.571772441154125*^9}, {
   3.571779050029125*^9, 3.571779050185375*^9}, 3.571782487060375*^9, {
   3.571782608810375*^9, 3.571782624701*^9}, {3.5717852922142286`*^9, 
   3.5717852929171195`*^9}, {3.5718436723091707`*^9, 
   3.5718436738413734`*^9}, {3.571869901910125*^9, 3.571869902003875*^9}, 
   3.571870069457*^9}],

Cell[CellGroupData[{

Cell["Geometry Functions", "Section",
 CellChangeTimes->{{3.571586183372875*^9, 3.571586201310375*^9}}],

Cell[BoxData[
 RowBox[{
  RowBox[{"sensorR", "=", " ", "1.98"}], ";"}]], "Input",
 CellChangeTimes->{3.571772396216625*^9}],

Cell[BoxData[{
 RowBox[{
  RowBox[{"RxNormalUnit", "[", 
   RowBox[{"\[Theta]_", ",", "SensorNum_"}], "]"}], ":=", 
  RowBox[{
   RowBox[{"RotationMatrix", "[", 
    RowBox[{
     RowBox[{"-", 
      FractionBox["\[Pi]", "3"]}], "SensorNum"}], "]"}], ".", 
   RowBox[{"RotationMatrix", "[", 
    RowBox[{"-", 
     FractionBox["\[Pi]", "6"]}], "]"}], ".", 
   RowBox[{"RotationMatrix", "[", "\[Theta]", "]"}], ".", 
   RowBox[{"{", 
    RowBox[{"1", ",", "0"}], "}"}]}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"TxNormalUnit", "[", 
   RowBox[{"\[Theta]_", ",", "EmitterNum_"}], "]"}], ":=", 
  RowBox[{
   RowBox[{"RotationMatrix", "[", 
    RowBox[{
     RowBox[{"-", 
      FractionBox["\[Pi]", "3"]}], "EmitterNum"}], "]"}], ".", 
   RowBox[{"RotationMatrix", "[", 
    RowBox[{"-", 
     FractionBox["\[Pi]", "6"]}], "]"}], ".", 
   RowBox[{"RotationMatrix", "[", "\[Theta]", "]"}], ".", 
   RowBox[{"{", 
    RowBox[{"1", ",", "0"}], "}"}]}]}]}], "Input",
 CellChangeTimes->{{3.57158589016975*^9, 3.571585908060375*^9}}],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"BasisMatrix", "=", 
  RowBox[{"Table", "[", 
   RowBox[{
    RowBox[{"RxNormalUnit", "[", 
     RowBox[{"0", ",", "SensorNum"}], "]"}], ",", 
    RowBox[{"{", 
     RowBox[{"SensorNum", ",", "0", ",", "5"}], "}"}]}], "]"}]}]], "Input",
 CellChangeTimes->{3.571603060951*^9}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{
     FractionBox[
      SqrtBox["3"], "2"], ",", 
     RowBox[{"-", 
      FractionBox["1", "2"]}]}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"0", ",", 
     RowBox[{"-", "1"}]}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{
     RowBox[{"-", 
      FractionBox[
       SqrtBox["3"], "2"]}], ",", 
     RowBox[{"-", 
      FractionBox["1", "2"]}]}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{
     RowBox[{"-", 
      FractionBox[
       SqrtBox["3"], "2"]}], ",", 
     FractionBox["1", "2"]}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"0", ",", "1"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{
     FractionBox[
      SqrtBox["3"], "2"], ",", 
     FractionBox["1", "2"]}], "}"}]}], "}"}]], "Output",
 CellChangeTimes->{{3.571603061466625*^9, 3.57160308004475*^9}, 
   3.571626687620832*^9, 3.5716874269606*^9, 3.571765923810375*^9, 
   3.57177233729475*^9, 3.571779051966625*^9, 3.571783516836521*^9, 
   3.571785294853975*^9, 3.57178804197068*^9, 3.5718435171160135`*^9, 
   3.5718436765652905`*^9, 3.571851320253875*^9, 3.571868256457*^9, 
   3.571870072160125*^9}]
}, Open  ]],

Cell[BoxData[
 RowBox[{
  RowBox[{"RxTxPairVector", "[", 
   RowBox[{
    RowBox[{"{", 
     RowBox[{"RxDropletX_", ",", "RxDropletY_"}], "}"}], ",", 
    "RxDroplet\[Theta]_", ",", 
    RowBox[{"{", 
     RowBox[{"TxDropletX_", ",", "TxDropletY_"}], "}"}], ",", 
    "TxDroplet\[Theta]_", ",", "EmitterNum_", ",", "SensorNum_"}], "]"}], ":=", 
  RowBox[{"(*", " ", 
   RowBox[{
   "center", " ", "to", " ", "center", " ", "distance", " ", "minus", " ", 
    "the", " ", "receive", " ", "unit", " ", "vector", " ", "plus", " ", 
    "the", " ", "transmit", " ", "unit", " ", "vector"}], " ", "*)"}], 
  "\[IndentingNewLine]", 
  RowBox[{"(*", " ", 
   RowBox[{
    RowBox[{"this", " ", "function", " ", "is", " ", "correct"}], ",", " ", 
    RowBox[{"dont", " ", "change"}]}], " ", "*)"}], "\[IndentingNewLine]", 
  RowBox[{"(*", " ", 
   RowBox[{
    RowBox[{
    "vector", " ", "points", " ", "FROM", " ", "the", " ", "sensor", " ", 
     "TO", " ", "the", " ", "emitter"}], ",", " ", 
    RowBox[{"opposite", " ", "the", " ", "path", " ", "of", " ", "light"}]}], 
   " ", "*)"}], "\[IndentingNewLine]", 
  RowBox[{
   RowBox[{"-", 
    RowBox[{"{", 
     RowBox[{"RxDropletX", ",", "RxDropletY"}], "}"}]}], "+", 
   RowBox[{"{", 
    RowBox[{"TxDropletX", ",", "TxDropletY"}], "}"}], "-", 
   RowBox[{"sensorR", " ", 
    RowBox[{"RxNormalUnit", "[", 
     RowBox[{"RxDroplet\[Theta]", ",", "SensorNum"}], "]"}]}], "+", 
   RowBox[{"sensorR", " ", 
    RowBox[{"TxNormalUnit", "[", 
     RowBox[{"TxDroplet\[Theta]", ",", "EmitterNum"}], "]"}]}]}]}]], "Input",
 CellChangeTimes->{{3.571585944560375*^9, 3.571585964576*^9}}],

Cell[BoxData[
 RowBox[{
  RowBox[{"FullAngleBetween", "[", 
   RowBox[{"V1_", ",", "V2_"}], "]"}], ":=", 
  RowBox[{"(", 
   RowBox[{
    RowBox[{"V1norm", "=", 
     RowBox[{"Normalize", "[", "V1", "]"}]}], ";", 
    RowBox[{"V2norm", "=", 
     RowBox[{"Normalize", "[", "V2", "]"}]}], ";", 
    RowBox[{"If", "[", 
     RowBox[{
      RowBox[{
       RowBox[{"Det", "[", 
        RowBox[{"{", 
         RowBox[{"V1norm", ",", "V2norm"}], "}"}], "]"}], "<", "0"}], ",", 
      RowBox[{"-", 
       RowBox[{"ArcCos", "[", 
        RowBox[{"V1norm", ".", "V2norm"}], "]"}]}], ",", 
      RowBox[{"ArcCos", "[", 
       RowBox[{"V1norm", ".", "V2norm"}], "]"}]}], "]"}]}], ")"}]}]], "Input"],

Cell[BoxData[{
 RowBox[{
  RowBox[{"Rx\[Alpha]", "[", 
   RowBox[{
    RowBox[{"{", 
     RowBox[{"RxDropletX_", ",", "RxDropletY_"}], "}"}], ",", 
    "RxDroplet\[Theta]_", ",", 
    RowBox[{"{", 
     RowBox[{"TxDropletX_", ",", "TxDropletY_"}], "}"}], ",", 
    "TxDroplet\[Theta]_", ",", "EmitterNum_", ",", "SensorNum_"}], "]"}], ":=",
   " ", 
  RowBox[{"(*", " ", 
   RowBox[{"full", " ", "angle"}], " ", "*)"}], "\[IndentingNewLine]", 
  RowBox[{"FullAngleBetween", "[", 
   RowBox[{"(*", " ", 
    RowBox[{
     RowBox[{"this", " ", "function", " ", "is", " ", "correct"}], ",", " ", 
     RowBox[{"dont", " ", "change"}]}], " ", "*)"}], "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"RxNormalUnit", "[", 
     RowBox[{"RxDroplet\[Theta]", ",", "SensorNum"}], "]"}], ",", 
    "\[IndentingNewLine]", 
    RowBox[{"RxTxPairVector", "[", 
     RowBox[{
      RowBox[{"{", 
       RowBox[{"RxDropletX", ",", "RxDropletY"}], "}"}], ",", 
      "RxDroplet\[Theta]", ",", 
      RowBox[{"{", 
       RowBox[{"TxDropletX", ",", "TxDropletY"}], "}"}], ",", 
      "TxDroplet\[Theta]", ",", "EmitterNum", ",", "SensorNum"}], "]"}]}], 
   "\[IndentingNewLine]", 
   RowBox[{"(*", " ", 
    RowBox[{
     RowBox[{
     "last", " ", "line", " ", "needs", " ", "to", " ", "be", " ", 
      "positive"}], ",", " ", 
     RowBox[{
     "represents", " ", "the", " ", "direction", " ", "of", " ", "light"}]}], 
    " ", "*)"}], "\[IndentingNewLine]", "]"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"Tx\[Beta]", "[", 
   RowBox[{
    RowBox[{"{", 
     RowBox[{"RxDropletX_", ",", "RxDropletY_"}], "}"}], ",", 
    "RxDroplet\[Theta]_", ",", 
    RowBox[{"{", 
     RowBox[{"TxDropletX_", ",", "TxDropletY_"}], "}"}], ",", 
    "TxDroplet\[Theta]_", ",", "EmitterNum_", ",", "SensorNum_"}], "]"}], ":=",
   " ", 
  RowBox[{"(*", " ", 
   RowBox[{"full", " ", "angle"}], " ", "*)"}], "\[IndentingNewLine]", 
  RowBox[{"FullAngleBetween", "[", " ", 
   RowBox[{"(*", " ", 
    RowBox[{
     RowBox[{"this", " ", "function", " ", "is", " ", "correct"}], ",", " ", 
     RowBox[{"dont", " ", "change"}]}], " ", "*)"}], "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"TxNormalUnit", "[", 
     RowBox[{"TxDroplet\[Theta]", ",", "EmitterNum"}], "]"}], ",", 
    "\[IndentingNewLine]", 
    RowBox[{"-", 
     RowBox[{"RxTxPairVector", "[", 
      RowBox[{
       RowBox[{"{", 
        RowBox[{"RxDropletX", ",", "RxDropletY"}], "}"}], ",", 
       "RxDroplet\[Theta]", ",", 
       RowBox[{"{", 
        RowBox[{"TxDropletX", ",", "TxDropletY"}], "}"}], ",", 
       "TxDroplet\[Theta]", ",", "EmitterNum", ",", "SensorNum"}], "]"}]}]}], 
   "\[IndentingNewLine]", 
   RowBox[{"(*", " ", 
    RowBox[{
     RowBox[{
     "last", " ", "line", " ", "needs", " ", "to", " ", "be", " ", 
      "negative"}], ",", " ", 
     RowBox[{
     "represents", " ", "the", " ", "direction", " ", "of", " ", "light"}]}], 
    " ", "*)"}], "\[IndentingNewLine]", "]"}]}]}], "Input",
 CellChangeTimes->{{3.571585702935375*^9, 3.57158570323225*^9}}]
}, Open  ]],

Cell[CellGroupData[{

Cell["Hardware Models", "Section",
 CellChangeTimes->{{3.57158575373225*^9, 3.5715857566385*^9}}],

Cell[BoxData[
 RowBox[{
  RowBox[{"SensorFromData", "[", "x_", "]"}], ":=", 
  RowBox[{"98.05626241039101`", "\[VeryThinSpace]", "+", 
   RowBox[{"52.02649146176708`", " ", 
    SuperscriptBox["x", "2"]}], "-", 
   RowBox[{"54.29602580788779`", " ", 
    SuperscriptBox["x", "4"]}], "-", 
   RowBox[{"273.5874435072842`", " ", 
    SuperscriptBox["x", "6"]}], "+", 
   RowBox[{"392.90073198475466`", " ", 
    SuperscriptBox["x", "8"]}], "-", 
   RowBox[{"222.72807880995836`", " ", 
    SuperscriptBox["x", "10"]}], "+", 
   RowBox[{"64.09684729140568`", " ", 
    SuperscriptBox["x", "12"]}], "-", 
   RowBox[{"9.313599390451419`", " ", 
    SuperscriptBox["x", "14"]}], "+", 
   RowBox[{"0.5434316849030875`", " ", 
    SuperscriptBox["x", "16"]}]}]}]], "Input",
 CellChangeTimes->{{3.571255355185375*^9, 3.571255356076*^9}, {
  3.571255478529125*^9, 3.571255484701*^9}, {3.571424661622875*^9, 
  3.571424753654125*^9}, {3.571589934326*^9, 3.571589961747875*^9}, {
  3.571589999341625*^9, 3.5715900317635*^9}, {3.571590336966625*^9, 
  3.57159034035725*^9}}],

Cell[CellGroupData[{

Cell[BoxData[{
 RowBox[{
  RowBox[{"SensorModel", "[", 
   RowBox[{"\[Theta]_", ",", "ModelNum_"}], "]"}], ":=", 
  RowBox[{"Switch", "[", 
   RowBox[{"ModelNum", ",", "\[IndentingNewLine]", "1", ",", 
    RowBox[{"If", "[", 
     RowBox[{
      RowBox[{
       RowBox[{"-", 
        FractionBox["\[Pi]", "2"]}], "<", "\[Theta]", "<", 
       FractionBox["\[Pi]", "2"]}], ",", 
      RowBox[{"Cos", "[", "\[Theta]", "]"}], ",", "0"}], "]"}], ",", 
    "\[IndentingNewLine]", "2", ",", 
    RowBox[{"If", "[", 
     RowBox[{
      RowBox[{
       RowBox[{"-", 
        FractionBox["\[Pi]", "2"]}], "<", "\[Theta]", "<", 
       FractionBox["\[Pi]", "2"]}], ",", 
      SuperscriptBox[
       RowBox[{"Cos", "[", "\[Theta]", "]"}], "2"], ",", "0"}], "]"}], ",", 
    "\[IndentingNewLine]", "3", ",", 
    RowBox[{"If", "[", 
     RowBox[{
      RowBox[{
       RowBox[{"-", 
        FractionBox["\[Pi]", "2"]}], "<", "\[Theta]", "<", 
       FractionBox["\[Pi]", "2"]}], ",", 
      SqrtBox[
       RowBox[{"Cos", "[", "\[Theta]", "]"}]], ",", "0"}], "]"}], ",", 
    "\[IndentingNewLine]", "4", ",", 
    RowBox[{"If", "[", 
     RowBox[{
      RowBox[{
       RowBox[{"-", 
        FractionBox["\[Pi]", "2"]}], "<", "\[Theta]", "<", 
       FractionBox["\[Pi]", "2"]}], ",", 
      RowBox[{"1", "-", 
       RowBox[{"Abs", "[", 
        RowBox[{"(", 
         RowBox[{"\[Theta]", " ", 
          FractionBox["2", "\[Pi]"]}], ")"}], "]"}]}], ",", "0"}], "]"}], ",",
     "\[IndentingNewLine]", "5", ",", 
    RowBox[{"If", "[", 
     RowBox[{
      RowBox[{
       RowBox[{"-", 
        FractionBox["\[Pi]", "2"]}], "<", "\[Theta]", "<", 
       FractionBox["\[Pi]", "2"]}], ",", 
      FractionBox[
       RowBox[{"SensorFromData", "[", "\[Theta]", "]"}], "105"], ",", "0"}], 
     "]"}]}], "\[IndentingNewLine]", "\[IndentingNewLine]", 
   "]"}]}], "\[IndentingNewLine]", 
 RowBox[{"Manipulate", "[", 
  RowBox[{
   RowBox[{"Plot", "[", 
    RowBox[{
     RowBox[{"SensorModel", "[", 
      RowBox[{"\[Theta]", ",", "modelnum"}], "]"}], ",", 
     RowBox[{"{", 
      RowBox[{"\[Theta]", ",", 
       RowBox[{
        RowBox[{"-", "2"}], "\[Pi]"}], ",", 
       RowBox[{"2", "\[Pi]"}]}], "}"}], ",", 
     RowBox[{"PlotRange", "\[Rule]", 
      RowBox[{"{", 
       RowBox[{"0", ",", "1"}], "}"}]}], ",", 
     RowBox[{"AspectRatio", "\[Rule]", "0.25"}]}], "]"}], ",", 
   RowBox[{"{", 
    RowBox[{"modelnum", ",", "1", ",", "5", ",", "1"}], "}"}]}], 
  "]"}]}], "Input",
 CellChangeTimes->{{3.571162784198*^9, 3.571162800994875*^9}, {
   3.571162847463625*^9, 3.571162861026125*^9}, {3.571163861401125*^9, 
   3.571163893494875*^9}, {3.571187166449301*^9, 3.5711871672204094`*^9}, {
   3.57124974223225*^9, 3.571249769451*^9}, 3.57124985973225*^9, {
   3.5712499010135*^9, 3.5712500857635*^9}, {3.5712553816385*^9, 
   3.57125546998225*^9}, {3.5712555372635*^9, 3.571255546997875*^9}, {
   3.57142463854475*^9, 3.571424665060375*^9}, {3.571428013247875*^9, 
   3.571428063779125*^9}, {3.57158961141975*^9, 3.571589623560375*^9}, {
   3.571589973122875*^9, 3.5715899765135*^9}, {3.571590353591625*^9, 
   3.571590359779125*^9}, {3.571627928254776*^9, 3.5716279658388195`*^9}, {
   3.5716280945138445`*^9, 3.5716281366444254`*^9}}],

Cell[BoxData[
 TagBox[
  StyleBox[
   DynamicModuleBox[{$CellContext`modelnum$$ = 1, Typeset`show$$ = True, 
    Typeset`bookmarkList$$ = {}, Typeset`bookmarkMode$$ = "Menu", 
    Typeset`animator$$, Typeset`animvar$$ = 1, Typeset`name$$ = 
    "\"untitled\"", Typeset`specs$$ = {{
      Hold[$CellContext`modelnum$$], 1, 5, 1}}, Typeset`size$$ = {
    360., {54., 58.}}, Typeset`update$$ = 0, Typeset`initDone$$, 
    Typeset`skipInitDone$$ = True, $CellContext`modelnum$3652$$ = 0}, 
    DynamicBox[Manipulate`ManipulateBoxes[
     1, StandardForm, "Variables" :> {$CellContext`modelnum$$ = 1}, 
      "ControllerVariables" :> {
        Hold[$CellContext`modelnum$$, $CellContext`modelnum$3652$$, 0]}, 
      "OtherVariables" :> {
       Typeset`show$$, Typeset`bookmarkList$$, Typeset`bookmarkMode$$, 
        Typeset`animator$$, Typeset`animvar$$, Typeset`name$$, 
        Typeset`specs$$, Typeset`size$$, Typeset`update$$, Typeset`initDone$$,
         Typeset`skipInitDone$$}, "Body" :> Plot[
        $CellContext`SensorModel[$CellContext`\[Theta], \
$CellContext`modelnum$$], {$CellContext`\[Theta], (-2) Pi, 2 Pi}, 
        PlotRange -> {0, 1}, AspectRatio -> 0.25], 
      "Specifications" :> {{$CellContext`modelnum$$, 1, 5, 1}}, 
      "Options" :> {}, "DefaultOptions" :> {}],
     ImageSizeCache->{407., {99., 104.}},
     SingleEvaluation->True],
    Deinitialization:>None,
    DynamicModuleValues:>{},
    SynchronousInitialization->True,
    UnsavedVariables:>{Typeset`initDone$$},
    UntrackedVariables:>{Typeset`size$$}], "Manipulate",
   Deployed->True,
   StripOnInput->False],
  Manipulate`InterpretManipulate[1]]], "Output",
 CellChangeTimes->{
  3.57160288735725*^9, 3.571602945466625*^9, 3.5716266880614653`*^9, {
   3.5716279292161584`*^9, 3.5716279666700144`*^9}, {3.5716281287330494`*^9, 
   3.571628137435563*^9}, 3.571687427511392*^9, 3.57176592416975*^9, 
   3.571772337466625*^9, 3.57177905210725*^9, 3.571783516961479*^9, 
   3.571785295010173*^9, 3.5717880420956383`*^9, 3.571843517656791*^9, 
   3.571843676925809*^9, 3.571851320707*^9, 3.5718682568945*^9, 
   3.571870072316375*^9}]
}, Open  ]],

Cell[BoxData[{
 RowBox[{
  RowBox[{"EmitterModel", "[", 
   RowBox[{"\[Theta]_", ",", "ModelNum_"}], "]"}], ":=", 
  RowBox[{"Switch", "[", 
   RowBox[{"ModelNum", ",", "\[IndentingNewLine]", "1", ",", 
    RowBox[{"If", "[", 
     RowBox[{
      RowBox[{
       RowBox[{"-", 
        FractionBox["\[Pi]", "2"]}], "<", "\[Theta]", "<", 
       FractionBox["\[Pi]", "2"]}], ",", 
      RowBox[{"Cos", "[", "\[Theta]", "]"}], ",", "0"}], "]"}], ",", 
    "\[IndentingNewLine]", "2", ",", 
    RowBox[{"If", "[", 
     RowBox[{
      RowBox[{
       RowBox[{"-", 
        FractionBox["\[Pi]", "2"]}], "<", "\[Theta]", "<", 
       FractionBox["\[Pi]", "2"]}], ",", 
      SuperscriptBox[
       RowBox[{"Cos", "[", "\[Theta]", "]"}], "2"], ",", "0"}], "]"}], ",", 
    "\[IndentingNewLine]", "3", ",", 
    RowBox[{"If", "[", 
     RowBox[{
      RowBox[{
       RowBox[{"-", 
        FractionBox["\[Pi]", "2"]}], "<", "\[Theta]", "<", 
       FractionBox["\[Pi]", "2"]}], ",", 
      SqrtBox[
       RowBox[{"Cos", "[", "\[Theta]", "]"}]], ",", "0"}], "]"}], ",", 
    "\[IndentingNewLine]", "4", ",", 
    RowBox[{"If", "[", 
     RowBox[{
      RowBox[{
       RowBox[{"-", 
        FractionBox["\[Pi]", "2"]}], "<", "\[Theta]", "<", 
       FractionBox["\[Pi]", "2"]}], ",", 
      RowBox[{"1", "-", 
       RowBox[{"Abs", "[", 
        RowBox[{"(", 
         RowBox[{"\[Theta]", " ", 
          FractionBox["2", "\[Pi]"]}], ")"}], "]"}]}], ",", "0"}], "]"}], ",",
     "\[IndentingNewLine]", "5", ",", 
    RowBox[{"If", "[", 
     RowBox[{
      RowBox[{
       RowBox[{"-", 
        FractionBox["\[Pi]", "2"]}], "<", "\[Theta]", "<", 
       FractionBox["\[Pi]", "2"]}], ",", 
      FractionBox[
       RowBox[{"SensorFromData", "[", "\[Theta]", "]"}], "105"], ",", "0"}], 
     "]"}]}], "\[IndentingNewLine]", "]"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"Manipulate", "[", 
   RowBox[{
    RowBox[{"Plot", "[", 
     RowBox[{
      RowBox[{"EmitterModel", "[", 
       RowBox[{"\[Theta]", ",", "1"}], "]"}], ",", 
      RowBox[{"{", 
       RowBox[{"\[Theta]", ",", 
        RowBox[{
         RowBox[{"-", "2"}], "\[Pi]"}], ",", 
        RowBox[{"2", "\[Pi]"}]}], "}"}], ",", 
      RowBox[{"PlotRange", "\[Rule]", 
       RowBox[{"{", 
        RowBox[{"0", ",", "1"}], "}"}]}], ",", 
      RowBox[{"AspectRatio", "\[Rule]", "0.25"}]}], "]"}], ",", 
    RowBox[{"{", 
     RowBox[{"modelnum", ",", "1", ",", "3", ",", "1"}], "}"}]}], "]"}], 
  ";"}]}], "Input",
 CellChangeTimes->{{3.571162863276125*^9, 3.57116287160425*^9}, 
   3.571163901932375*^9, {3.571187171997278*^9, 3.5711871728084445`*^9}, {
   3.571249764451*^9, 3.5712497667635*^9}, 3.57124986329475*^9, {
   3.571250069810375*^9, 3.571250081904125*^9}, {3.57125015804475*^9, 
   3.57125016460725*^9}, {3.571255560935375*^9, 3.571255607701*^9}, {
   3.571428389701*^9, 3.5714283902635*^9}, 3.57162799060443*^9, {
   3.571628062447736*^9, 3.57162806955796*^9}, 3.5716281857450285`*^9}],

Cell[CellGroupData[{

Cell[BoxData[{
 RowBox[{
  RowBox[{"AmplitudeModel", "[", 
   RowBox[{"r_", ",", "ModelNum_"}], "]"}], ":=", 
  RowBox[{"Switch", "[", 
   RowBox[{"ModelNum", ",", "\[IndentingNewLine]", "1", ",", 
    SuperscriptBox[
     RowBox[{"(", 
      FractionBox["1", "r"], ")"}], "2"], ",", "\[IndentingNewLine]", "2", 
    ",", 
    FractionBox["1", "r"]}], "\[IndentingNewLine]", 
   "]"}]}], "\[IndentingNewLine]", 
 RowBox[{"Manipulate", "[", 
  RowBox[{
   RowBox[{"Plot", "[", 
    RowBox[{
     RowBox[{"AmplitudeModel", "[", 
      RowBox[{"r", ",", "modelnum"}], "]"}], ",", 
     RowBox[{"{", 
      RowBox[{"r", ",", "0", ",", "30"}], "}"}], ",", 
     RowBox[{"PlotRange", "\[Rule]", 
      RowBox[{"{", 
       RowBox[{"0", ",", "1"}], "}"}]}], ",", 
     RowBox[{"AspectRatio", "\[Rule]", "0.25"}]}], "]"}], ",", 
   RowBox[{"{", 
    RowBox[{"modelnum", ",", "1", ",", "3", ",", "1"}], "}"}]}], 
  "]"}]}], "Input",
 CellChangeTimes->{{3.571162877838625*^9, 3.57116291047925*^9}, {
  3.57116378366675*^9, 3.57116379516675*^9}, {3.571255630622875*^9, 
  3.571255736060375*^9}, {3.571628008279846*^9, 3.5716280262356653`*^9}, {
  3.5717880855030622`*^9, 3.5717880872212405`*^9}}],

Cell[BoxData[
 TagBox[
  StyleBox[
   DynamicModuleBox[{$CellContext`modelnum$$ = 1, Typeset`show$$ = True, 
    Typeset`bookmarkList$$ = {}, Typeset`bookmarkMode$$ = "Menu", 
    Typeset`animator$$, Typeset`animvar$$ = 1, Typeset`name$$ = 
    "\"untitled\"", Typeset`specs$$ = {{
      Hold[$CellContext`modelnum$$], 1, 3, 1}}, Typeset`size$$ = {
    360., {53., 56.}}, Typeset`update$$ = 0, Typeset`initDone$$, 
    Typeset`skipInitDone$$ = True, $CellContext`modelnum$3677$$ = 0}, 
    DynamicBox[Manipulate`ManipulateBoxes[
     1, StandardForm, "Variables" :> {$CellContext`modelnum$$ = 1}, 
      "ControllerVariables" :> {
        Hold[$CellContext`modelnum$$, $CellContext`modelnum$3677$$, 0]}, 
      "OtherVariables" :> {
       Typeset`show$$, Typeset`bookmarkList$$, Typeset`bookmarkMode$$, 
        Typeset`animator$$, Typeset`animvar$$, Typeset`name$$, 
        Typeset`specs$$, Typeset`size$$, Typeset`update$$, Typeset`initDone$$,
         Typeset`skipInitDone$$}, "Body" :> Plot[
        $CellContext`AmplitudeModel[$CellContext`r, $CellContext`modelnum$$], \
{$CellContext`r, 0, 30}, PlotRange -> {0, 1}, AspectRatio -> 0.25], 
      "Specifications" :> {{$CellContext`modelnum$$, 1, 3, 1}}, 
      "Options" :> {}, "DefaultOptions" :> {}],
     ImageSizeCache->{407., {97., 102.}},
     SingleEvaluation->True],
    Deinitialization:>None,
    DynamicModuleValues:>{},
    SynchronousInitialization->True,
    UnsavedVariables:>{Typeset`initDone$$},
    UntrackedVariables:>{Typeset`size$$}], "Manipulate",
   Deployed->True,
   StripOnInput->False],
  Manipulate`InterpretManipulate[1]]], "Output",
 CellChangeTimes->{
  3.5715857856385*^9, 3.5715860913885*^9, 3.571602881622875*^9, 
   3.571602945560375*^9, 3.5716266886022434`*^9, {3.5716280148292637`*^9, 
   3.571628027177019*^9}, 3.571687428703106*^9, 3.571765924685375*^9, 
   3.571772337591625*^9, 3.571779052201*^9, 3.571783517055198*^9, 
   3.5717852951038914`*^9, 3.571788042220597*^9, 3.571788087674215*^9, 
   3.5718435188184614`*^9, 3.571843677226241*^9, 3.571851321253875*^9, 
   3.5718682570195*^9, 3.5718700723945*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[{
 RowBox[{
  RowBox[{"InverseAmplitudeModel", "[", 
   RowBox[{"r_", ",", "ModelNum_"}], "]"}], ":=", 
  RowBox[{"Switch", "[", 
   RowBox[{"ModelNum", ",", "\[IndentingNewLine]", "1", ",", 
    FractionBox["1", 
     SqrtBox["r"]], ",", "\[IndentingNewLine]", "2", ",", 
    FractionBox["1", "r"]}], "\[IndentingNewLine]", 
   "]"}]}], "\[IndentingNewLine]", 
 RowBox[{"Manipulate", "[", 
  RowBox[{
   RowBox[{"Plot", "[", 
    RowBox[{
     RowBox[{"InverseAmplitudeModel", "[", 
      RowBox[{"r", ",", "modelnum"}], "]"}], ",", 
     RowBox[{"{", 
      RowBox[{"r", ",", "0", ",", "30"}], "}"}], ",", 
     RowBox[{"PlotRange", "\[Rule]", 
      RowBox[{"{", 
       RowBox[{"0", ",", "1"}], "}"}]}], ",", 
     RowBox[{"AspectRatio", "\[Rule]", "0.25"}]}], "]"}], ",", 
   RowBox[{"{", 
    RowBox[{"modelnum", ",", "1", ",", "2", ",", "1"}], "}"}]}], 
  "]"}]}], "Input",
 CellChangeTimes->{{3.571788095702792*^9, 3.571788116024152*^9}, {
  3.5717881467326784`*^9, 3.571788157963315*^9}}],

Cell[BoxData[
 TagBox[
  StyleBox[
   DynamicModuleBox[{$CellContext`modelnum$$ = 1, Typeset`show$$ = True, 
    Typeset`bookmarkList$$ = {}, Typeset`bookmarkMode$$ = "Menu", 
    Typeset`animator$$, Typeset`animvar$$ = 1, Typeset`name$$ = 
    "\"untitled\"", Typeset`specs$$ = {{
      Hold[$CellContext`modelnum$$], 1, 2, 1}}, Typeset`size$$ = {
    360., {53., 56.}}, Typeset`update$$ = 0, Typeset`initDone$$, 
    Typeset`skipInitDone$$ = True, $CellContext`modelnum$3702$$ = 0}, 
    DynamicBox[Manipulate`ManipulateBoxes[
     1, StandardForm, "Variables" :> {$CellContext`modelnum$$ = 1}, 
      "ControllerVariables" :> {
        Hold[$CellContext`modelnum$$, $CellContext`modelnum$3702$$, 0]}, 
      "OtherVariables" :> {
       Typeset`show$$, Typeset`bookmarkList$$, Typeset`bookmarkMode$$, 
        Typeset`animator$$, Typeset`animvar$$, Typeset`name$$, 
        Typeset`specs$$, Typeset`size$$, Typeset`update$$, Typeset`initDone$$,
         Typeset`skipInitDone$$}, "Body" :> Plot[
        $CellContext`InverseAmplitudeModel[$CellContext`r, \
$CellContext`modelnum$$], {$CellContext`r, 0, 30}, PlotRange -> {0, 1}, 
        AspectRatio -> 0.25], 
      "Specifications" :> {{$CellContext`modelnum$$, 1, 2, 1}}, 
      "Options" :> {}, "DefaultOptions" :> {}],
     ImageSizeCache->{407., {97., 102.}},
     SingleEvaluation->True],
    Deinitialization:>None,
    DynamicModuleValues:>{},
    SynchronousInitialization->True,
    UnsavedVariables:>{Typeset`initDone$$},
    UntrackedVariables:>{Typeset`size$$}], "Manipulate",
   Deployed->True,
   StripOnInput->False],
  Manipulate`InterpretManipulate[1]]], "Output",
 CellChangeTimes->{{3.5717881396412897`*^9, 3.5717881582913303`*^9}, 
   3.571843519078836*^9, 3.5718436774966297`*^9, 3.571851321347625*^9, 
   3.571868257097625*^9, 3.571870072472625*^9}]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell["Brightness Matrix", "Section",
 CellChangeTimes->{{3.57158576660725*^9, 3.571585772076*^9}}],

Cell[BoxData[
 RowBox[{
  RowBox[{"BrightnessMatrix", "[", 
   RowBox[{
    RowBox[{"{", 
     RowBox[{"RxDropletX_", ",", "RxDropletY_"}], "}"}], ",", 
    "RxDroplet\[Theta]_", ",", 
    RowBox[{"{", 
     RowBox[{"TxDropletX_", ",", "TxDropletY_"}], "}"}], ",", 
    "TxDroplet\[Theta]_", ",", 
    RowBox[{"{", 
     RowBox[{"SmodelNum_", ",", "EmodelNum_", ",", "AmodelNum_"}], "}"}]}], 
   "]"}], ":=", 
  RowBox[{"Table", "[", "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{
     RowBox[{"SensorModel", "[", 
      RowBox[{
       RowBox[{"Rx\[Alpha]", "[", 
        RowBox[{
         RowBox[{"{", 
          RowBox[{"RxDropletX", ",", "RxDropletY"}], "}"}], ",", 
         "RxDroplet\[Theta]", ",", 
         RowBox[{"{", 
          RowBox[{"TxDropletX", ",", "TxDropletY"}], "}"}], ",", 
         "TxDroplet\[Theta]", ",", "e", ",", "s"}], "]"}], ",", "SmodelNum"}],
       "]"}], "*", "\[IndentingNewLine]", 
     RowBox[{"EmitterModel", "[", 
      RowBox[{
       RowBox[{"Tx\[Beta]", "[", 
        RowBox[{
         RowBox[{"{", 
          RowBox[{"RxDropletX", ",", "RxDropletY"}], "}"}], ",", 
         "RxDroplet\[Theta]", ",", 
         RowBox[{"{", 
          RowBox[{"TxDropletX", ",", "TxDropletY"}], "}"}], ",", 
         "TxDroplet\[Theta]", ",", "e", ",", "s"}], "]"}], ",", "EmodelNum"}],
       "]"}], "*", "\[IndentingNewLine]", 
     RowBox[{"AmplitudeModel", "[", 
      RowBox[{
       RowBox[{"Norm", "[", 
        RowBox[{"RxTxPairVector", "[", 
         RowBox[{
          RowBox[{"{", 
           RowBox[{"RxDropletX", ",", "RxDropletY"}], "}"}], ",", 
          "RxDroplet\[Theta]", ",", 
          RowBox[{"{", 
           RowBox[{"TxDropletX", ",", "TxDropletY"}], "}"}], ",", 
          "TxDroplet\[Theta]", ",", "e", ",", "s"}], "]"}], "]"}], ",", 
       "AmodelNum"}], "]"}]}], ",", 
    RowBox[{"{", 
     RowBox[{"e", ",", "0", ",", "5"}], "}"}], 
    RowBox[{"(*", " ", "rows", " ", "*)"}], ",", 
    RowBox[{"{", 
     RowBox[{"s", ",", "0", ",", "5"}], "}"}]}], 
   RowBox[{"(*", " ", "columns", " ", "*)"}], "]"}]}]], "Input",
 CellChangeTimes->{{3.57158602948225*^9, 3.571586032326*^9}}],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"BrightnessMatrix", "[", 
   RowBox[{
    RowBox[{"{", 
     RowBox[{"0", ",", "0"}], "}"}], ",", "0", ",", 
    RowBox[{"{", 
     RowBox[{"StandardX", ",", "StandardY"}], "}"}], ",", "Standard\[Theta]", 
    ",", 
    RowBox[{"{", 
     RowBox[{"1", ",", "1", ",", "1"}], "}"}]}], "]"}], "//", 
  "MatrixForm"}]], "Input",
 CellChangeTimes->{{3.571585988326*^9, 3.571586037872875*^9}, {
  3.57158609591975*^9, 3.57158609804475*^9}, {3.571870288910125*^9, 
  3.571870301472625*^9}}],

Cell[BoxData[
 TagBox[
  RowBox[{"(", "\[NoBreak]", GridBox[{
     {"0.00023866552540270815`", "0.`", "0.`", "0.`", 
      "0.0021736656964957695`", "0.0029013803549628363`"},
     {"0.`", "0.`", "0.`", "0.`", "0.`", "0.`"},
     {"0.`", "0.`", "0.`", "0.`", "0.`", "0.`"},
     {"0.`", "0.`", "0.`", "0.`", "0.`", "0.`"},
     {"0.`", "0.`", "0.`", "0.`", "0.`", "0.`"},
     {"0.0005980850678491959`", "0.`", "0.`", "0.`", "0.0016600498169486242`",
       "0.0031634380949730626`"}
    },
    GridBoxAlignment->{
     "Columns" -> {{Center}}, "ColumnsIndexed" -> {}, "Rows" -> {{Baseline}}, 
      "RowsIndexed" -> {}},
    GridBoxSpacings->{"Columns" -> {
        Offset[0.27999999999999997`], {
         Offset[0.7]}, 
        Offset[0.27999999999999997`]}, "ColumnsIndexed" -> {}, "Rows" -> {
        Offset[0.2], {
         Offset[0.4]}, 
        Offset[0.2]}, "RowsIndexed" -> {}}], "\[NoBreak]", ")"}],
  Function[BoxForm`e$, 
   MatrixForm[BoxForm`e$]]]], "Output",
 CellChangeTimes->{{3.57158603866975*^9, 3.571586098560375*^9}, 
   3.571602881826*^9, 3.571602945654125*^9, 3.571626689022848*^9, 
   3.5716874293440275`*^9, 3.571765924997875*^9, 3.57177233773225*^9, 
   3.571779052341625*^9, 3.5717835171957765`*^9, 3.57178529522885*^9, 
   3.571788042361175*^9, 3.571843519699729*^9, 3.5718436780974936`*^9, 
   3.571851321582*^9, 3.571868257222625*^9, 3.571870072597625*^9, 
   3.57187030223825*^9}]
}, Open  ]]
}, Open  ]]
},
WindowSize->{1392, 963},
WindowMargins->{{58, Automatic}, {Automatic, 25}},
FrontEndVersion->"8.0 for Microsoft Windows (32-bit) (October 6, 2011)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[557, 20, 649, 14, 87, "Input"],
Cell[CellGroupData[{
Cell[1231, 38, 103, 1, 71, "Section"],
Cell[1337, 41, 123, 3, 31, "Input"],
Cell[1463, 46, 1032, 29, 78, "Input"],
Cell[CellGroupData[{
Cell[2520, 79, 298, 8, 31, "Input"],
Cell[2821, 89, 1142, 37, 50, "Output"]
}, Open  ]],
Cell[3978, 129, 1626, 39, 112, "Input"],
Cell[5607, 170, 690, 20, 31, "Input"],
Cell[6300, 192, 3027, 78, 252, "Input"]
}, Open  ]],
Cell[CellGroupData[{
Cell[9364, 275, 97, 1, 71, "Section"],
Cell[9464, 278, 1061, 24, 52, "Input"],
Cell[CellGroupData[{
Cell[10550, 306, 3232, 82, 276, "Input"],
Cell[13785, 390, 2118, 41, 220, "Output"]
}, Open  ]],
Cell[15918, 434, 2960, 78, 256, "Input"],
Cell[CellGroupData[{
Cell[18903, 516, 1185, 30, 160, "Input"],
Cell[20091, 548, 2105, 40, 216, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[22233, 593, 1010, 26, 164, "Input"],
Cell[23246, 621, 1825, 36, 216, "Output"]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[25120, 663, 98, 1, 71, "Section"],
Cell[25221, 666, 2135, 56, 92, "Input"],
Cell[CellGroupData[{
Cell[27381, 726, 517, 14, 31, "Input"],
Cell[27901, 742, 1412, 30, 116, "Output"]
}, Open  ]]
}, Open  ]]
}
]
*)

(* End of internal cache information *)
